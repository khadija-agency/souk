<?php
namespace Souk\Extension\Product;

interface i_table_object {
    public function get_data(): array;
    public function set_data(array $array): bool;
    public function set(string $key, $value): bool;
    public function get(string $key);
    public function validate(string $key, $value): bool|array;
    public function valid(): bool;
    public function set_stm(string $name, PDOStatement $stm): bool;
    public function get_stm(string $name): PDOStatement|bool;
}
