<?php
namespace Souk\Extension\Product;

class table_object implements i_table_object {

    protected array $data;
    protected array $stm;

    public function __construct($pdo_con) {
        // get column names
        $this->stm['table_columns'] = $pdo_con->prepare("DESCRIBE $tablenames");
        $this->data = $this->get_data();
        foreach($data as $key) {
            $this->error[$key] = false;
        }
    }

    public function set_stm(string $name, PDOStatement $stm): bool {
        // set prepared statement
        $this->stm[$name] = $stm;
        return true;
    }

    public function get_stm(string $name): PDOStatement|bool {
        // return prepared statement
        if(isset($this->stm[$name])) {
            return $this->stm[$name];
        }
        return false;
    }

    public function fetch_stm($name) {
        $stm = $this->get_stm($name);
        if($stm == false) {
            return false;
        }
        // this saves time for folks:  should use 
        // $count = $stmt->rowCount() 
        // after $stmt->execute() 
        $ret = $stm->execute($this->data);
        $this->stm[$name]['count'] = $stm->rowCount();
        $this->data = $stm->fetch(PDO::FETCH_ASSOC);
        //$result = $sth->fetchAll(PDO::FETCH_FUNC, array('Foo', 'bar'));
        
    }
    public function get_data(): array {
        if($this->data == null) {
            $stm_table_col = $this->stm['table_col']->execute();
            $table_names = $query->fetchAll(PDO::FETCH_COLUMN);
            var_dump($table_names);
            exit();


        }
        return $this->data;
    }

    public function validate(string $key, $value):bool{
        if(isset($this->data[$key])) {
            return true;
        }
        return false;
    }

    public function set_data(array $data): bool {
        $return = true;
        // check if data is valid
        foreach($data as $key => $value) {
            if($this->validate($key, $value) == true) {
                $this->data[$key] = $value;
                continue;
            }
            // validate may return error array
            $return = false;
        }
        // product data set
        return $return;
    }
 
    public function set(string $key, $value): bool{
       if($this->validate($key, $value) == true){
           $this->data[$key] = $value;
           return true;
       }
       return false;
    }

    public function get($key){
       return $this->data[$key]; 
    }

    public function valid(): bool{
        foreach($this->data as $key => $value){
            if($this->validate($key, $value)){
                continue;
            }
            return false;
        }
        return true;
    }
}
