<?php
namespace Session;
final class DB {
    private $st_read    = null;
    private $st_write   = null;
    private $st_destroy = null;
    private $st_delete  = null;
    public $maxlifetime;

	public function __construct($registry) {
		$pdo = $registry->get('pdo');
		$this->config = $registry->get('config');

		$this->maxlifetime = ini_get('session.gc_maxlifetime') !== null ? (int)ini_get('session.gc_maxlifetime') : 1440;

        $this->st_read = $pdo->prepare("
            SELECT data
            FROM " . DB_PREFIX . "session
            WHERE session_id = :session_id
            AND expire > :expire");

        $this->st_write = $pdo->prepare("
            REPLACE INTO " . DB_PREFIX . "session SET
            session_id  = :session_id,
            data            = :data,
            expire          = :expire");

        $this->st_destroy = $pdo->prepare("DELETE
            FROM " . DB_PREFIX . "session
            WHERE session_id = :session_id");

        $this->st_delete  =	$pdo->prepare("DELETE
            FROM " . DB_PREFIX . "session
            WHERE expire < :expire");

        // we do it on close
        //$this->gc();
	}

	public function read($session_id) {
        $this->st_read->execute(array(
            'session_id' => $session_id,
            'expire' => date('Y-m-d H:i:s')
        ));
		if ($this->st_read->rowCount() == 1) {
            $result = $this->st_read->fetch();
			return json_decode($result['data'], true);
		} else {
			return array();
		}
	}

	public function write($session_id, $data) {
		if ($session_id) {
            $this->st_write->execute(array(
                'session_id' => $session_id,
                'data'       => is_array($data) ? json_encode($data) : '',
                'expire'     => date('Y-m-d H:i:s', time() + $this->maxlifetime)
            ));
		}
		return true;
	}

	public function destroy($session_id) {
        $this->st_destroy->execute(array(
            'session_id' => $session_id
        ));
		return true;
	}

	public function gc() {
       if (ini_get('session.gc_divisor') && $gc_divisor = (int)ini_get('session.gc_divisor')) {
			$gc_divisor = $gc_divisor === 0 ? 100 : $gc_divisor;
		} else {
			$gc_divisor = 100;
		}

		if (ini_get('session.gc_probability')) {
			$gc_probability = (int)ini_get('session.gc_probability');
		} else {
			$gc_probability = 1;
		}

        if (mt_rand() / mt_getrandmax() < $gc_probability / $gc_divisor) {
            $this->st_delete->execute(array(
                'expire' => date('Y-m-d H:i:s', time())
            ));
		}
		return true;
	}
}
