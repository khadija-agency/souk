<?php
namespace Cart;
class User {
    private $user_id;
    private $user_group_id;
    private $username;
    private $permission = array();

    private $st_username   = null;
    private $st_userid     = null;
    private $st_userperm   = null;
    private $st_updatepass = null;
    private $st_updateip   = null;

    public function __construct($registry) {
        $pdo = $registry->get('pdo');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');

        $this->st_username = $pdo->prepare("SELECT *
            FROM " . DB_PREFIX . "user
            WHERE username = :username
            AND status = '1'");

        $this->st_userperm = $pdo->prepare("SELECT permission
                    FROM " . DB_PREFIX . "user_group
                    WHERE user_group_id = :user_group_id");

        $this->st_updatepass = $pdo->prepare("UPDATE " . DB_PREFIX . "user
            SET password = :password
            WHERE user_id = :user_id");

        $this->st_userid = $pdo->prepare("SELECT *
            FROM " . DB_PREFIX . "user
            WHERE user_id = :user_id
            AND status = '1'");

        $this->st_updateip = $pdo->prepare("UPDATE " . DB_PREFIX . "user
            SET ip = :ip
            WHERE user_id = :user_id");

        if (isset($this->session->data['user_id'])) {

            $this->st_userid->execute(array(
                'user_id' => $this->session->data['user_id']
            ));

            if ($this->st_userid->rowCount() == 1) {

                $user = $this->st_userid->fetch();

                $this->user_id       = $user['user_id'];
                $this->username      = $user['username'];
                $this->user_group_id = $user['user_group_id'];

                $this->st_updateip->execute(array(
                    'ip'        => $this->request->server['REMOTE_ADDR'],
                    'user_id'   => $this->session->data['user_id']
                ));

                $this->st_userperm->execute(array(
                    'user_group_id' => $user['user_group_id']
                ));

                $userperm = $this->st_userperm->fetch();

                $permissions = json_decode($userperm['permission'], true);
                if (is_array($permissions)) {
                    $this->permission = $permissions;
                }
            } else {
                $this->logout();
            }
        }
    }

    public function login($username, $password) {
        $this->st_username->execute(array(
            'username' => $username
        ));

        $user = $this->st_username->fetch();

        if ($this->st_username->rowCount() == 1) {
            if (password_verify($password, $user['password'])) {
                if (password_needs_rehash($user['password'], PASSWORD_DEFAULT)) {
                    $this->st_updatepass->execute(array(
                        'password' => password_hash($password, PASSWORD_DEFAULT),
                        'user_id'  => $user['user_id']
                    ));
                }
            } else {
                return false;
            }

            $this->session->data['user_id'] = $user['user_id'];

            $this->user_id       = $user['user_id'];
            $this->username      = $user['username'];
            $this->user_group_id = $user['user_group_id'];

            $this->st_userperm->execute(array(
                'user_group_id' => $user['user_group_id']
            ));

            $userperm = $this->st_userperm->fetch();

            $permissions = json_decode($userperm['permission'], true);
            if (is_array($permissions)) {
                $this->permission = $permissions;
            }

            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        unset($this->session->data['user_id']);

        $this->user_id = '';
        $this->username = '';
    }

    public function hasPermission($key, $value) {
        if (isset($this->permission[$key])) {
            return in_array($value, $this->permission[$key]);
        } else {
            return false;
        }
    }

    public function isLogged() {
        return $this->user_id;
    }

    public function getId() {
        return $this->user_id;
    }

    public function getUserName() {
        return $this->username;
    }

    public function getGroupId() {
        return $this->user_group_id;
    }
}
