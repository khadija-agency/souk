<?php

namespace dpd;

class dpd_autoload {
    public function start() {
		require_once("dpdconfiguration.php");
		require_once("Common/UrlGenerator.php");
		require_once("Common/ListAction.php");
		require_once("Label/Action/ShipmentListAction.php");
		require_once("Label/Action/BatchListAction.php");
		require_once("Label/LabelCallbackManager.php");
		require_once("Label/LabelResponseTransformer.php");
		require_once("Label/JobStateManager.php");
		require_once("Label/LabelRequestFactory.php");
		require_once("Label/OrderViewDataTransformer.php");
		require_once("Label/Entity/Batch.php");
		require_once("Label/LabelGenerationManager.php");
		require_once("Sdk/DpdConnectClientBuilder.php");
		require_once("dpd_autoload.php");
		require_once("Settings/IndexSetAction.php");
		require_once("Settings/IndexGetAction.php");
		require_once("dpdauthentication.php");
		require_once("ParcelShop/Client/GoogleMapsClient.php");
    }
}

