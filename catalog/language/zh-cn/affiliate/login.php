<?php
$_['heading_title']		= '会员计划';
$_['text_account']		= '帐户';
$_['text_login']		= '登录';
$_['text_description']		= '<p>%s 联盟计划是免费的，会员可以通过在其网站上放置一个或多个链接来为 %s 或特定产品做广告，从而获得收入。向点击这些链接的客户进行的任何销售都将获得会员佣金。目前的标准佣金率为 %s。</p><p>有关详细信息，请访问我们的常见问题页面或查看我们的附属条款和条件。</p>';
$_['text_new_affiliate']		= '新会员';
$_['text_register_account']		= '<p>我目前不是会员。</p><p>单击下面的继续以创建新的会员帐户。请注意，这与您的客户帐户没有任何关联。</p>';
$_['text_returning_affiliate']		= '会员登录';
$_['text_i_am_returning_affiliate']		= '我是一个返回的会员。';
$_['text_forgotten']		= '忘记密码';
$_['entry_email']		= '附属电子邮件';
$_['entry_password']		= '密码';
$_['error_login']		= '警告：电子邮件地址和/或密码不匹配。';
$_['error_attempts']		= '警告：您的帐户已超过允许的登录尝试次数。请在 1 小时后重试。';
$_['error_approved']		= '警告：您的帐户需要批准才能登录。';
