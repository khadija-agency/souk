<?php
$_['heading_title']		= '产品对比';
$_['text_product']		= '产品详情';
$_['text_name']		= '产品';
$_['text_image']		= '图片';
$_['text_price']		= '价格';
$_['text_model']		= '模型';
$_['text_manufacturer']		= '牌';
$_['text_availability']		= '可用性';
$_['text_instock']		= '有存货';
$_['text_rating']		= '评分';
$_['text_reviews']		= '基于 %s 的评论。';
$_['text_summary']		= '概括';
$_['text_weight']		= '重量';
$_['text_dimension']		= '尺寸（长 x 宽 x 高）';
$_['text_compare']		= '产品比较 (%s)';
$_['text_success']		= '成功：您已将 <a href="%s">%s</a> 添加到您的<a href="%s">产品比较</a>！';
$_['text_remove']		= '成功：您已修改产品比较！';
$_['text_empty']		= '您还没有选择任何产品进行比较。';
