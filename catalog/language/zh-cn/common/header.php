<?php
$_['text_wishlist']		= '愿望清单 (%s)';
$_['text_shopping_cart']		= '购物车';
$_['text_account']		= '我的帐户';
$_['text_register']		= '登记';
$_['text_login']		= '登录';
$_['text_order']		= '订单历史';
$_['text_transaction']		= '交易';
$_['text_download']		= '下载';
$_['text_logout']		= '登出';
