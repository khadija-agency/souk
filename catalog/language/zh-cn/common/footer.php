<?php
$_['text_information']		= '信息';
$_['text_service']		= '客户服务';
$_['text_extra']		= '附加功能';
$_['text_contact']		= '联系我们';
$_['text_return']		= '退货';
$_['text_sitemap']		= '网站地图';
$_['text_gdpr']		= 'GDPR';
$_['text_manufacturer']		= '品牌';
$_['text_voucher']		= '礼券';
$_['text_affiliate']		= '附属公司';
$_['text_special']		= '特价商品';
$_['text_account']		= '我的帐户';
$_['text_order']		= '订单历史';
$_['text_wishlist']		= '愿望清单';
$_['text_newsletter']		= '通讯';
$_['text_powered'] = 'Powered By <a href="https://khadija.agency">Souk</a><br /> %s &copy; %s';
