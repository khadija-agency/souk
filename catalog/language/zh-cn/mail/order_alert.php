<?php
$_['text_subject']		= '%s - 订单 %s';
$_['text_received']		= '您已收到订单。';
$_['text_order_id']		= '订单编号：';
$_['text_date_added']		= '添加日期：';
$_['text_order_status']		= '订单状态：';
$_['text_product']		= '产品';
$_['text_total']		= '总计';
$_['text_comment']		= '对您的订单的评论是：';
