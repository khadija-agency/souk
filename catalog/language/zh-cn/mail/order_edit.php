<?php
$_['text_subject']		= '%s - 订单更新 %s';
$_['text_order_id']		= '订单编号：';
$_['text_date_added']		= '添加日期：';
$_['text_order_status']		= '您的订单已更新为以下状态：';
$_['text_comment']		= '对您的订单的评论是：';
$_['text_link']		= '要查看您的订单，请单击以下链接：';
$_['text_footer']		= '如果您有任何问题，请回复此电子邮件。';
