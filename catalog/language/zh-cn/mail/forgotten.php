<?php
$_['text_subject']		= '%s - 密码重置请求';
$_['text_greeting']		= '为 %s 客户帐户请求了新密码。';
$_['text_change']		= '要重置您的密码，请点击以下链接：';
$_['text_ip']		= '用于发出此请求的 IP 是：';
