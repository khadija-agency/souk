<?php
$_['heading_title']		= '联系我们';
$_['text_location']		= '我们的位置';
$_['text_store']		= '我们的商店';
$_['text_contact']		= '联系表';
$_['text_address']		= '地址';
$_['text_telephone']		= '电话';
$_['text_open']		= '开放时间';
$_['text_comment']		= '注释';
$_['text_message']		= '<p>您的询问已成功发送给店主！</p>';
$_['entry_name']		= '你的名字';
$_['entry_email']		= '电子邮件地址';
$_['entry_enquiry']		= '询问';
$_['email_subject']		= '查询 %s';
$_['error_name']		= '名称必须介于 3 到 32 个字符之间！';
$_['error_email']		= '电子邮件地址似乎无效！';
$_['error_enquiry']		= '查询必须在 10 到 3000 个字符之间！';
