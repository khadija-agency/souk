<?php
$_['text_success']		= '成功：您的礼券折扣已应用！';
$_['text_cart']		= '成功：您已经修改了您的购物车！';
$_['text_for']		= '%s 的 %s 礼券';
$_['error_permission']		= '警告：您没有访问 API 的权限！';
$_['error_voucher']		= '警告：礼券无效或余额已用完！';
$_['error_to_name']		= '收件人姓名必须介于 1 到 64 个字符之间！';
$_['error_from_name']		= '您的姓名必须介于 1 到 64 个字符之间！';
$_['error_email']		= '电子邮件地址似乎无效！';
$_['error_theme']		= '您必须选择一个主题！';
$_['error_amount']		= '金额必须在 %s 和 %s 之间！';
