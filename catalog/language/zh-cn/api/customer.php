<?php
$_['text_success']		= '您已成功修改客户';
$_['error_permission']		= '警告：您没有访问 API 的权限！';
$_['error_customer']		= '您必须选择客户！';
$_['error_firstname']		= '名字必须介于 1 到 32 个字符之间！';
$_['error_lastname']		= '姓氏必须介于 1 到 32 个字符之间！';
$_['error_email']		= '电子邮件地址似乎无效！';
$_['error_telephone']		= '电话号码必须在 3 到 32 个字符之间！';
$_['error_custom_field']		= '需要 %s！';
