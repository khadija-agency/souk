<?php
$_['text_success']		= '成功：您的优惠券折扣已应用！';
$_['error_permission']		= '警告：您没有访问 API 的权限！';
$_['error_coupon']		= '警告：优惠券无效、过期或已达到使用限制！';
