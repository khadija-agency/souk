<?php
$_['code']		= 'zh';
$_['direction']		= 'ltr';
$_['date_format_short']		= 'd/m/Y';
$_['date_format_long']		= 'l dS F Y';
$_['time_format']		= 'h:i:s A';
$_['datetime_format']		= 'd/m/Y H:i:s';
$_['decimal_point']		= '.';
$_['thousand_point']		= ',';
$_['text_home']		= '<i class="fa fa-home"></i>';
$_['text_yes']		= '是的';
$_['text_no']		= '不';
$_['text_none']		= ' - - 没有任何  - -';
$_['text_select']		= ' - - 请选择  - -';
$_['text_all_zones']		= '所有区域';
$_['text_pagination']		= '显示 %d 到 %d，共 %d（%d 页）';
$_['text_loading']		= '正在加载...';
$_['text_no_results']		= '没有结果！';
$_['button_address_add']		= '添加地址';
$_['button_back']		= '后退';
$_['button_continue']		= '继续';
$_['button_cart']		= '添加到购物车';
$_['button_cancel']		= '取消';
$_['button_compare']		= '比较这个产品';
$_['button_wishlist']		= '添加到愿望清单';
$_['button_checkout']		= '查看';
$_['button_remove_coupon']		= 'Remove Coupon';
$_['button_confirm']		= '确认订单';
$_['button_coupon']		= '申请优惠券';
$_['button_delete']		= '删除';
$_['button_download']		= '下载';
$_['button_edit']		= '编辑';
$_['button_filter']		= '优化搜索';
$_['button_new_address']		= '新地址';
$_['button_change_address']		= '更换地址';
$_['button_reviews']		= '评论';
$_['button_write']		= '写评论';
$_['button_login']		= '登录';
$_['button_update']		= '更新';
$_['button_remove']		= '消除';
$_['button_reorder']		= '重新排序';
$_['button_return']		= '返回';
$_['button_shopping']		= '继续购物';
$_['button_search']		= '搜索';
$_['button_shipping']		= '申请送货';
$_['button_submit']		= '提交';
$_['button_guest']		= '客人结帐';
$_['button_view']		= '看法';
$_['button_voucher']		= '申请礼券';
$_['button_upload']		= '上传文件';
$_['button_reward']		= '申请积分';
$_['button_quote']		= '获取报价';
$_['button_list']		= '列表';
$_['button_grid']		= '网格';
$_['button_map']		= '查看谷歌地图';
$_['error_exception']		= '错误代码(%s): %s in %s on line %s';
$_['error_upload_1']		= '警告：上传的文件超过了 php.ini 中的 upload_max_filesize 指令！';
$_['error_upload_2']		= '警告：上传的文件超出了 HTML 表单中指定的 MAX_FILE_SIZE 指令！';
$_['error_upload_3']		= '警告：上传的文件只是部分上传！';
$_['error_upload_4']		= '警告：没有上传文件！';
$_['error_upload_6']		= '警告：缺少一个临时文件夹！';
$_['error_upload_7']		= '警告：无法将文件写入磁盘！';
$_['error_upload_8']		= '警告：文件上传被扩展停止！';
$_['error_upload_999']		= '警告：没有可用的错误代码！';
$_['error_curl']		= 'CURL: 错误代码(%s): %s';
$_['datepicker']		= 'zh-cn';
