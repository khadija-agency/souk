<?php
$_['heading_title']		= '更改密码';
$_['text_account']		= '帐户';
$_['text_password']		= '你的密码';
$_['text_success']		= '成功：您的密码已成功更新。';
$_['entry_password']		= '密码';
$_['entry_confirm']		= '确认密码';
$_['error_password']		= '密码必须在 4 到 20 个字符之间！';
$_['error_confirm']		= '密码确认与密码不符！';
