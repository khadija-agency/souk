<?php
$_['heading_title']		= '帐户下载';
$_['text_account']		= '帐户';
$_['text_downloads']		= '下载';
$_['text_empty']		= '您之前没有下过任何可下载的订单！';
$_['column_order_id']		= '订单编号';
$_['column_name']		= '姓名';
$_['column_size']		= '尺寸';
$_['column_date_added']		= '添加日期';
