<?php
$_['heading_title']		= '会员追踪';
$_['text_account']		= '帐户';
$_['text_description']		= '为了确保您收到您发送给我们的推荐的报酬，我们需要通过在链接到我们的 URL 中放置一个跟踪代码来跟踪推荐。您可以使用以下工具生成指向 %s 网站的链接。';
$_['entry_code']		= '您的跟踪代码';
$_['entry_generator']		= '跟踪链接生成器';
$_['entry_link']		= '追踪链接';
$_['help_generator']		= '输入您要链接的产品名称';
