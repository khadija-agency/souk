<?php
$_['heading_title']		= '我的账户信息';
$_['text_account']		= '帐户';
$_['text_edit']		= '编辑信息';
$_['text_your_details']		= '你的个人信息';
$_['text_success']		= '成功：您的帐户已成功更新。';
$_['entry_firstname']		= '名';
$_['entry_lastname']		= '姓';
$_['entry_email']		= '电子邮件';
$_['entry_telephone']		= '电话';
$_['error_exists']		= '警告：电子邮件地址已经注册！';
$_['error_firstname']		= '名字必须介于 1 到 32 个字符之间！';
$_['error_lastname']		= '姓氏必须介于 1 到 32 个字符之间！';
$_['error_email']		= '电子邮件地址似乎无效！';
$_['error_telephone']		= '电话号码必须在 3 到 32 个字符之间！';
$_['error_custom_field']		= '需要 %s！';
