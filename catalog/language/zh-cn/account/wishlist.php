<?php
$_['heading_title']		= '我的收藏';
$_['text_account']		= '帐户';
$_['text_instock']		= '有存货';
$_['text_wishlist_total']		= 'Wish List (%s)';
$_['text_login']		= '您必须<a href="%s">登录</a>或<a href="%s">创建一个帐户</a>来保存<a href="%s">%s</a>加入您的<a href="%s">愿望清单</a>！';
$_['text_success']		= '成功：您已将 <a href="%s">%s</a> 添加到您的<a href="%s">愿望清单</a>！';
$_['text_remove']		= '成功：您已修改您的愿望清单！';
$_['text_empty']		= '你的愿望清单是空的。';
$_['column_image']		= '图片';
$_['column_name']		= '产品名称';
$_['column_model']		= '模型';
$_['column_stock']		= '库存';
$_['column_price']		= '单价';
$_['column_action']		= '行动';
