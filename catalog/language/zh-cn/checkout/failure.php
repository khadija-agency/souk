<?php
$_['heading_title']		= '支付失败！';
$_['text_basket']		= '购物车';
$_['text_checkout']		= '查看';
$_['text_failure']		= '付款失败';
$_['text_message']		= '<p>处理您的付款时出现问题，订单未完成。</p>

<p>可能的原因有：</p>
<ul>
<li>资金不足</li>
<li>验证失败</li>
</ul>

<p>请尝试使用其他付款方式再次订购。</p>

<p>如果问题仍然存在，请<a href="%s">与我们联系</a>并提供您尝试下的订单的详细信息。</p>
';
