<?php
$_['text_title']		= 'Royal Mail';
$_['text_weight']		= 'Peso:';
$_['text_insurance']		= 'Assicurato al di sopra di:';
$_['text_special_delivery']		= 'Special Delivery Next Day';
$_['text_1st_class_signed']		= 'First Class Signed Post';
$_['text_2nd_class_signed']		= 'Second Class Signed Post';
$_['text_1st_class_standard']		= 'Posta Prima Classe Standard';
$_['text_2nd_class_standard']		= 'Posta Seconda Classe Standard';
$_['text_international_standard']		= 'International Standard';
$_['text_international_tracked_signed']		= 'International Tracked & Signed';
$_['text_international_tracked']		= 'International Tracked';
$_['text_international_signed']		= 'International Signed';
$_['text_international_economy']		= 'International Economy';
