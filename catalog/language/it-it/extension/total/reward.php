<?php
$_['heading_title']		= 'Punti Fedelt&agrave; (Disponibili %s)';
$_['text_reward']		= 'Punti Fedelt&agrave; (%s):';
$_['text_order_id']		= 'ID Ordine: #%s';
$_['text_success']		= 'I tuoi punti Fedelt&agrave; sono stati applicati!';
$_['entry_reward']		= 'Punti da usare (Max %s):';
$_['error_reward']		= 'Warning: Please enter the amount of reward points to use!';
$_['error_points']		= 'Error: Non hai %s Punti Fedelt&agrave;!';
$_['error_maximum']		= 'Errore: Il numero massimo di punti fedelt&agrave; applicabili &egrave; %s!';
