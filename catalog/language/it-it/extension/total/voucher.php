<?php
$_['heading_title']		= 'Utilizza Voucher Regalo';
$_['text_voucher']		= 'Voucher(%s):';
$_['text_success']		= 'Il tuo Voucher Regalo &egrave; stato applicato!';
$_['entry_voucher']		= 'Inserisci il codice qui:';
$_['error_voucher']		= 'Errore: Il Voucher non &egrave; valido oppure &egrave; gi&agrave; stato utilizzato!';
$_['error_empty']		= 'Warning: Please enter a gift certificate code!';
