<?php
$_['text_subject']		= '%s - Programma di Affiliazione';
$_['text_welcome']		= 'Grazie per esserti unito al Programma di Affiliazione di %s !';
$_['text_login']		= 'Account creato ed è possibile accedere utilizzando email e password da inserire nella pagina seguente:';
$_['text_approval']		= 'Il tuo account deve essere approvato prima che tu possa effettuare il login. Una volta approvato puoi effettuare il login usando il tuo indirizzo e-mail e la tua password visitando il nostro sito al seguente indirizzo:';
$_['text_service']		= 'Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.';
$_['text_thanks']		= 'Grazie,';
$_['text_new_affiliate']		= 'Nuovo Affiliato';
$_['text_signup']		= 'Un nuovo affiliato si è iscritto:';
$_['text_website']		= 'Sito Web:';
$_['text_customer_group']		= 'Customer Group:';
$_['text_firstname']		= 'Nome:';
$_['text_lastname']		= 'Cognome:';
$_['text_company']		= 'Azienda:';
$_['text_email']		= 'E-Mail:';
$_['text_telephone']		= 'Telefono:';
