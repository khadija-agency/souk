<?php
$_['heading_title']		= 'Cambia Password';
$_['text_account']		= 'Account';
$_['text_password']		= 'La Tua Password';
$_['text_success']		= 'Password modificata con successo.';
$_['entry_password']		= 'Password:';
$_['entry_confirm']		= 'Conferma Password:';
$_['error_password']		= 'La Password deve essere tra 3 e 20 caratteri!';
$_['error_confirm']		= 'La Password di conferma non corrisponde!';
