<?php
$_['heading_title']		= 'Account Download';
$_['text_account']		= 'Account';
$_['text_downloads']		= 'Download';
$_['text_empty']		= 'Non sono presenti ordini con Download!';
$_['column_order_id']		= 'ID Ordine:';
$_['column_name']		= 'Nome:';
$_['column_size']		= 'Dimensione:';
$_['column_date_added']		= 'Data Aggiunta:';
