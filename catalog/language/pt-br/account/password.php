<?php
$_['heading_title']		= 'Modificar senha';
$_['text_account']		= 'Minha conta';
$_['text_password']		= 'Preencha abaixo a sua nova senha.';
$_['text_success']		= 'Sua senha foi modifica.';
$_['entry_password']		= 'Senha';
$_['entry_confirm']		= 'Repetir a senha';
$_['error_password']		= 'A senha deve ter entre 4 e 20 caracteres.';
$_['error_confirm']		= 'A senha repetida esta errada.';
