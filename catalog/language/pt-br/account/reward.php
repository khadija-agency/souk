<?php
$_['heading_title']		= 'Pontos';
$_['column_date_added']		= 'Data';
$_['column_description']		= 'Descrição';
$_['column_points']		= 'Pontos';
$_['text_account']		= 'Minha conta';
$_['text_reward']		= 'Pontos';
$_['text_total']		= 'Saldo atual:';
$_['text_empty']		= 'Você ainda não recebeu pontos.';
