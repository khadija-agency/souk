<?php
$_['heading_title']		= 'Transações';
$_['column_date_added']		= 'Data';
$_['column_description']		= 'Descrição';
$_['column_amount']		= 'Valor (%s)';
$_['text_account']		= 'Minha conta';
$_['text_transaction']		= 'Transações';
$_['text_total']		= 'Saldo atual:';
$_['text_empty']		= 'Não há registro de transações.';
