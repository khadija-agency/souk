<?php
$_['text_success']		= 'Succès : la déduction de vos points de fidélité a bien été appliquée !';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de accéder à l’API !';
$_['error_reward']		= 'Attention : veuillez saisir le montant de points de fidélité à utiliser !';
$_['error_points']		= 'Attention : vous n’avez pas %s points de fidélité !';
$_['error_maximum']		= 'Attention : le nombre maximum de points de fidélité pouvant être utilisés est %s !';
