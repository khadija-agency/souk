<?php
$_['text_success']		= 'Succès : la réduction de votre bon a été appliquée';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de accéder à l’API !';
$_['error_coupon']		= 'Attention : le bon de réduction est invalide, a expiré ou a déjà été utilisé !';
