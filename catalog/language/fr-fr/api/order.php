<?php
$_['text_success']		= 'Votre commande a été modifiée avec succès';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de accéder à l’API !';
$_['error_customer']		= 'Attention : les détails de l’utilisateur doivent avoir été configurés';
$_['error_payment_address']		= 'Attention : une adresse de paiement est requise';
$_['error_payment_method']		= 'Attention : un moyen de paiement est requis !';
$_['error_no_payment']		= 'Attention : aucune option de paiement n’est disponible !';
$_['error_shipping_address']		= 'Attention : une adresse de livraison est requise !';
$_['error_shipping_method']		= 'Attention : un mode de livraison est requis !';
$_['error_no_shipping']		= 'Attention : aucune option d’expédition n’est disponible !';
$_['error_stock']		= 'Attention : les produits indiqués avec *** ne sont pas disponibles dans la quantité désirée ou ne sont actuellement pas en stock !';
$_['error_minimum']		= 'Attention : le montant minimum d’une commande pour %s est %s !';
$_['error_not_found']		= 'Attention : la commande ne peut pas être trouvée !';
