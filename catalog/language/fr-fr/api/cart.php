<?php
$_['text_success']		= 'Succès : vous avez correctement modifié votre panier d’achat !';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de accéder à l’API !';
$_['error_stock']		= 'Les produits indiqués avec *** ne sont pas disponibles dans la quantité désirée ou ne sont actuellement pas en stock !';
$_['error_minimum']		= 'Le montant minimum d’une commande pour %s est %s !';
$_['error_store']		= 'Le produit ne peut pas être acheté dans la boutique que vous avez choisi !';
$_['error_required']		= '%s est requis !';
