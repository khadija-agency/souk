<?php
$_['text_success']		= 'Succès : la déduction de votre bon d’achat a été appliquée !';
$_['text_cart']		= 'Succès : vous avez correctement modifié votre panier d’achat !';
$_['text_for']		= '%s bon d’achat pour %s';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de accéder à l’API !';
$_['error_voucher']		= 'Attention : le bon d’achat est soit invalide soit son solde a déjà été compensé !';
$_['error_to_name']		= 'Le nom du destinataire doit contenir entre 1 et 64 caractères';
$_['error_from_name']		= 'Votre nom doit contenir entre 1 et 64 caractères';
$_['error_email']		= 'L’adresse électronique ne semble pas être valide !';
$_['error_theme']		= 'Vous devez sélectionner un thème !';
$_['error_amount']		= 'le montant doit être compris entre %s et %s !';
