<?php
$_['text_information']		= 'Informations';
$_['text_service']		= 'Service client';
$_['text_extra']		= 'Extras';
$_['text_contact']		= 'Contact';
$_['text_return']		= 'Retours';
$_['text_sitemap']		= 'Plan du site';
$_['text_gdpr']		= 'GDPR';
$_['text_manufacturer']		= 'Fabricants';
$_['text_voucher']		= 'Bons d’achat';
$_['text_affiliate']		= 'Affiliés';
$_['text_special']		= 'Promotions';
$_['text_account']		= 'Mon compte';
$_['text_order']		= 'Historique des commandes';
$_['text_wishlist']		= 'Liste de souhaits';
$_['text_newsletter']		= 'Lettre d’informations';
$_['text_powered'] = 'Powered By <a href="https://khadija.agency">Souk</a><br /> %s &copy; %s';
