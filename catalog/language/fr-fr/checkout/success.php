<?php
$_['heading_title']		= 'Votre commande a été prise en compte !';
$_['text_basket']		= 'Panier d’achat';
$_['text_checkout']		= 'Valider la commande';
$_['text_success']		= 'Opération réussie';
$_['text_customer']		= '<p>Votre commande a été traitée avec succès!</p><p>Vous pouvez consulter l’historique de vos commande en vous rendant sur <a href="%s">votre compte</a> et en cliquant sur l’<a href="%s">historique</a>.</p><p>Si votre achat inclus un téléchargement associé, vous pouvez vous rendre dans la section <a href="%s">téléchargements</a> pour les consulter.</p><p>Veuillez adresser vos éventuelles questions au <a href="%s">propriétaire de la boutique</a>.</p><p>Merci d’avoir effectué vos achats sur notre site!</p>';
$_['text_guest']		= '<p>Votre commande a été traitée avec succès!</p><p>Veuillez adresser vos éventuelles questions au <a href="%s">propriétaire de la boutique</a>.</p><p>Merci d’avoir effectué vos achats sur notre site!</p>';
