<?php
$_['heading_title']		= 'Modifier le mot de passe';
$_['text_account']		= 'Compte';
$_['text_password']		= 'Votre mot de passe';
$_['text_success']		= 'Succès : votre mot de passe a correctement été mis à jour.';
$_['entry_password']		= 'Mot de passe';
$_['entry_confirm']		= 'Confirmer ce mot de passe';
$_['error_password']		= 'Le mot de passe doit contenir entre 4 et 20 caractères !';
$_['error_confirm']		= 'La confirmation de mot de passe ne correspond pas au mot de passe !';
