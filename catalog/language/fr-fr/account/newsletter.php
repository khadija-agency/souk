<?php
$_['heading_title']		= 'Inscription à la lettre d’informations';
$_['text_account']		= 'Compte';
$_['text_newsletter']		= 'Lettre d’informations';
$_['text_success']		= 'Succès : votre inscription à la lettre d’informations a été modifiée !';
$_['entry_newsletter']		= 'S’abonner';
