<?php
$_['heading_title']		= 'Téléchargements';
$_['text_account']		= 'Compte';
$_['text_downloads']		= 'Téléchargements';
$_['text_empty']		= 'Vous n’avez pas encore fait de commande de produits téléchargeables !';
$_['column_order_id']		= 'Commande n°';
$_['column_name']		= 'Nom';
$_['column_size']		= 'Taille';
$_['column_date_added']		= 'Date d’ajout';
