<?php
$_['heading_title']		= 'Connexion au compte';
$_['text_account']		= 'Compte';
$_['text_login']		= 'Ouvrir une session';
$_['text_new_customer']		= 'Nouveau client';
$_['text_register']		= 'Créer un compte';
$_['text_register_account']		= 'En créant un compte vous pourrez faire vos achats plus rapidement, connaître le statut de vos commandes et garder trace de vos achats précédents.';
$_['text_returning_customer']		= 'Déjà client';
$_['text_i_am_returning_customer']		= 'Je suis déjà client';
$_['text_forgotten']		= 'Mot de passe oublié';
$_['entry_email']		= 'Adresse électronique';
$_['entry_password']		= 'Mot de passe';
$_['error_login']		= 'Attention : aucune correspondance trouvée entre ce nom d’utilisateur et/ou ce mot de passe.';
$_['error_attempts']		= 'Attention : votre compte a dépassé le nombre autorisé de tentatives de connexion. Merci d’essayer à nouveau dans 1 heure.';
$_['error_approved']		= 'Attention : votre compte nécessite d’être approuvé avant que vous puissiez vous connecter.';
