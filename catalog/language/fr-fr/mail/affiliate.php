<?php
$_['text_subject']		= '%s - Programme d’affiliation';
$_['text_welcome']		= 'Merci de rejoindre le programme d’affiliation %s !';
$_['text_login']		= 'Votre compte a été créé. Vous pouvez vous connecter en utilisant votre adresse électronique et votre mot de passe sur notre site internet ou à l’adresse suivante : ';
$_['text_approval']		= 'Votre compte doit être approuvé avant que vous puissiez vous connecter. Une fois approuvé, vous pourrez vous connecter en utilisant votre adresse électronique et le mot de passe sur notre site internet ou à l’adresse suivante : ';
$_['text_service']		= 'Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.';
$_['text_thanks']		= 'Merci,';
$_['text_new_affiliate']		= 'Nouvel affilié';
$_['text_signup']		= 'Un nouvel affilié a souscrit : ';
$_['text_website']		= 'Site internet : ';
$_['text_customer_group']		= 'Customer Group:';
$_['text_firstname']		= 'Prénom : ';
$_['text_lastname']		= 'Nom de famille : ';
$_['text_company']		= 'Société : ';
$_['text_email']		= 'Adresse électronique : ';
$_['text_telephone']		= 'Numéro de téléphone : ';
