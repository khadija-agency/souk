<?php
$_['text_title']		= 'Royal Mail';
$_['text_weight']		= 'Poids :';
$_['text_insurance']		= 'Assur&eacute; &agrave; hauteur de :';
$_['text_special_delivery']		= 'Special Delivery Next Day';
$_['text_1st_class_signed']		= 'First Class Signed Post';
$_['text_2nd_class_signed']		= 'Second Class Signed Post';
$_['text_1st_class_standard']		= 'Courrier standard de premi&egrave;re classe';
$_['text_2nd_class_standard']		= 'Courrier standard de seconde classe';
$_['text_international_standard']		= 'International Standard';
$_['text_international_tracked_signed']		= 'International Tracked & Signed';
$_['text_international_tracked']		= 'International Tracked';
$_['text_international_signed']		= 'International avec signature';
$_['text_international_economy']		= 'International Economy';
