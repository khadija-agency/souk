<?php
$_['heading_title']		= 'Transações';
$_['column_date_added']		= 'Registo';
$_['column_description']		= 'Descrição';
$_['column_amount']		= 'Valor (%s)';
$_['text_account']		= 'A Minha Conta';
$_['text_transaction']		= 'Transações';
$_['text_total']		= 'Saldo:';
$_['text_empty']		= 'Não existem transações registadas!';
