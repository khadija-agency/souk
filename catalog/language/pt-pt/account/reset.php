<?php
$_['heading_title']		= 'Redefinir a Palavra-passe';
$_['text_account']		= 'Conta';
$_['text_password']		= 'Digite a nova Palavra-passe que você deseja usar.';
$_['text_success']		= 'A sua Palavra-passe foi atualizada com sucesso.';
$_['entry_password']		= 'Palavra-passe';
$_['entry_confirm']		= 'Confirmar';
$_['error_password']		= 'A Palavra-passe deve estar entre 4 e 20 caracteres!';
$_['error_confirm']		= 'A Palavra-passe e a confirmação da palavra-passe não coincidem!';
$_['error_code']		= 'O código de redefinição de palavra-passe é inválido ou foi usado anteriormente!';
