<?php
$_['heading_title']		= 'Logout';
$_['text_message']		= '<p>Saiu da sua Conta.</p> <p>O carrinho foi guardado. Será restaurado assim que voltar a entrar na sua conta.</p>';
$_['text_account']		= 'Criar conta';
$_['text_logout']		= 'Logout';
