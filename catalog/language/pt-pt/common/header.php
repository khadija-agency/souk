<?php
$_['text_wishlist']		= 'Lista de Favoritos (%s)';
$_['text_shopping_cart']		= 'Carrinho';
$_['text_account']		= 'A Minha Conta';
$_['text_register']		= 'Criar Conta';
$_['text_login']		= 'Login';
$_['text_order']		= 'Histórico de Pedidos';
$_['text_transaction']		= 'Transações';
$_['text_download']		= 'Downloads';
$_['text_logout']		= 'Logout';
