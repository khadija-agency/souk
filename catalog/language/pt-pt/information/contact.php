<?php
$_['heading_title']		= 'Contactos';
$_['text_location']		= 'Localização';
$_['text_store']		= 'Os nossos espaços de atendimento';
$_['text_contact']		= 'Formulário de contacto';
$_['text_address']		= 'Endereço';
$_['text_telephone']		= 'Telefone';
$_['text_open']		= 'Horário de funcionamento';
$_['text_comment']		= 'Comentários';
$_['text_message']		= '<p>A sua mensagem foi enviada com sucesso! Entraremos em contacto.</p>';
$_['entry_name']		= 'Nome';
$_['entry_email']		= 'Endereço de e-mail';
$_['entry_enquiry']		= 'Questão ou comentário';
$_['email_subject']		= 'Contacto - %s';
$_['error_name']		= 'O seu nome de ter entre 3 e 32 caracteres!';
$_['error_email']		= 'O e-mail não é válido!';
$_['error_enquiry']		= 'A questão ou comentário deve ter entre 10 e 3000 caracteres!';
