<?php
$_['text_success']		= 'Cupão aceite.';
$_['error_permission']		= 'Atenção: não tem permissão para aceder a API!';
$_['error_coupon']		= 'Atenção: cupão inválido, caducado, ou atingiu o seu limite de utilizações!';
