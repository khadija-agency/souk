<?php
$_['text_title']		= 'Targeta de Cr&#232;dit o D&#232;bit (Processat de forma segura per Perpetual Payments)';
$_['text_credit_card']		= 'Detalls de la Targeta de Cr&#232;dit';
$_['text_transaction']		= 'ID de Transacci&#243;:';
$_['text_avs']		= 'AVS/CVV:';
$_['text_avs_full_match']		= 'Coincid&#232;ncia Completa';
$_['text_avs_not_match']		= 'No Coincideixen';
$_['text_authorisation']		= 'Codi Autoritzaci&#243;:';
$_['entry_cc_number']		= 'No. de Targeta:';
$_['entry_cc_start_date']		= 'V&#224;lida des de:';
$_['entry_cc_expire_date']		= 'Data de Caducitat:';
$_['entry_cc_cvv2']		= 'Codi de Seguretat de la Targete (CVV2):';
$_['entry_cc_issue']		= 'No. d&#039;Edici&#243; de la Targeta (Card Issue Number):';
$_['help_start_date']		= '(if available)';
$_['help_issue']		= '(for Maestro and Solo cards only)';
