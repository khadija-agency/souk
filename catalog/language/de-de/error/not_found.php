<?php
$_['heading_title']		= 'Seite nicht gefunden';
$_['text_error']		= 'Trotz aller Anstrengungen kann die angeforderte Seite leider nicht gefunden werden.<br>Sollte das Problem weiterhin bestehen, bitte uns kontaktieren - vielen Dank.';
