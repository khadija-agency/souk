<?php
$_['heading_title']		= 'Partnerkonto wurde erstellt!';
$_['text_message']		= '<p>Gratulation! Das neue Konto im Partnernetzwerk von %s wurde erfolgreich erstellt!</p><p>Bei Fragen dazu, stehe wir gerne zur Verfügung (siehe Kontakt).</p><p>Eine Bestätigung wurde per Email an die angegebene Emailadresse gesendet, sollte das nicht angekommen sein, bitte <a href="%s">uns kontaktieren</a>.</p>';
$_['text_approval']		= '<p>Vielen Dank für die Anmeldung zum Partnerprogramm bei %s.</p><p>Sobald das Konto freigeschalten wurde, erfolgt eine Benachrichtigung per Email.</p><p>Sollten noch Fragen zum Partnerprogramm offen sein, bitte unsere <a href="%s">Verwaltung kontaktieren</a>.</p>';
$_['text_account']		= 'Partnerkonto';
$_['text_success']		= '<p>Glückwunsch - das Partnerkonto wurde erfolgreich erstellt.</p><p>Ab sofort können alle Partnervorteile in Anspruch genommen werden.</p><p>Sollten noch Fragen zum Partnerprogramm offen sein, bitte uns kontaktieren.</p><p>An die angegebene Emailadresse wurde eine Bestätigung gesendet. Sollte es nicht ankommen, bitte uns <a href="%s">kontaktieren</a></p>';
