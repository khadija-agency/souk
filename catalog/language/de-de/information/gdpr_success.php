<?php
$_['heading_title']		= 'DSGVO';
$_['text_account']		= 'Konto';
$_['text_export']		= 'A request to export your account data has been received.';
$_['text_remove']		= 'GDPR account deletion requests will process after <strong>%s days</strong> so any chargebacks, refunds or fraud detection can be processed.';
