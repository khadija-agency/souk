<?php
$_['text_title']		= 'Kreditkartenzahlung (sichere Verarbeitung durch Perpetual Payments)';
$_['text_credit_card']		= 'Kreditkartendetails';
$_['text_transaction']		= 'Buchungsnummer:';
$_['text_avs']		= 'AVS/CVV:';
$_['text_avs_full_match']		= 'Volle Übereinstimmung';
$_['text_avs_not_match']		= 'Keine Übereinstimmung';
$_['text_authorisation']		= 'Genehmigungscode:';
$_['entry_cc_number']		= 'Kartennummer:';
$_['entry_cc_start_date']		= 'Karte gültig ab:';
$_['entry_cc_expire_date']		= 'Karte gültig bis:';
$_['entry_cc_cvv2']		= 'Prüfziffer (CVV2):';
$_['entry_cc_issue']		= 'Ausgabenummer:';
$_['help_start_date']		= '(if available)';
$_['help_issue']		= '(for Maestro and Solo cards only)';
