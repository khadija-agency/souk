<?php
$_['text_subject']		= '%s - Auftrag %s wurde aktualisiert';
$_['text_order_id']		= 'Auftragsnr.';
$_['text_date_added']		= 'Datum';
$_['text_order_status']		= 'Auftrag wurde auf folgenden Status aktualisiert:';
$_['text_comment']		= 'Kommentar:';
$_['text_link']		= 'Um den Auftrag anzusehen, bitte nachstehenden Link anklicken:';
$_['text_footer']		= 'Sollten noch Fragen offen sein, bitte auf dieses Email antworten.';
