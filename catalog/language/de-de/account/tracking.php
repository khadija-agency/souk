<?php
$_['heading_title']		= 'Partner Tracking';
$_['text_account']		= 'Konto';
$_['text_description']		= 'Um Provisionen für verlinkte Artikel zugeweisen zu können, ist es notwendig der URL einen Trackingcode hinzu zufügen. Dazu kann das Werkzeug unten verwendet werden um Links zu %s erstellen.';
$_['entry_code']		= 'Mein Trackingcode';
$_['entry_generator']		= 'Trackinglink erstellen (Produktname eintippen)';
$_['entry_link']		= 'Trackinglink';
$_['help_generator']		= 'Produktname eingeben welcher verlinkt werden soll';
