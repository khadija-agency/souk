<?php
$_['heading_title']		= 'Abmeldung';
$_['text_message']		= '<p>Abmeldung erfolgreich ausgeführt.<br>Persönliche Einstellungen (z.B. Warenkorb, Wunschliste, usw.) sind bei der nächsten Anmeldung wieder verfügbar.</p>';
$_['text_account']		= 'Konto';
$_['text_logout']		= 'Abmelden';
