<?php
$_['heading_title']		= 'Bonuspunkte';
$_['column_date_added']		= 'Datum';
$_['column_description']		= 'Beschreibung';
$_['column_points']		= 'Punkte';
$_['text_account']		= 'Konto';
$_['text_reward']		= 'Bonuspunkte';
$_['text_total']		= 'Aktueller Punktestand';
$_['text_empty']		= 'Keine Bonuspunkte vorhanden';
