<?php
class ModelSettingStore extends Europa\Model {
    private $st_get_by_hostname;
	public function getStore($store_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "store` WHERE `store_id` = '" . (int)$store_id . "'");

		return $query->row;
	}

	public function getStoreByHostname(string $url): array {
        $data = array();

        if(is_null($this->st_get_by_hostname)){
            $this->st_get_by_hostname = $this->pdo->prepare(
                "SELECT * FROM " . DB_PREFIX . "store
                WHERE REGEXP_REPLACE(store_url, 'www\\.', '') = :https_server;");
        }
        $https_server = ($this->request->server['HTTPS'] ? 'https://' : 'http://');
        $https_server .= str_replace('www.', '', $this->request->server['HTTP_HOST']);
        $https_server .= rtrim(dirname($this->request->server['PHP_SELF']), '/.\\');
        $this->st_get_by_hostname->execute(array(
            'https_server' => $https_server
        ));
        if(($data = $this->st_get_by_hostname->fetch())){
            return $data;
        }
        return array();
	}

	public function getStores() {
		$store_data = $this->cache->get('store');

		if (!$store_data) {
			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "store` ORDER BY `store_url`");

			$store_data = $query->rows;

			$this->cache->set('store', $store_data);
		}

		return $store_data;
	}
}
