<?php
class ControllerStartupEvent extends Europa\Controller {
	public function index() {
		// Add events from the DB
		$this->load->model('setting/event');

		$results = $this->model_setting_event->getEvents();

		foreach ($results as $result) {
			$this->event->register(substr($result['event_trigger'], strpos($result['event_trigger'], '/') + 1), new Europa\Action($result['action']), $result['sort_order']);
		}
	}
}
