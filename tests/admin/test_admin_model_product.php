<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */


include_once("../../europa/europa.php");

define('DIR_APPLICATION', "../../admin/");

// Registry
$registry = new Europa\Data();

$config = new Europa\config();
$registry->set('config', $config);

// Database configuration data
$config->set("username", "user_souk", "database");
$config->set("password", "souk", "database");
$config->set("database", "db_souk", "database");
$config->set("hostname", "localhost", "database");
$config->set("prefix", "sk_", "database");

//$config->set("port", "5432", "database");
//$config->set("system", "pgsql", "database");
//
$config->set("port", "3306", "database");
$config->set("system", "mysql", "database");

// Language
$config->set("config_language_id", "1");

// Save config
//$config->save("config.ini");

$database = new Europa\database();
$database->set_config($config);

$registry->set('database', $database);
$registry->set('pdo', $database->get_pdo());

include("../../admin/model/catalog/product.php");

$model_product = new ModelCatalogProduct($registry);

$list = $model_product->getProducts();

echo "\nID\tName\t\tModel\n";
foreach($list as $product){
    echo $product['product_id']."\t".$product['name']."\t".$product['model']."\n";
}

echo "\nEnter product ID to view and press Enter.\n";
echo "Product ID: ";
$product_id = (int)readline();

$product = $model_product->getProduct($product_id);
$data = new Souk\Catalog\product_data();
$data->set_data($product);

var_dump($data->getModel());

//$model_product->getProducts($data = array());
//$model_product->getProductsByCategoryId($category_id);
//$model_product->getProductDescriptions($product_id);
//$model_product->getProductCategories($product_id);
//$model_product->getProductFilters($product_id);
//$model_product->getProductAttributes($product_id);
//$model_product->getProductOptions($product_id);
//$model_product->getProductOptionValue($product_id, $product_option_value_id);
//$model_product->getProductImages($product_id);
//$model_product->getProductDiscounts($product_id);
//$model_product->getProductSpecials($product_id);
//$model_product->getProductRewards($product_id);
//$model_product->getProductDownloads($product_id);
//$model_product->getProductStores($product_id);
//$model_product->getProductSeoUrls($product_id);
//$model_product->getProductLayouts($product_id);
//$model_product->getProductRelated($product_id);
//$model_product->getRecurrings($product_id);
//$model_product->getTotalProducts($data = array());
//$model_product->getTotalProductsByTaxClassId($tax_class_id);
//$model_product->getTotalProductsByStockStatusId($stock_status_id);
//$model_product->getTotalProductsByWeightClassId($weight_class_id);
//$model_product->getTotalProductsByLengthClassId($length_class_id);
//$model_product->getTotalProductsByDownloadId($download_id);
//$model_product->getTotalProductsByManufacturerId($manufacturer_id);
//$model_product->getTotalProductsByAttributeId($attribute_id);
//$model_product->getTotalProductsByOptionId($option_id);
//$model_product->getTotalProductsByProfileId($recurring_id);
//$model_product->getTotalProductsByLayoutId($layout_id);

