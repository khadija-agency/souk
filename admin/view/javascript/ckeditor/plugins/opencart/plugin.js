CKEDITOR.plugins.add('souk', {
	init: function(editor) {
		editor.addCommand('Souk', {
			exec: function(editor) {
				$('#modal-image').remove();

				$.ajax({
					url: 'index.php?route=common/filemanager&user_token=' + getURLVar('user_token') + '&ckeditor=' + editor.name,
					dataType: 'html',
					success: function(html) {
						$('body').append('<div id="modal-image" class="modal">' + html + '</div>');

						$('#modal-image').modal('show');
					}
				});
			}
		});

		editor.ui.addButton('Souk', {
			label: 'Souk',
			command: 'Souk',
			icon: this.path + 'images/icon.png'
		});
	}
});