<?php
class ModelLocalisationStockStatus extends Europa\Model {
    private $st_create_stock_status;
    private $st_create_stock_name;
    private $st_read_stock_list;
    private $st_read_stock_total;
    private $st_read_stock_status;
    private $st_read_stock_all;
    private $st_update_stock_name;
    private $st_delete_stock_status;
    private $st_delete_stock_name;
    private $st_delete_stock_name_all;

    public function addStockStatus($data) {
        if(is_null($this->st_create_stock_status)) {
            $this->st_create_stock_status = $this->pdo->prepare("INSERT INTO " . DB_PREFIX . "stock_status (stock_status_id) VALUES (NULL)");

        }

        if(is_null($this->st_create_stock_name)){
            $this->st_create_stock_name = $this->pdo->prepare("INSERT INTO ". DB_PREFIX . "stock_status_name (
                stock_status_id,
                language_id,
                name
            ) VALUES (
                :stock_status_id,
                :language_id,
                :name
            )");
        }

        if (!isset($stock_status_id)) {
            $this->st_create_stock_status->execute();
            $stock_status_id = $this->pdo->lastInsertId();
        }

        foreach ($data['stock_status'] as $language_id => $value) {
            $this->st_create_stock_name->execute(array(
                'stock_status_id' => $stock_status_id,
                'language_id'     => $language_id,
                'name'            => $value['name']
            ));

        }

        $this->cache->delete('stock_status');

        return $stock_status_id;
    }

    public function editStockStatus($stock_status_id, $data) {
        if(is_null($this->st_create_stock_name)){
            $this->st_create_stock_name = $this->pdo->prepare("INSERT INTO ". DB_PREFIX . "stock_status_name (
                stock_status_id,
                language_id,
                name
            ) VALUES (
                :stock_status_id,
                :language_id,
                :name
            )");
        }

        if(is_null($this->st_update_stock_name)){
            $this->st_update_stock_name = $this->pdo->prepare("
            UPDATE sk_stock_status_name
            SET name = :name
            WHERE stock_status_id = :stock_status_id
            AND language_id = :language_id"
            );
        }

        $stock_names = $this->getStockStatusDescriptions($stock_status_id);


        foreach ($data['stock_status'] as $language_id => $value) {
        if(isset($stock_names[$language_id])){
            $this->st_update_stock_name->execute(array(
                'name' => $value['name'],
                'stock_status_id' => $stock_status_id,
                'language_id' => $language_id
            ));
        } else {
            $this->st_create_stock_name->execute(array(
                'stock_status_id' => $stock_status_id,
                'language_id'     => $language_id,
                'name'            => $value['name']
            ));
        }
        }

        $this->cache->delete('stock_status');
    }

    public function deleteStockStatus($stock_status_id, $language_id = null) {
        // if language_id is null deletes all stock_status
        // if language is not null deletes only that language
        if(is_null($this->st_delete_stock_status)){
            $this->st_delete_stock_status = $this->pdo->prepare(
                "DELETE FROM ". DB_PREFIX ."stock_status
                WHERE stock_status_id = :stock_status_id"
                );
        }
        if(is_null($this->st_delete_stock_name)){
            $this->st_delete_stock_name = $this->pdo->prepare(
                "DELETE FROM ". DB_PREFIX ."stock_status_name
                WHERE stock_status_id = :stock_status_id
                AND language_id = :language_id;"
            );
        }

        if(is_null($this->st_delete_stock_name_all)){
            $this->st_delete_stock_name_all = $this->pdo->prepare(
                "DELETE FROM ". DB_PREFIX ."stock_status_name
                WHERE stock_status_id = :stock_status_id;"
            );
        }

        if(is_null($language_id)) {
            $this->st_delete_stock_name_all->execute(array(
                'stock_status_id' => $stock_status_id
            ));
            $this->st_delete_stock_status->execute(array(
                'stock_status_id' => $stock_status_id
            ));
        } else {
            $this->st_delete_stock_name->execute(array(
                'stock_status_id' => $stock_status_id,
                'language_id'     => $language_id
            ));
        }

        $this->cache->delete('stock_status');
    }

    public function getStockStatus($stock_status_id) {
        if(is_null($this->st_read_stock_status)) {
            $this->st_read_stock_status = $this->pdo->prepare("
                SELECT * FROM " . DB_PREFIX . "stock_status_name
                WHERE stock_status_id = :stock_status_id
                AND language_id = :language_id
            ");
        }
        $language_id = $this->config->get('config_language_id');
        $this->st_read_stock_status->execute(array(
            'stock_status_id' => $stock_status_id,
            'language_id' => $language_id
        ));
        return $this->st_read_stock->fetch();
    }

    public function getStockStatusDescriptions($stock_status_id) {
        if(is_null($this->st_read_stock_status)) {
            $this->st_read_stock_all = $this->pdo->prepare("
                SELECT * FROM " . DB_PREFIX . "stock_status_name
                WHERE stock_status_id = :stock_status_id
                ");
        }
        $this->st_read_stock_all->execute(array(
            'stock_status_id' => $stock_status_id,
        ));

        $data = $this->st_read_stock_all->fetchAll();

        $stock_names = array();

        foreach($data as $stock){
            $stock_names[$stock['language_id']]['name'] = $stock['name'];
        }

        return $stock_names;
    }

    public function getStockStatuses($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "stock_status_name WHERE language_id = :language_id";
        if(isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ORDER BY name ASC";
        } else {
            $sql .= " ORDER BY name DESC";
        }
        if(isset($data['start']) || isset($data['limit'])) {
            if(!isset($data['start']) || $data['start'] < 0) {
                $data['start'] = 0;
            }
            if(!isset($data['limit']) || $data['limit'] < 1) {
                $data['limit'] = 20;
            }
            $sql .= " LIMIT :start, :limit";
        }
        $language_id = $this->config->get('config_language_id');
        if (isset($data['start']) || isset($data['limit'])) {
            $this->st_read_stock_list = $this->pdo->prepare($sql);
            $this->st_read_stock_list->execute(array(
                'start' => $data['start'],
                'limit' => $data['limit'],
                'language_id' => $language_id
            ));
        }else{
            $this->st_read_stock_list = $this->pdo->prepare($sql);
            $this->st_read_stock_list->execute(array(
                'language_id' => $language_id
            ));
        }
        return $this->st_read_stock_list->fetchAll();
    }
    public function getTotalStockStatuses() {
        if(is_null($this->st_stock_status_total)){
            $this->st_stock_status_total = $this->pdo->prepare("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "stock_status");
        }

        $this->st_stock_status_total->execute();
        $data = $this->st_stock_status_total->fetch();
        return $data['total'];
    }
}
