<?php
class ModelUserUser extends Europa\Model {
    private $st_c_login_attempt = null;
    private $st_r_login_attempt = null;
    private $st_u_login_attempt = null;
    private $st_d_login_attempt = null;
    private $st_total_by_email = null;

    public function __construct($registry){
        parent::__construct($registry);

        $this->st_total_by_email = $this->pdo->prepare(
            "SELECT COUNT(*) AS total
            FROM " . DB_PREFIX . "user
            WHERE LCASE(email) = :email");
        $this->st_c_login_attempt = $this->pdo->prepare(
            "INSERT INTO " . DB_PREFIX . "user_login SET
            username = :username,
            ip = :ip,
            total = 1,
            date_added = :added,
            date_modified = :modified");
        $this->st_r_login_attempt = $this->pdo->prepare(
            "SELECT *
            FROM " . DB_PREFIX . "user_login
            WHERE username = :username");
        $this->st_u_login_attempt = $this->pdo->prepare(
            "UPDATE " . DB_PREFIX . "user_login SET
            total = (total + 1),
            date_modified = :date
            WHERE user_login_id = :user_id");
        $this->st_d_login_attempt = $this->pdo->prepare(
            "DELETE
            FROM " . DB_PREFIX . "user_login
            WHERE username = :username");
    }

    public function addUser($data) {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "user` SET `username` = '" . $this->db->escape($data['username']) . "', `user_group_id` = '" . (int)$data['user_group_id'] . "', `password` = '" . $this->db->escape(password_hash(html_entity_decode($data['password'], ENT_QUOTES, 'UTF-8'), PASSWORD_DEFAULT)) . "', `firstname` = '" . $this->db->escape($data['firstname']) . "', `lastname` = '" . $this->db->escape($data['lastname']) . "', `email` = '" . $this->db->escape($data['email']) . "', `image` = '" . $this->db->escape($data['image']) . "', `status` = '" . (int)$data['status'] . "', `date_added` = NOW()");

        return $this->db->getLastId();
    }

    public function editUser($user_id, $data) {
        $this->db->query("UPDATE `" . DB_PREFIX . "user` SET `username` = '" . $this->db->escape($data['username']) . "', `user_group_id` = '" . (int)$data['user_group_id'] . "', `firstname` = '" . $this->db->escape($data['firstname']) . "', `lastname` = '" . $this->db->escape($data['lastname']) . "', `email` = '" . $this->db->escape($data['email']) . "', `image` = '" . $this->db->escape($data['image']) . "', `status` = '" . (int)$data['status'] . "' WHERE `user_id` = '" . (int)$user_id . "'");

        if ($data['password']) {
            $this->db->query("UPDATE `" . DB_PREFIX . "user` SET `password` = '" . $this->db->escape(password_hash(html_entity_decode($data['password'], ENT_QUOTES, 'UTF-8'), PASSWORD_DEFAULT)) . "' WHERE `user_id` = '" . (int)$user_id . "'");
        }
    }

    public function editPassword($user_id, $password) {
        $this->db->query("UPDATE `" . DB_PREFIX . "user` SET `password` = '" . $this->db->escape(password_hash(html_entity_decode($password, ENT_QUOTES, 'UTF-8'), PASSWORD_DEFAULT)) . "', `code` = '' WHERE `user_id` = '" . (int)$user_id . "'");
    }

    public function editCode($email, $code) {
        $this->db->query("UPDATE `" . DB_PREFIX . "user` SET `code` = '" . $this->db->escape($code) . "' WHERE LCASE(`email`) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
    }

    public function deleteUser($user_id) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "user` WHERE `user_id` = '" . (int)$user_id . "'");
    }

    public function getUser($user_id) {
        $query = $this->db->query("SELECT *, (SELECT ug.`name` FROM `" . DB_PREFIX . "user_group` ug WHERE ug.`user_group_id` = u.`user_group_id`) AS `user_group` FROM `" . DB_PREFIX . "user` u WHERE u.`user_id` = '" . (int)$user_id . "'");

        return $query->row;
    }

    public function getUserByUsername($username) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE `username` = '" . $this->db->escape($username) . "'");

        return $query->row;
    }

    public function getUserByEmail($email) {
        $query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "user` WHERE LCASE(`email`) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

        return $query->row;
    }

    public function getUserByCode($code) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE `code` = '" . $this->db->escape($code) . "' AND `code` != ''");

        return $query->row;
    }

    public function getUsers($data = array()) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "user`";

        $sort_data = array(
            '`username`',
            '`status`',
            '`date_added`'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY `" . $data['sort'] . "`";
        } else {
            $sql .= " ORDER BY `username`";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getTotalUsers() {
        $query = $this->db->query("SELECT COUNT(*) AS `total` FROM `" . DB_PREFIX . "user`");

        return (int)$query->row['total'];
    }

    public function getTotalUsersByGroupId($user_group_id) {
        $query = $this->db->query("SELECT COUNT(*) AS `total` FROM `" . DB_PREFIX . "user` WHERE `user_group_id` = '" . (int)$user_group_id . "'");

        return (int)$query->row['total'];
    }

    public function getTotalUsersByEmail($email) {
        $this->st_total_email->execute(array(
            'email' => utf8_strtolower($email)
        ));

        $total = $this->st_total_email->fetch();

        return (int)$total['total'];
    }

    public function addLoginAttempt($username) {
        $attempts = $this->getLoginAttempts($username);
        if($attempts === false){
            $this->st_c_login_attempt->execute(array(
                'username'  => $username,
                'ip'        =>$this->request->server['REMOTE_ADDR'],
                'added'     =>date('Y-m-d H:i:s'),
                'modified'  =>date('Y-m-d H:i:s')
            ));
        } else {
            $this->st_u_login_attempt->execute(array(
                'date'      => date('Y-m-d H:i:s'),
                'user_id'   => $attempts['user_login_id']
            ));
        }
    }

    public function getLoginAttempts($username) {
        $this->st_r_login_attempt->execute(array(
            'username' => $username
        ));
        return $this->st_r_login_attempt->fetch();
    }

    public function deleteLoginAttempts($username) {
        $this->st_d_login_attempt->execute(array(
            'username' => $username
        ));
    }
}
