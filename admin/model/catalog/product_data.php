<?php
/**
* @package     souk
* @author      contact at khadija dot agency
* @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
* @license     https://opensource.org/license/bsd-3-clause/
* @link        https://khadija.agency
*/
namespace Souk\Catalog;

class product_data {
    protected $product_description = array();
    protected $model = '';
    protected $sku = '';
    protected $upc = '';
    protected $ean = '';
    protected $jan = '';
    protected $isbn = '';
    protected $mpn = '';
    protected $location = '';
    protected $price = '0.0000';
    protected $tax_class_id = '0';
    protected $quantity = '0';
    protected $minimum = '0';
    protected $subtract = '0';
    protected $stock_status_id = '0';
    protected $shipping = '0';
    protected $date_available = '0000-00-00';
    protected $length = '0.00000000';
    protected $width = '0.00000000';
    protected $height = '0.00000000';
    protected $length_class_id = '0';
    protected $weight = '0.00000000';
    protected $weight_class_id = '0';
    protected $status = '0';
    protected $sort_order = '0';
    protected $manufacturer = '';
    protected $manufacturer_id = '0';
    protected $category = '';
    protected $product_category = array();
    protected $filter = '';
    protected $product_store = array();
    protected $download = '';
    protected $related = '';
    protected $option = '';
    protected $image = '';
    protected $points = '0';
    protected $product_reward = array();
    protected $product_seo_url = array();
    protected $product_layout = array('', '');

    public function getProductDescription() {
        return $this->product_description;
    }

    public function getModel(){
        return $this->model;
    }

    public function getSku(){
        return $this->sku;
    }

    public function getUpc(){
        return $this->upc;
    }

    public function getEan(){
        return $this->ean;
    }

    public function getJan(){
        return $this->jan;
    }

    public function getIsbn(){
        return $this->isbn;
    }

    public function getMpn(){
        return $this->mpn;
    }

    public function getLocation(){
        return $this->location;
    }

    public function getPrice(){
        return $this->price;
    }

    public function getTaxClassId(){
        return $this->tax_class_id;
    }

    public function getQuantity(){
        return $this->quantity;
    }

    public function getMinimum(){
        return $this->minimum;
    }

    public function getSubtract(){
        return $this->subtract;
    }

    public function getStockStatusId(){
        return $this->stock_status_id;
    }

    public function getShipping(){
        return $this->shipping;
    }

    public function getDateAvailable(){
        return $this->date_available;
    }

    public function getLength(){
        return $this->length;
    }

    public function getWidth(){
        return $this->width;
    }

    public function getHeight(){
        return $this->height;
    }

    public function getLengthClassId(){
        return $this->length_class_id;
    }

    public function getWeight(){
        return $this->weight;
    }

    public function getWeightClassId(){
        return $this->weight_class_id;
    }

    public function getStatus(){
        return $this->status;
    }

    public function getSortOrder(){
        return $this->sort_order;
    }

    public function getManufacturer(){
        return $this->manufacturer;
    }

    public function getManufacturerId(){
        return $this->manufacturer_id;
    }

    public function getCategory(){
        return $this->category;
    }

    public function getProductCategory(){
        return $this->product_category;
    }

    public function getFilter(){
        return $this->filter;
    }

    public function getProductStore(){
        return $this->product_store;
    }

    public function getDownload(){
        return $this->download;
    }

    public function getRelated(){
        return $this->related;
    }

    public function getOption(){
        return $this->option;
    }

    public function getImage(){
        return $this->image;
    }

    public function getPoints(){
        return $this->points;
    }

    public function getProductReward(){
        return $this->product_reward;
    }

    public function getProductSeoUrl(){
        return $this->product_seo_url;
    }

    public function getProductLayout(){
        return $this->product_layout;
    }
    public function get_data(){
        return get_object_vars($this);
    }
    public function set_data($data) {
        foreach ($data as $property => $value) {
            // Check if the property exists in the class
            if (property_exists($this, $property)) {
                // Set the property with the provided value
                $this->$property = $value;
            }
        }
    }
}


