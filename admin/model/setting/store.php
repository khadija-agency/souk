<?php
class ModelSettingStore extends Europa\Model {
    private $st_get_stores = null;
    private $st_get_total  = null;

    public function __construct($registry){
        parent::__construct($registry);
        $pdo = $registry->get('pdo');

		$this->st_get_stores = $pdo->prepare("SELECT * FROM " . DB_PREFIX . "store ORDER BY store_url");
		$this->st_get_total  = $pdo->prepare("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "store");
    }
	public function addStore($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "store` SET `store_name` = '" . $this->db->escape($data['config_name']) . "', `store_url` = '" . $this->db->escape($data['config_url']) . "'");

		$store_id = $this->db->getLastId();

		// Layout Route
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "layout_route` WHERE `store_id` = '1'");

		foreach ($query->rows as $layout_route) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "layout_route` SET `layout_id` = '" . (int)$layout_route['layout_id'] . "', `route` = '" . $this->db->escape($layout_route['route']) . "', `store_id` = '" . (int)$store_id . "'");
		}

		$this->cache->delete('store');

		return $store_id;
	}

	public function editStore($store_id, $data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "store` SET `store_name` = '" . $this->db->escape($data['config_name']) . "', `store_url` = '" . $this->db->escape($data['config_url']) . "' WHERE `store_id` = '" . (int)$store_id . "'");

		$this->cache->delete('store');
	}

	public function deleteStore($store_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "store` WHERE `store_id` = '" . (int)$store_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "layout_route` WHERE `store_id` = '" . (int)$store_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE `store_id` = '" . (int)$store_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "seo_url` WHERE `store_id` = '" . (int)$store_id . "'");

		$this->cache->delete('store');
	}

	public function getStore($store_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "store` WHERE `store_id` = '" . (int)$store_id . "'");

		return $query->row;
	}

	public function getStores($data = array()) {
		$store_data = $this->cache->get('store');

		if (!$store_data) {
            $this->st_get_stores->execute();

			$store_data = $this->st_get_stores->fetchAll();

			$this->cache->set('store', $store_data);
		}

		return $store_data;
	}

	public function getTotalStores() {
        $this->st_get_total->execute();
        $result = $this->st_get_total->fetch();
        return $result['total'];
	}

	public function getTotalStoresByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS `total` FROM `" . DB_PREFIX . "setting` WHERE `setting_key` = 'config_layout_id' AND `value` = '" . (int)$layout_id . "' AND `store_id` != '1'");

		return (int)$query->row['total'];
	}

	public function getTotalStoresByLanguage($language) {
		$query = $this->db->query("SELECT COUNT(*) AS `total` FROM `" . DB_PREFIX . "setting` WHERE `setting_key` = 'config_language' AND `value` = '" . $this->db->escape($language) . "' AND `store_id` != '1'");

		return (int)$query->row['total'];
	}

	public function getTotalStoresByCurrency($currency) {
		$query = $this->db->query("SELECT COUNT(*) AS `total` FROM `" . DB_PREFIX . "setting` WHERE `setting_key` = 'config_currency' AND `value` = '" . $this->db->escape($currency) . "' AND `store_id` != '1'");

		return (int)$query->row['total'];
	}

	public function getTotalStoresByCountryId($country_id) {
		$query = $this->db->query("SELECT COUNT(*) AS `total` FROM `" . DB_PREFIX . "setting` WHERE `setting_key` = 'config_country_id' AND `value` = '" . (int)$country_id . "' AND `store_id` != '1'");

		return (int)$query->row['total'];
	}

	public function getTotalStoresByZoneId($zone_id) {
		$query = $this->db->query("SELECT COUNT(*) AS `total` FROM `" . DB_PREFIX . "setting` WHERE `setting_key` = 'config_zone_id' AND `value` = '" . (int)$zone_id . "' AND `store_id` != '1'");

		return (int)$query->row['total'];
	}

	public function getTotalStoresByCustomerGroupId($customer_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS `total` FROM `" . DB_PREFIX . "setting` WHERE `setting_key` = 'config_customer_group_id' AND `value` = '" . (int)$customer_group_id . "' AND `store_id` != '1'");

		return (int)$query->row['total'];
	}

	public function getTotalStoresByInformationId($information_id) {
		$account_query = $this->db->query("SELECT COUNT(*) AS `total` FROM `" . DB_PREFIX . "setting` WHERE `setting_key` = 'config_account_id' AND `value` = '" . (int)$information_id . "' AND `store_id` != '1'");

		$checkout_query = $this->db->query("SELECT COUNT(*) AS `total` FROM `" . DB_PREFIX . "setting` WHERE `setting_key` = 'config_checkout_id' AND `value` = '" . (int)$information_id . "' AND `store_id` != '1'");

		return ($account_query->row['total'] + $checkout_query->row['total']);
	}

	public function getTotalStoresByOrderStatusId($order_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS `total` FROM `" . DB_PREFIX . "setting` WHERE `setting_key` = 'config_order_status_id' AND `value` = '" . (int)$order_status_id . "' AND `store_id` != '1'");

		return (int)$query->row['total'];
	}
}
