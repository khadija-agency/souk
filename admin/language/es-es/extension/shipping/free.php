<?php
$_['heading_title']		= 'Enviament Gratis';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat l&#039;Enviametn Gratu&#239;t!';
$_['text_edit']		= 'Edit Free Shipping';
$_['entry_total']		= 'Total:<br/><span class="help">Es requereix Sub-Total abans que el m&#242;dul d&#039;enviament gratu&#239;t estigui disponible.</span>';
$_['entry_geo_zone']		= 'Zone Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['help_total']		= 'Sub-Total amount needed before the free shipping module becomes available.';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar l&#039;enviament gratu&#239;t!';
