<?php
$_['heading_title']		= 'Austr&#224;lia Post';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: S&#039;ha modificat l&#039;enviament del correu d&#039;Austr&#224;lia!';
$_['text_edit']		= 'Edit Australia Post Shipping';
$_['entry_api']		= 'API Key';
$_['entry_postcode']		= 'Codi Postal:';
$_['entry_weight_class']		= 'Pes Classe:<span class="help">S&#039;estableix en grams.</span>';
$_['entry_tax_class']		= 'Classe d&#039;Impost:';
$_['entry_geo_zone']		= 'Zone Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['help_weight_class']		= 'Set to kilograms.';
$_['error_permission']		= 'Advert&#232;ncia: No t&#233; perm&#237;s per a modificar l&#039;enviament del correu d&#039;Austr&#224;ia!';
$_['error_postcode']		= 'Codi Postal ha de ser de 4 d&#237;gits!';
