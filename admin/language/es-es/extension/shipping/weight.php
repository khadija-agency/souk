<?php
$_['heading_title']		= 'Enviament Basat en Pes';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat l&#039;enviament Basat en el Pes!';
$_['text_edit']		= 'Edit Weight Based Shipping';
$_['entry_rate']		= 'Tarifes:<br/><span class="help">Exemple: 5:10.00,7:12.00 Pes:Cost,Pes:Cost, etc..</span>';
$_['entry_tax_class']		= 'Tipus d&#039;Impost:';
$_['entry_geo_zone']		= 'Zone Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['help_rate']		= 'Example: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar l&#039;enviament basat en el Pes!';
