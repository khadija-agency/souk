<?php
$_['heading_title']		= 'Recollida a Botiga';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat Recollida a Botiga!';
$_['text_edit']		= 'Edit Pickup From Store Shipping';
$_['entry_geo_zone']		= 'Zone Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per modificar recollida a botiga!';
