<?php
$_['heading_title']		= 'Taxa de Manipulaci&#243;';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat el Cr&#232;dit Total de Taxa de Manipulaci&#243;!';
$_['text_edit']		= 'Edit Handling Fee Total';
$_['entry_total']		= 'Comandes Totals:';
$_['entry_fee']		= 'Quota:';
$_['entry_tax_class']		= 'Classe d&#039;Impost:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['help_total']		= 'The checkout total the order must reach before this order total becomes active.';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar el total de taxa de manipulaci&#243;!';
