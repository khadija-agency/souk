<?php
$_['heading_title']		= 'Punts de Fidelitat';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat el Total de Punts de Fidelitat!';
$_['text_edit']		= 'Edit Reward Points Total';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar el total de punts de fidelitat!';
