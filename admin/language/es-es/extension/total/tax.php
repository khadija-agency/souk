<?php
$_['heading_title']		= 'Impostos';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat el Total d&#039;Impostos!';
$_['text_edit']		= 'Edit Tax Total';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar el total d&#039;impostos!';
