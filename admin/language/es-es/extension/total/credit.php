<?php
$_['heading_title']		= 'Cr&#232;dit Botiga';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat el Cr&#232;dit Total a la Botiga!';
$_['text_edit']		= 'Edit Store Credit Total';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar el cr&#232;dit total a la botiga!';
