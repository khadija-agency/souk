<?php
$_['heading_title']		= 'Enviaments';
$_['text_success']		= 'Success: You have modified shipping!';
$_['column_name']		= 'Forma d&#039;Enviament:';
$_['column_status']		= 'Estat:';
$_['column_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['column_action']		= 'Acci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar enviaments!';
