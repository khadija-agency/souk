<?php
$_['heading_title']		= 'M&#242;duls';
$_['text_success']		= 'Success: You have modified modules!';
$_['text_layout']		= 'After you have installed and configured a module you can add it to a layout <a href="%s" class="alert-link">here</a>!';
$_['column_name']		= 'Nom del M&#242;dul:';
$_['column_status']		= 'Status';
$_['column_action']		= 'Acci&#243;:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar m&#242;dul!';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_code_name']		= 'Module Code must be between 3 and 32 characters!';
$_['error_code']		= 'Extension required!';
