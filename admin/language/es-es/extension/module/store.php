<?php
$_['heading_title']		= 'Botiga';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat M&#242;dul Botiga!';
$_['text_edit']		= 'Edit Store Module';
$_['entry_admin']		= 'Nom&#233;s Usuaris Admin:';
$_['entry_status']		= 'Estat:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per modificar m&#242;dul botiga!';
