<?php
$_['heading_title']		= 'Informaci&#243;';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat M&#242;dul d&#039;Informaci&#243;!';
$_['text_edit']		= 'Edit Information Module';
$_['entry_status']		= 'Estat:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per modificar el m&#242;dul informaci&#243;!';
