<?php
$_['heading_title']		= 'M&#233;s Venuts';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat el M&#242;duls de M&#233;s Venuts!';
$_['text_edit']		= 'Edit Bestsellers Module';
$_['entry_name']		= 'Module Name';
$_['entry_limit']		= 'L&#237;mit:';
$_['entry_width']		= 'Width';
$_['entry_height']		= 'Height';
$_['entry_status']		= 'Estat:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar el m&#242;dul dels m&#233;s venuts!';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_width']		= 'Width required!';
$_['error_height']		= 'Height required!';
