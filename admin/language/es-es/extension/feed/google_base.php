<?php
$_['heading_title']		= 'Google Base';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat els Feeds de Google Base!';
$_['text_edit']		= 'Edit Google Base';
$_['text_import']		= 'To download the latest Google category list by <a href="https://support.google.com/merchants/answer/160081?hl=en" target="_blank" class="alert-link">clicking here</a> and choose taxonomy with numeric IDs in Plain Text (.txt) file. Upload via the green import button.';
$_['column_google_category']		= 'Google Category';
$_['column_category']		= 'Category';
$_['column_action']		= 'Action';
$_['entry_google_category']		= 'Google Category';
$_['entry_category']		= 'Category';
$_['entry_data_feed']		= 'Dades de l&#039;URL del Feed:';
$_['entry_status']		= 'Estat:';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per a modificar Feeds de Google Base!';
$_['error_upload']		= 'File could not be uploaded!';
$_['error_filetype']		= 'Invalid file type!';
