<?php
$_['heading_title']		= 'Informe de Productes Vistos';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Products Viewed Report';
$_['text_success']		= '&#200;xit: Heu Reiniciat l&#039;Informe de Productes Vistos!';
$_['column_name']		= 'Nom del Producte:';
$_['column_model']		= 'Model:';
$_['column_viewed']		= 'Vist:';
$_['column_percent']		= 'Percentatge:';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify products viewed report!';
