<?php
$_['heading_title']		= 'SagePay (US)';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#232;xit: S&#039;han Modificat els Detalls SagePay Compte!';
$_['text_edit']		= 'Edit Sage Payment Solutions (US)';
$_['entry_merchant_id']		= 'ID del Comerciant:';
$_['entry_merchant_key']		= 'Comerciant Clau:';
$_['entry_total']		= 'Total:<br/><span class="help">El total de caixa de l&#039;ordre ha d&#039;arribar abans que aquest m&#232;tode de pagament s&#039;activa.</span>';
$_['entry_order_status']		= 'Estat de la Comanda:';
$_['entry_geo_zone']		= 'Zone Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Advert&#232;ncia: No t&#233; perm&#237;s per a modificar SagePay pagament!';
$_['error_merchant_id']		= 'ID del Comerciant es Requereix!';
$_['error_merchant_key']		= 'Clau Comerciant es Requereix!';
