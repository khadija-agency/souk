<?php
$_['heading_title']		= 'Xec / Gir';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#232;xt: Heu Modificat els Detals de Pagament Mitjan&#231;ant Xec/Gir!';
$_['text_edit']		= 'Edit Cheque / Money Order';
$_['entry_payable']		= 'Pagar A:';
$_['entry_total']		= 'Total:<br/><span class="help">El total de caixa per aquesta comanda ha d&#039;arribar abans que aquest m&#232;tode de pagament es converteixe en actiu.</span>';
$_['entry_order_status']		= 'Estat de la Comanda:';
$_['entry_geo_zone']		= 'Zona Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Advert&#232;ncia: No teniu perm&#237;s per modificar pagament per Xec/Gir!';
$_['error_payable']		= 'Per Pagar Requerit!';
