<?php
$_['heading_title']		= 'Pagaments Perpetus';
$_['text_extension']		= 'Extensions';
$_['text_success']		= '&#200;xit: Heu Modificat el Compte Pagaments Perpetus!';
$_['text_edit']		= 'Edit Perpetual Payments';
$_['entry_auth_id']		= 'ID d&#039;Autoritzaci&#243;:';
$_['entry_auth_pass']		= 'Autoritzaci&#243; Contrasenya:';
$_['entry_test']		= 'Mode de Prova:<span class="help">Utilitzeu aquest m&#242;dul a la prova (Si) o la manera de producci&#243; (No)?</span>';
$_['entry_total']		= 'Total:<br/><span class="help">El total de caixa de l&#039;ordre ha d&#039;arribar abans que aquest m&#232;tode de pagament s&#039;activa.</span>';
$_['entry_order_status']		= 'Estat de la Comanda:';
$_['entry_geo_zone']		= 'Zone Geogr&#224;fica:';
$_['entry_status']		= 'Estat:';
$_['entry_sort_order']		= 'Ordre de Classificaci&#243;:';
$_['help_test']		= 'Use this module in Test (YES) or Production mode (NO)?';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Advert&#232;ncia: No t&#233; perm&#237;s per modificar els pagaments de pagament Perpetu!';
$_['error_auth_id']		= 'ID d&#039;Autoritzaci&#243; Requerits!';
$_['error_auth_pass']		= 'Contrasenya es Requereix Autoritzaci&#243;!';
