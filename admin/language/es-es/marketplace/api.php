<?php
$_['heading_title']		= 'Souk Marketplace API';
$_['text_success']		= '¡Has modificado exitosamente la información de API!';
$_['text_signup']		= 'Por favor ingrese su información de API de Souk, puedes obtenerla <a href="https://khadija.agency/index.php?route=account/store" target="_blank" class="alert-link">aquí</a>.';
$_['entry_username']		= 'Usuario';
$_['entry_secret']		= 'Secreto';
$_['error_permission']		= 'Advertencia: No tienes permisos para modificar la API del Marketplace!';
$_['error_username']		= '¡Usuario Requerido!';
$_['error_secret']		= '¡Secreto Requerido!';
