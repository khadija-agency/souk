<?php
$_['heading_title']		= 'Subidas';
$_['text_success']		= '¡Has modificado exitosamente las subidas!';
$_['text_list']		= 'Lista de Subidas';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'NOmbre';
$_['column_filename']		= 'Nombre del Archivo';
$_['column_date_added']		= 'Fecha inicial';
$_['column_action']		= 'Acción';
$_['entry_name']		= 'Upload Name';
$_['entry_filename']		= 'Nombre del Archivo';
$_['entry_date_added']		= 'Fecha Inicial';
$_['error_permission']		= 'Warning: You do not have permission to modify uploads!';
$_['error_upload']		= 'Invalid upload!';
$_['error_file']		= 'Upload file is not found!';
