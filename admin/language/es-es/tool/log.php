<?php
$_['heading_title']		= 'Registro de Errores';
$_['text_success']		= '¡Has borrado exitosamente tu registro de errores!';
$_['text_list']		= 'Lista de Errores';
$_['error_warning']		= 'Advertencia: ¡El archivo de registro de errores %s es %s!';
$_['error_permission']		= 'Advertencia: ¡No tiene permisos para borrar el registro de errores!';
