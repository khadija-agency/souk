<?php
$_['heading_title']		= 'Reporte Online';
$_['text_extension']		= 'Extensiones';
$_['text_success']		= '¡Has modificado exitosamente el informe en línea de los clientes!';
$_['text_list']		= 'Lista de Reportes Online';
$_['text_filter']		= 'Filtro';
$_['text_guest']		= 'Invitado';
$_['column_ip']		= 'IP';
$_['column_customer']		= 'Cliente';
$_['column_url']		= 'Última Página Visitada';
$_['column_referer']		= 'Referido';
$_['column_date_added']		= 'Último Clic';
$_['column_action']		= 'Acción';
$_['entry_ip']		= 'IP';
$_['entry_customer']		= 'Cliente';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Orden de Clasificación';
$_['error_permission']		= 'Advertencia: ¡No tiene permiso para modificar el informe en línea de los clientes!';
