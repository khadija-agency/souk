<?php
$_['heading_title']		= '退货原因';
$_['text_success']		= '成功：您已修改退货原因！';
$_['text_list']		= '退货原因列表';
$_['text_add']		= '添加退货原因';
$_['text_edit']		= '修改退货原因';
$_['column_name']		= '退货原因名称';
$_['column_action']		= '行动';
$_['entry_name']		= '退货原因名称';
$_['error_permission']		= '警告：您无权修改退货原因！';
$_['error_name']		= '返回原因名称必须介于 3 到 128 个字符之间！';
$_['error_return']		= '警告：无法删除此退货原因，因为它当前已分配给 %s 退回的产品！';
