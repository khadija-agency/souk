<?php
$_['heading_title']		= '订单状态';
$_['text_success']		= '成功：您已修改订单状态！';
$_['text_list']		= '订单状态列表';
$_['text_add']		= '添加订单状态';
$_['text_edit']		= '编辑订单状态';
$_['column_name']		= '订单状态名称';
$_['column_action']		= '行动';
$_['entry_name']		= '订单状态名称';
$_['error_permission']		= '警告：您无权修改订单状态！';
$_['error_name']		= '订单状态名称必须介于 3 到 32 个字符之间！';
$_['error_default']		= '警告：此订单状态无法删除，因为它当前被指定为默认的商店订单状态！';
$_['error_download']		= '警告：此订单状态无法删除，因为它当前被指定为默认下载状态！';
$_['error_store']		= '警告：此订单状态无法删除，因为它当前已分配给 %s 商店！';
$_['error_order']		= '警告：无法删除此订单状态，因为它当前已分配给 %s 个订单！';
