<?php
$_['heading_title']		= '区域';
$_['text_success']		= '成功：您已修改区域！';
$_['text_list']		= '区域列表';
$_['text_add']		= '添加区域';
$_['text_edit']		= '编辑区';
$_['text_filter']		= 'Filter';
$_['column_name']		= '区域名称';
$_['column_code']		= '区号';
$_['column_country']		= '国家';
$_['column_action']		= '行动';
$_['entry_name']		= '区域名称';
$_['entry_code']		= '区号';
$_['entry_country']		= '国家';
$_['entry_status']		= '地位';
$_['error_permission']		= '警告：您无权修改区域！';
$_['error_name']		= '区域名称必须介于 1 到 128 个字符之间！';
$_['error_default']		= '警告：此区域无法删除，因为它当前被指定为默认存储区域！';
$_['error_store']		= '警告：无法删除此区域，因为它当前已分配给 %s 商店！';
$_['error_address']		= '警告：此区域无法删除，因为它当前已分配给 %s 通讯簿条目！';
$_['error_affiliate']		= '警告：无法删除此区域，因为它当前已分配给 %s 附属公司！';
$_['error_zone_to_geo_zone']		= '警告：无法删除此区域，因为它当前已分配给 %s 个区域到地理区域！';
