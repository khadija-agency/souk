<?php
$_['heading_title']		= 'Souk 市场 API';
$_['text_success']		= '成功：您已经修改了您的 API 信息！';
$_['text_signup']		= '请输入您可以获取的 Souk API 信息<a href="https://khadija.agency/index.php?route=account/store" target="_blank" class="alert-link">这里< /a>。';
$_['entry_username']		= '用户名';
$_['entry_secret']		= '秘密';
$_['error_permission']		= '警告：您无权修改市场 API！';
$_['error_username']		= '需要用户名！';
$_['error_secret']		= '需要秘密！';
