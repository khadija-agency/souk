<?php
$_['text_success']		= '成功：您已经修改了扩展！';
$_['text_unzip']		= '提取文件！';
$_['text_move']		= '复制文件！';
$_['text_xml']		= '应用修改！';
$_['text_remove']		= '删除临时文件！';
$_['error_permission']		= '警告：您无权修改扩展！';
$_['error_install']		= '正在安装扩展程序，请等待几秒钟，然后再尝试安装！';
$_['error_unzip']		= '无法打开 Zip 文件！';
$_['error_file']		= '找不到安装文件！';
$_['error_directory']		= '找不到安装目录！';
$_['error_code']		= '修改 XML 需要唯一的代码！';
$_['error_xml']		= '修改 %s 已被使用！';
$_['error_exists']		= '文件 %s 已经存在！';
$_['error_allowed']		= '不允许写入目录 %s！';
