<?php
$_['text_success']		= '成功：您已修改凭证！';
$_['text_subject']		= '您已收到 %s 的礼券';
$_['text_greeting']		= '恭喜，您收到了价值 %s 的礼券';
$_['text_from']		= '此礼券已由 %s 发送给您';
$_['text_message']		= '用留言说';
$_['text_redeem']		= '要兑换此礼券，请记下兑换代码 <b>%s</b>，然后单击下面的链接并购买您希望使用此礼券的产品。您可以在购物车页面输入礼券代码，然后点击结帐。';
$_['text_footer']		= '如果您有任何问题，请回复此电子邮件。';
$_['text_sent']		= '成功：礼券电子邮件已发送！';
