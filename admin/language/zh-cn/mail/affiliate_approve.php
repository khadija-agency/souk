<?php
$_['text_subject']		= '%s - 您的附属帐户已激活！';
$_['text_welcome']		= '欢迎并感谢您在 %s 注册！';
$_['text_login']		= '您的帐户现已获得批准，您可以访问我们的网站或通过以下 URL 使用您的电子邮件地址和密码登录：';
$_['text_service']		= 'Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.';
$_['text_thanks']		= '谢谢，';
$_['button_login']		= 'Login';
