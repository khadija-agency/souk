<?php
$_['heading_title']		= '优惠券主题';
$_['text_success']		= '成功：您已修改优惠券主题！';
$_['text_list']		= '优惠券主题列表';
$_['text_add']		= '添加优惠券主题';
$_['text_edit']		= '编辑优惠券主题';
$_['column_name']		= '优惠券主题名称';
$_['column_action']		= '行动';
$_['entry_name']		= '优惠券主题名称';
$_['entry_description']		= '优惠券主题说明';
$_['entry_image']		= '图片';
$_['error_permission']		= '警告：您无权修改优惠券主题！';
$_['error_name']		= '优惠券主题名称必须介于 3 到 32 个字符之间！';
$_['error_image']		= '需要图片！';
$_['error_voucher']		= '警告：此优惠券主题无法删除，因为它当前已分配给 %s 优惠券！';
