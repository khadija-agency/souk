<?php
$_['heading_title']		= '备份和恢复';
$_['text_success']		= '成功：您已成功导入数据库！';
$_['entry_progress']		= '进步';
$_['entry_export']		= '出口';
$_['tab_backup']		= '备份';
$_['tab_restore']		= '恢复';
$_['error_permission']		= '警告：您无权修改备份和还原！';
$_['error_export']		= '警告：您必须至少选择一张表来导出！';
$_['error_file']		= '找不到文件！';
