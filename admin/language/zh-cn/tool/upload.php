<?php
$_['heading_title']		= '上传';
$_['text_success']		= '成功：您已修改上传！';
$_['text_list']		= '上传列表';
$_['text_filter']		= 'Filter';
$_['column_name']		= '上传名称';
$_['column_filename']		= '文件名';
$_['column_date_added']		= '添加日期';
$_['column_action']		= '行动';
$_['entry_name']		= '上传名称';
$_['entry_filename']		= '文件名';
$_['entry_date_added']		= '添加日期';
$_['error_permission']		= '警告：您无权修改上传！';
$_['error_upload']		= '上传无效！';
$_['error_file']		= '找不到上传文件！';
