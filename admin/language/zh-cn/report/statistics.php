<?php
$_['heading_title']		= '统计数据';
$_['text_success']		= '成功：您已修改统计信息！';
$_['text_list']		= '统计列表';
$_['text_order_sale']		= '订单销售';
$_['text_order_processing']		= '订单处理';
$_['text_order_complete']		= '订单完成';
$_['text_order_other']		= '订单其他';
$_['text_return']		= 'Returns';
$_['text_customer']		= '等待批准的客户';
$_['text_affiliate']		= '等待批准的附属公司';
$_['text_product']		= '缺货产品';
$_['text_review']		= '待审核';
$_['column_name']		= '统计名称';
$_['column_value']		= '价值';
$_['column_action']		= '行动';
$_['error_permission']		= '警告：您无权修改统计信息！';
