<?php
$_['heading_title']		= '在线报告';
$_['text_extension']		= '扩展';
$_['text_success']		= '成功：您已修改客户在线报告！';
$_['text_list']		= '在线列表';
$_['text_filter']		= '筛选';
$_['text_guest']		= '来宾';
$_['column_ip']		= '知识产权';
$_['column_customer']		= '顾客';
$_['column_url']		= '访问的最后一页';
$_['column_referer']		= '参考';
$_['column_date_added']		= '最后点击';
$_['column_action']		= '行动';
$_['entry_ip']		= '知识产权';
$_['entry_customer']		= '顾客';
$_['entry_status']		= '地位';
$_['entry_sort_order']		= '排序';
$_['error_permission']		= '警告：您无权修改客户在线报告！';
