<?php
$_['heading_title']		= '行政';
$_['text_heading']		= '行政';
$_['text_login']		= '请输入您的登录信息。';
$_['text_forgotten']		= '忘记密码';
$_['entry_username']		= '用户名';
$_['entry_password']		= '密码';
$_['button_login']		= '登录';
$_['error_login']		= '用户名和/或密码不匹配。';
$_['error_token']		= '无效的令牌会话。请重新登录。';
$_['error_attempts']		= '警告：您的帐户已超过允许的登录尝试次数。请在 1 小时后重试或重置密码。';
