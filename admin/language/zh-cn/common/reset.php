<?php
$_['heading_title']		= '重置你的密码';
$_['text_password']		= '输入您要使用的新密码。';
$_['text_success']		= '成功：您的密码已成功更新。';
$_['entry_password']		= '密码';
$_['entry_confirm']		= '确认';
$_['error_password']		= '密码必须在 4 到 20 个字符之间！';
$_['error_confirm']		= '密码和密码确认不符！';
