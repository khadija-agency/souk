<?php
$_['heading_title']		= '图像管理器';
$_['text_uploaded']		= '成功：您的文件已上传！';
$_['text_directory']		= '成功：目录已创建！';
$_['text_delete']		= '成功：您的文件或目录已被删除！';
$_['entry_search']		= '搜索..';
$_['entry_folder']		= '文件夹名称';
$_['error_permission']		= '警告：权限被拒绝！';
$_['error_filename']		= '警告：文件名必须在 3 到 255 之间！';
$_['error_folder']		= '警告：文件夹名称必须介于 3 到 255 之间！';
$_['error_exists']		= '警告：同名文件或目录已存在！';
$_['error_directory']		= '警告：目录不存在！';
$_['error_filesize']		= '警告：文件大小不正确！';
$_['error_filetype']		= '警告：文件类型不正确！';
$_['error_upload']		= '警告：由于未知原因无法上传文件！';
$_['error_delete']		= '警告：您不能删除此目录！';
