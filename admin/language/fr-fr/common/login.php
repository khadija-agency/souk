<?php
$_['heading_title']		= 'Administration';
$_['text_heading']		= 'Administration';
$_['text_login']		= 'Veuillez saisir vos identifiants.';
$_['text_forgotten']		= 'Mot de passe oublié';
$_['entry_username']		= 'Nom d’utilisateur';
$_['entry_password']		= 'Mot de passe';
$_['button_login']		= 'Ouvrir une session';
$_['error_login']		= 'Attention : aucune correspondance trouvée entre ce <br/><b>nom d’utilisateur et/ou ce mot de passe</b>.';
$_['error_token']		= 'Attention : session invalide. Veuillez ouvrir une nouvelle session.';
$_['error_attempts']		= 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour or reset password.';
