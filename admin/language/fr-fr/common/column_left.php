<?php
$_['text_affiliate']		= 'Affiliés';
$_['text_api']		= 'API';
$_['text_attribute']		= 'Attributs';
$_['text_attribute_group']		= 'Groupe d’attributs';
$_['text_backup']		= 'Sauvegarde et restauration';
$_['text_banner']		= 'Bannières';
$_['text_catalog']		= 'Catalogue';
$_['text_cron']		= 'Cron Jobs';
$_['text_category']		= 'Catégories';
$_['text_country']		= 'Pays';
$_['text_coupon']		= 'Bons de réduction';
$_['text_currency']		= 'Devises';
$_['text_customer']		= 'Clients';
$_['text_customer_group']		= 'Groupes de client';
$_['text_customer_online']		= 'Customer Online';
$_['text_customer_approval']		= 'Customer Approvals';
$_['text_custom_field']		= 'Champs personnalisés';
$_['text_dashboard']		= 'Tableau de bord';
$_['text_design']		= 'Mise en page';
$_['text_download']		= 'Téléchargements';
$_['text_log']		= 'Journaux d’erreurs';
$_['text_event']		= 'Événements';
$_['text_extension']		= 'Extensions';
$_['text_filter']		= 'Filtres';
$_['text_geo_zone']		= 'Zones géographiques';
$_['text_gdpr']		= 'GDPR';
$_['text_information']		= 'Information';
$_['text_installer']		= 'Installateur d’extensions';
$_['text_language']		= 'Langues';
$_['text_language_editor']		= 'Language Editor';
$_['text_layout']		= 'Gabarits';
$_['text_localisation']		= 'Localisation';
$_['text_location']		= 'Emplacement';
$_['text_contact']		= 'Contact';
$_['text_marketing']		= 'Marketing';
$_['text_marketplace']		= 'Marketplace';
$_['text_maintenance']		= 'Maintenance';
$_['text_menu']		= 'Gestionnaire de menu';
$_['text_modification']		= 'Modifications';
$_['text_manufacturer']		= 'Fabricants';
$_['text_navigation']		= 'Navigation';
$_['text_option']		= 'Options';
$_['text_order']		= 'Commandes';
$_['text_order_status']		= 'Statuts de commande';
$_['text_online']		= 'Who\'s Online';
$_['text_product']		= 'Produits';
$_['text_reports']		= 'Rapports';
$_['text_review']		= 'Évaluations';
$_['text_return']		= 'Retours';
$_['text_return_action']		= 'Actions de retour';
$_['text_return_reason']		= 'Motifs de retour';
$_['text_return_status']		= 'Statuts des retours';
$_['text_sale']		= 'Ventes';
$_['text_setting']		= 'Paramètres';
$_['text_seo_url']		= 'SEO URL';
$_['text_statistics']		= 'Statistics';
$_['text_stock_status']		= 'Statuts des stocks';
$_['text_system']		= 'Système';
$_['text_tax']		= 'Taxes';
$_['text_tax_class']		= 'Classes de taxes';
$_['text_tax_rate']		= 'Taux de taxe';
$_['text_translation']		= 'Éditeur linguistique';
$_['text_theme']		= 'Éditeur de thème';
$_['text_upload']		= 'Transferts de fichiers';
$_['text_users']		= 'Utilisateurs';
$_['text_user_group']		= 'Groupes d’utilisateurs';
$_['text_voucher']		= 'Bons d’achat';
$_['text_voucher_theme']		= 'Thèmes des bons d’achat';
$_['text_weight_class']		= 'Unités de poids';
$_['text_length_class']		= 'Unités de longueur';
$_['text_zone']		= 'Domaines';
$_['text_recurring']		= 'Profils récurrents';
$_['text_order_recurring']		= 'Commandes récurrentes';
$_['text_complete_status']		= 'Commandes complètes';
$_['text_processing_status']		= 'Commandes en cours';
$_['text_other_status']		= 'Autres statuts';
