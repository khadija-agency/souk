<?php
$_['heading_title']		= 'Souk Marketplace API';
$_['text_success']		= 'Success: You have modified your API information!';
$_['text_signup']		= 'Please enter your Souk API information which you can obtain <a href="https://khadija.agency/index.php?route=account/store" target="_blank" class="alert-link">here</a>.';
$_['entry_username']		= 'Username';
$_['entry_secret']		= 'Secret';
$_['error_permission']		= 'Warning: You do not have permission to modify marketplace API!';
$_['error_username']		= 'Username required!';
$_['error_secret']		= 'Secret required!';
