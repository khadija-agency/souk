<?php
$_['heading_title']		= 'Statuts de stock';
$_['text_success']		= 'Vous venez de modifier les statuts de stock !';
$_['text_list']		= 'Liste des statuts de stock';
$_['text_add']		= 'Ajouter un statut de stock';
$_['text_edit']		= 'Modifier un statut de stock';
$_['column_name']		= 'Nom de l’état de stock';
$_['column_action']		= 'Action';
$_['entry_name']		= 'Nom de l’état de stock';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les statuts de stock !';
$_['error_name']		= 'Le nom de l’état de stock doit contenir entre 3 et 32 caractères !';
$_['error_product']		= 'Attention : cet statut de stock ne peut être supprimé car il est actuellement attribué à %s produits !';
