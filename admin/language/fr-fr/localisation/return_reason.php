<?php
$_['heading_title']		= 'Motifs de retour';
$_['text_success']		= 'Vous venez de modifier les motifs de retour !';
$_['text_list']		= 'Liste des gabarits';
$_['text_add']		= 'Ajouter un motif de retour';
$_['text_edit']		= 'Modifier un motif de retour';
$_['column_name']		= 'Motif de retour';
$_['column_action']		= 'Action';
$_['entry_name']		= 'Nom du motif de retour';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les motifs de retour !';
$_['error_name']		= 'Le nom du motif doit contenir entre 3 et 128 caractères !';
$_['error_return']		= 'Attention : ce motif de retour ne peut être supprimé car il est actuellement attribué à %s produits retournés !';
