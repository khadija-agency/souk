<?php
$_['heading_title']		= 'Emplacements de boutiques';
$_['text_success']		= 'Succès : vous avez modifié les emplacements de boutiques !';
$_['text_list']		= 'Liste d’emplacements de boutiques';
$_['text_add']		= 'Ajouter un emplacement de boutique';
$_['text_edit']		= 'Modifier un emplacement de boutique';
$_['text_geocode']		= 'Géocode n’a pas réussi pour la raison suivante : ';
$_['column_name']		= 'Nom de la boutique';
$_['column_address']		= 'Adresse';
$_['column_action']		= 'Action';
$_['entry_name']		= 'Nom de la boutique';
$_['entry_address']		= 'Adresse';
$_['entry_geocode']		= 'Géocode';
$_['entry_telephone']		= 'Téléphone';
$_['entry_image']		= 'Image';
$_['entry_open']		= 'Horaires d’ouverture';
$_['entry_comment']		= 'Commentaire';
$_['help_geocode']		= 'Veuillez saisir votre emplacement de boutique manuellement.';
$_['help_open']		= 'Renseignez les horaires d’ouverture de votre boutique.';
$_['help_comment']		= 'Ce champ est dédié aux notes spéciales que vous souhaitez indiquer au client, par exemple si votre boutique n’accepte pas les chèques.';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les emplacements de boutiquess !';
$_['error_name']		= 'Le nom de la boutique doit contenir entre 3 et 32 caractères !';
$_['error_address']		= 'L’adresse doit contenir entre 3 et 128 caractères !';
$_['error_telephone']		= 'Le numéro de téléphone doit contenir entre 3 et 32 caractères !';
