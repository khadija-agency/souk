<?php
$_['heading_title']		= 'Actions de retour';
$_['text_success']		= 'Vous venez de modifier les actions de retour !';
$_['text_list']		= 'Liste des actions de retour';
$_['text_add']		= 'Ajouter une action de retour';
$_['text_edit']		= 'Modifier une action de retour';
$_['column_name']		= 'Action de retour';
$_['column_action']		= 'Action';
$_['entry_name']		= 'Nom de l’action de retour';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les actions de retour !';
$_['error_name']		= 'Le nom de l’action de retour doit contenir entre 3 et 64 caractères !';
$_['error_return']		= 'Attention : cette action de retour ne peut être supprimée car elle est actuellement attribuée à %s produits retournés !';
