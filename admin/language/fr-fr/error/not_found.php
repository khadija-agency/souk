<?php
$_['heading_title']		= 'Page non trouvée !';
$_['text_not_found']		= 'La page que vous recherchez n’a pas pu être trouvée ! Veuillez contacter votre administrateur si le problème persiste.';
