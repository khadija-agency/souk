<?php
$_['heading_title']		= 'Transfert de fichiers';
$_['text_success']		= 'Succès : vous avez modifié le fichier transféré !';
$_['text_list']		= 'Liste des transferts';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'Nom du transfert';
$_['column_filename']		= 'Nom du fichier';
$_['column_date_added']		= 'Date d’ajout';
$_['column_action']		= 'Action';
$_['entry_name']		= 'Nom du transfert';
$_['entry_filename']		= 'Nom du fichier';
$_['entry_date_added']		= 'Date d’ajout';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de modifier les transferts de fichiers !';
$_['error_upload']		= 'Invalid upload!';
$_['error_file']		= 'Upload file is not found!';
