<?php
$_['heading_title']		= 'Journal des erreurs';
$_['text_success']		= 'Succès : vous avez nettoyé votre journal des erreurs';
$_['text_list']		= 'Liste des erreurs';
$_['error_warning']		= 'Attention : votre journal des erreurs %s est %s !';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de supprimer les messages d’erreurs !';
