<?php
$_['heading_title']		= 'Rapport des bons de réduction';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Coupons Report';
$_['text_success']		= 'Success: You have modified coupon report!';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'Nom';
$_['column_code']		= 'Code';
$_['column_orders']		= 'Commandes';
$_['column_total']		= 'Total';
$_['column_action']		= 'Action';
$_['entry_date_start']		= 'Date de début';
$_['entry_date_end']		= 'Date de fin';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify coupon report!';
