<?php
$_['heading_title']		= 'Rapport des produits consultés';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Products Viewed Report';
$_['text_success']		= 'Succès : vous avez réinitialisé le rapport des produits consultés !';
$_['column_name']		= 'Nom du produit';
$_['column_model']		= 'Référence';
$_['column_viewed']		= 'Vu';
$_['column_percent']		= 'Pourcentage';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Attention : vous n’avez pas l’autorisation de réinitialiser le rapport des produits consultés !';
