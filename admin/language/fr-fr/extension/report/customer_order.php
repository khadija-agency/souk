<?php
$_['heading_title']		= 'Rapport des commandes de clients';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Customer Orders Report';
$_['text_success']		= 'Success: You have modified customer orders report!';
$_['text_filter']		= 'Filter';
$_['text_all_status']		= 'Tous les statuts';
$_['column_customer']		= 'Nom du client';
$_['column_email']		= 'Adresse électronique';
$_['column_customer_group']		= 'Groupe de clients';
$_['column_status']		= 'Statut';
$_['column_orders']		= 'Commandes n°';
$_['column_products']		= 'Produit n°';
$_['column_total']		= 'Total';
$_['column_action']		= 'Action';
$_['entry_date_start']		= 'Date de début';
$_['entry_date_end']		= 'Date de fin';
$_['entry_customer']		= 'Client';
$_['entry_order_status']		= 'Order Status';
$_['entry_status']		= 'Statut de la commande';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify customer orders report!';
