<?php
$_['heading_title']		= 'Frais de manutention';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; les <b>Frais de manutention</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Handling Fee Total';
$_['entry_total']		= 'Total commande :';
$_['entry_fee']		= 'Frais :';
$_['entry_tax_class']		= 'Classe de taxe :';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['help_total']		= 'The checkout total the order must reach before this order total becomes active.';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier les <b>Frais de manutention</b> !';
