<?php
$_['heading_title']		= 'Livraison';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; la <b>Livraison</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Shipping Total';
$_['entry_estimator']		= 'Co&ucirc;t estimatif de livraison :';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier la <b>Livraison</b> !';
