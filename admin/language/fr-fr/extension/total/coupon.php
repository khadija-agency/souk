<?php
$_['heading_title']		= 'Bons de r&eacute;duction';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; les <b>Bons de r&eacute;duction</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Coupon';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier les <b>Bons de r&eacute;duction</b> !';
