<?php
$_['heading_title']		= 'Ch&egrave;ques-cadeaux';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; les <b>Ch&egrave;ques-cadeaux</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Gift Voucher Total';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier les <b>Ch&egrave;ques-cadeaux</b> !';
