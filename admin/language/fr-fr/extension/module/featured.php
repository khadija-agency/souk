<?php
$_['heading_title']		= 'En Vedette';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; le module <b>En Vedette</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Featured Module';
$_['entry_name']		= 'Module Name';
$_['entry_product']		= 'Produits :';
$_['entry_limit']		= 'Limite :';
$_['entry_width']		= 'Width';
$_['entry_height']		= 'Height';
$_['entry_status']		= '&Eacute;tat';
$_['help_product']		= '(Autocomplete)';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le module <b>En Vedette</b> !';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_width']		= 'Width required!';
$_['error_height']		= 'Height required!';
