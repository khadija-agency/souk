<?php
$_['heading_title']		= 'Informations';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; le module <b>Meilleures Ventes</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Information Module';
$_['entry_status']		= '&Eacute;tat';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le module <b>Informations</b> !';
