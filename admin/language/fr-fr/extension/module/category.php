<?php
$_['heading_title']		= 'Cat&eacute;gories';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; le module <b>Cat&eacute;gories</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Category Module';
$_['entry_status']		= '&Eacute;tat';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le module <b>Cat&eacute;gories</b> !';
