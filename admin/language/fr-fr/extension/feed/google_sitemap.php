<?php
$_['heading_title']		= 'Google Sitemap';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; le flux <b>Google Sitemap</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Google Sitemap';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_data_feed']		= 'Adresse URL des donn&eacute;es du flux:';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le flux <b>Google Sitemap</b> !';
