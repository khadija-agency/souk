<?php
$_['heading_title']		= 'Google Base';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; le flux <b>Google Base</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Google Base';
$_['text_import']		= 'To download the latest Google category list by <a href="https://support.google.com/merchants/answer/160081?hl=en" target="_blank" class="alert-link">clicking here</a> and choose taxonomy with numeric IDs in Plain Text (.txt) file. Upload via the green import button.';
$_['column_google_category']		= 'Google Category';
$_['column_category']		= 'Category';
$_['column_action']		= 'Action';
$_['entry_google_category']		= 'Google Category';
$_['entry_category']		= 'Category';
$_['entry_data_feed']		= 'Adresse URL des donn&eacute;es du flux :<br/><span class="help">Cette URL pointe vers votre flux. Collez-le dans votre serveur de flux.</span>';
$_['entry_status']		= '&Eacute;tat :';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement <b>Google Base feed<:b> !';
$_['error_upload']		= 'File could not be uploaded!';
$_['error_filetype']		= 'Invalid file type!';
