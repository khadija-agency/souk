<?php
$_['heading_title']		= 'Parcelforce 48';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; la livraison <b>Parcelforce 48</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Parcelforce 48 Shipping';
$_['entry_rate']		= 'Tarifs de Parcelforce 48 :<br /><span class="help">Entrez des valeurs jusqu&#8217;&agrave; 5 chiffres apr&egrave;s la virgule (12345,67) Exemple : .1:1, .25:1.27 - Poids inf&eacute;rieur ou &eacute;gal &agrave; 0,1 kg co&ucirc;te 1,00, poids inf&eacute;rieur ou &eacute;gal &agrave; 0,25 g mais plus de 0,1 kg co&ucirc;te 1.27. Ne pas entrer KG ou des symboles.</span>';
$_['entry_insurance']		= 'Parcelforce48 Compensation Rates:<br /><span class="help">Enter values upto 5,2 decimal places. (12345.67) Example: 34:0,100:1,250:2.25 - Insurance cover for cart values upto 34 would cost 0.00 extra, those values more than 100 and upto 250 will cost 2.25 extra. Do not enter currency symbols.</span>';
$_['entry_display_weight']		= 'Afficher le poids de livraison :<br /><span class="help">Voulez-vous afficher le poids d&#8217;exp&eacute;dition ? (par exemple :  Poids de livraison : 2,7674 kgs )</span>';
$_['entry_display_insurance']		= 'Afficher l&#8217;assurance :<br /><span class="help">Voulez-vous afficher l&#8217;assurance ? (par exemple : Assur&eacute; jusqu&#8217;&agrave; 500)</span>';
$_['entry_display_time']		= 'Afficher le d&eacute;lai de livraison :<br /><span class="help">Voulez-vous afficher le d&eacute;lai de livraison ? (par exemple : Livraison entre 3 et 5 jours)</span>';
$_['entry_tax_class']		= 'Classe de taxes :';
$_['entry_geo_zone']		= 'Zone g&eacute;ographique :';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['help_rate']		= 'Enter values upto 5,2 decimal places. (12345.67) Example: .1:1,.25:1.27 - Weights less than or equal to 0.1Kg would cost 1.00, Weights less than or equal to 0.25g but more than 0.1Kg will cost 1.27. Do not enter KG or symbols.';
$_['help_insurance']		= 'Enter values upto 5,2 decimal places. (12345.67) Example: 34:0,100:1,250:2.25 - Insurance cover for cart values upto 34 would cost 0.00 extra, those values more than 100 and upto 250 will cost 2.25 extra. Do not enter currency symbols.';
$_['help_display_weight']		= 'Do you want to display the shipping weight? (e.g. Delivery Weight : 2.7674 kg)';
$_['help_display_insurance']		= 'Do you want to display the shipping insurance? (e.g. Insured upto 500)';
$_['help_display_time']		= 'Do you want to display the shipping time? (e.g. Ships within 3 to 5 days)';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier la livraison <b>Parcelforce 48</b> !';
