<?php
$_['heading_title']		= 'Commande gratuite';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'F&eacute;licitations, vous avez modifi&eacute; les d&eacute;tails de la <b>Commande gratuite</b> avec succ&egrave;s !';
$_['text_edit']		= 'Edit Free Checkout';
$_['entry_order_status']		= '&Eacute;tat de la commande :';
$_['entry_status']		= '&Eacute;tat :';
$_['entry_sort_order']		= 'Classement :';
$_['error_permission']		= 'Attention, vous n&#8217;avez pas la permission de modifier le paiement <bCommande gratuite</b> !';
