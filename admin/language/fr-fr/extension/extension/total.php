<?php
$_['heading_title']		= 'Total commande';
$_['text_success']		= 'Success: You have modified totals!';
$_['column_name']		= 'Total commande';
$_['column_status']		= '&Eacute;tat';
$_['column_sort_order']		= 'Classement';
$_['column_action']		= 'Action';
$_['error_permission']		= 'Attention, vous n&#8217;avez la permission de modifier le <b>Total commande</b> !';
