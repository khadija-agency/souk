<?php
$_['heading_title']		= 'Modules';
$_['text_success']		= 'Success: You have modified modules!';
$_['text_layout']		= 'After you have installed and configured a module you can add it to a layout <a href="%s" class="alert-link">here</a>!';
$_['column_name']		= 'Nom du module';
$_['column_status']		= 'Status';
$_['column_action']		= 'Action';
$_['error_permission']		= 'Attention, vous n&#8217;avez la permission de modifier les <b>Modules</b> !';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_code_name']		= 'Module Code must be between 3 and 32 characters!';
$_['error_code']		= 'Extension required!';
