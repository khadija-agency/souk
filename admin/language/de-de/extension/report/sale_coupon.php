<?php
$_['heading_title']		= 'Bericht Aktionsgutschein';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Coupons Report';
$_['text_success']		= 'Success: You have modified coupon report!';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'Bezeichnung';
$_['column_code']		= 'Code';
$_['column_orders']		= 'Aufträge';
$_['column_total']		= 'Summe';
$_['column_action']		= 'Aktion';
$_['entry_date_start']		= 'Startdatum:';
$_['entry_date_end']		= 'Ablaufdatum:';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify coupon report!';
