<?php
$_['heading_title']		= 'Umsatzbericht';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Sales Report';
$_['text_success']		= 'Success: You have modified sales report!';
$_['text_filter']		= 'Filter';
$_['text_year']		= 'Jahre';
$_['text_month']		= 'Monate';
$_['text_week']		= 'Wochen';
$_['text_day']		= 'Tage';
$_['text_all_status']		= 'Alle Stati';
$_['column_date_start']		= 'Startdatum';
$_['column_date_end']		= 'Ablaufdatum';
$_['column_orders']		= 'Anzahl Aufträge';
$_['column_products']		= 'Anzahl Produkte';
$_['column_tax']		= 'Steuer';
$_['column_total']		= 'Summe';
$_['entry_date_start']		= 'Startdatum:';
$_['entry_date_end']		= 'Ablaufdatum:';
$_['entry_group']		= 'Gruppieren nach:';
$_['entry_order_status']		= 'Order Status';
$_['entry_status']		= 'Auftragsstatus:';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify sales report!';
