<?php
$_['heading_title']		= 'Australia Post';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Sie haben das Modul Australia Post geändert!';
$_['text_edit']		= 'Edit Australia Post Shipping';
$_['entry_api']		= 'API Key';
$_['entry_postcode']		= 'Post Code:';
$_['entry_weight_class']		= 'Gewichtseinheit:<span class="help">Bitte auf Gramm einstellen.</span>';
$_['entry_tax_class']		= 'Steuerklasse:';
$_['entry_geo_zone']		= 'Geo Zone:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['help_weight_class']		= 'Set to kilograms.';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um das Modul Australia Post zu ändern!';
$_['error_postcode']		= 'Post Code muss 4 Zeichen lang sein!';
