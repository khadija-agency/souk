<?php
$_['heading_title']		= 'Pro Stück';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Versand pro Stück erfolgreich geändert!';
$_['text_edit']		= 'Edit Per Item Shipping';
$_['entry_cost']		= 'Kosten:';
$_['entry_tax_class']		= 'Steuerklasse:';
$_['entry_geo_zone']		= 'Geo Zone:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Pro Stück zu ändern!';
