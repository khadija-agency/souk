<?php
$_['heading_title']		= 'Gutschrift';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Gutschrift erfolgreich geändert!';
$_['text_edit']		= 'Edit Store Credit Total';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Gutschriften zu bearbeiten!';
