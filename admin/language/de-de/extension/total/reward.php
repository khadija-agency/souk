<?php
$_['heading_title']		= 'Bonuspunkte';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Bonuspunkte erfolgreich geändert!';
$_['text_edit']		= 'Edit Reward Points Total';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Bonuspunkten zu bearbeiten!';
