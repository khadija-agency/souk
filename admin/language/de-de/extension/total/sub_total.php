<?php
$_['heading_title']		= 'Zwischensumme';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Zwischensumme erfolgreich geändert!';
$_['text_edit']		= 'Edit Sub-Total Total';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Zwischensumme zu ändern!';
