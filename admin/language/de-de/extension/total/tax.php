<?php
$_['heading_title']		= 'Steuern';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Steuern erfolgreich geändert!';
$_['text_edit']		= 'Edit Tax Total';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Steuern zu ändern!';
