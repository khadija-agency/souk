<?php
$_['heading_title']		= 'Informationen';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Modul Informationen erfolgreich geändert!';
$_['text_edit']		= 'Edit Information Module';
$_['entry_status']		= 'Status:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um das Modul Informationen zu ändern!';
