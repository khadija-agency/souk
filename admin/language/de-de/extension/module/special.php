<?php
$_['heading_title']		= 'Angebote';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Modul Angebote erfolgreich geändert!';
$_['text_edit']		= 'Edit Specials Module';
$_['entry_name']		= 'Module Name';
$_['entry_limit']		= 'Limit:';
$_['entry_width']		= 'Width';
$_['entry_height']		= 'Height';
$_['entry_status']		= 'Status:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um das Modul Angebote zu ändern!';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_width']		= 'Width required!';
$_['error_height']		= 'Height required!';
