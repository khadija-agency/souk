<?php
$_['heading_title']		= 'Empfehlungen';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Modul Empfehlungen erfolgreich geändert!';
$_['text_edit']		= 'Edit Featured Module';
$_['entry_name']		= 'Module Name';
$_['entry_product']		= 'Produkte:<br /><span class="help">(Autocomplete)</span>';
$_['entry_limit']		= 'Limit:';
$_['entry_width']		= 'Width';
$_['entry_height']		= 'Height';
$_['entry_status']		= 'Status:';
$_['help_product']		= '(Autocomplete)';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um das Modul Empfehlungen zu ändern!';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_width']		= 'Width required!';
$_['error_height']		= 'Height required!';
