<?php
$_['heading_title']		= 'Kategorie';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Modul Kategorie erfolgreich geändert!';
$_['text_edit']		= 'Edit Category Module';
$_['entry_status']		= 'Status:';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um das Modul Kategorien zu ändern!';
