<?php
$_['heading_title']		= 'Nachnahme';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Erfolgreich: Nachnahme erfolgreich geändert!';
$_['text_edit']		= 'Edit Cash On Delivery';
$_['entry_total']		= 'Summe:<br /><span class="help">Der Warenkorb muss diese Summe beinhalten, damit dieses Zahlungsverfahren verfügbar ist.</span>';
$_['entry_order_status']		= 'Auftragsstatus:';
$_['entry_geo_zone']		= 'Geo Zone:';
$_['entry_status']		= 'Status:';
$_['entry_sort_order']		= 'Reihenfolge:';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Nachnahme zu ändern!';
