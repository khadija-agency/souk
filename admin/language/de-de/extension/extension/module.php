<?php
$_['heading_title']		= 'Module';
$_['text_success']		= 'Success: You have modified modules!';
$_['text_layout']		= 'After you have installed and configured a module you can add it to a layout <a href="%s" class="alert-link">here</a>!';
$_['column_name']		= 'Bezeichnung';
$_['column_status']		= 'Status';
$_['column_action']		= 'Aktion';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Module zu ändern!';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_code_name']		= 'Module Code must be between 3 and 32 characters!';
$_['error_code']		= 'Extension required!';
