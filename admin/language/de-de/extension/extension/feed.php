<?php
$_['heading_title']		= 'Produktfeeds';
$_['text_success']		= 'Success: You have modified feeds!';
$_['column_name']		= 'Bezeichnung';
$_['column_status']		= 'Status';
$_['column_action']		= 'Aktion';
$_['error_permission']		= 'Warnung: Sie haben keine Berechtigung, um Produktfeeds zu ändern!';
