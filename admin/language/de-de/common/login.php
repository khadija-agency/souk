<?php
$_['heading_title']		= 'Verwaltung';
$_['text_heading']		= 'Verwaltung';
$_['text_login']		= 'Anmeldung Verwaltung';
$_['text_forgotten']		= 'Passwort vergessen';
$_['entry_username']		= 'Benutzername';
$_['entry_password']		= 'Passwort';
$_['button_login']		= 'Anmelden';
$_['error_login']		= 'Benutzername und/oder Passwort falsch';
$_['error_token']		= 'Sitzung ist abgelaufen - bitte nochmal anmelden';
$_['error_attempts']		= 'Hinweis: das Konto hat die maximal erlaubten Anmeldeversuche überschritten! Entweder in 1 Stunde nochmal probieren oder Passwort zurücksetzen.';
