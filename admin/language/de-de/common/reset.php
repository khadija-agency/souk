<?php
$_['heading_title']		= 'Passwort zurücksetzen';
$_['text_password']		= 'Neues Passwort angeben (zw. 4 &amp; 20 Zeichen)';
$_['text_success']		= 'Passwort wurde erfolgreich aktualisiert';
$_['entry_password']		= 'Passwort';
$_['entry_confirm']		= 'Wiederholung';
$_['error_password']		= 'Passwort muss zwischen 4 und 20 Zeichen lang sein';
$_['error_confirm']		= 'Passwort und Wiederholung stimmen nicht überein';
