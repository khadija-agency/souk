<?php
$_['text_subject']		= '%s - DSGVO Anfrage genehmigt';
$_['text_request']		= 'Account Deletion Request';
$_['text_hello']		= 'Guten Tag <strong>%s</strong>';
$_['text_user']		= 'User';
$_['text_gdpr']		= 'Anfrage zur Kontolöscung';
$_['text_q']		= 'F. Warum wird es nnicht sofort gelöscht?';
$_['text_a']		= 'A. Kontolöscungen erfolgen binnen <strong>%s Tagen</strong> um mögliche ausstehende Transaktionen durchführen zu können.';
$_['text_delete']		= 'Nach erfolgter Löschung wird eine Benachichtigung zugesendet.';
$_['text_thanks']		= 'Vielen Dank,';
