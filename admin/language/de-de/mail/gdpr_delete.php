<?php
$_['text_subject']		= '%s - Anfrage nach DSGVO wurde bearbeitet';
$_['text_request']		= 'Account Deletion Request';
$_['text_hello']		= 'Guten Tag <strong>%s</strong>';
$_['text_user']		= 'User';
$_['text_delete']		= 'Your GDPR data deletion request has now been completed.';
$_['text_contact']		= 'Für weitere Informationen sind wir hier erreichbar:';
$_['text_thanks']		= 'Vielen Dank,';
$_['button_contact']		= 'Uns kontaktieren';
