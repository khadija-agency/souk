<?php
$_['heading_title']		= 'Cronjobs';
$_['text_success']		= 'Einstellungen erfolgreich ebarbeitet';
$_['text_instruction']		= 'Anleitung';
$_['text_list']		= 'Übersicht';
$_['text_cron_1']		= 'Cronjobs sind wiederkehrende Arbeiten welche nach einem eingestellten Zeitplan automatisch Aufgaben erledigen.<br>In der Regel werden damit Scripte am Server aufgerufen um z.B. Sicherungen vorzunehmen, Währungen zu aktualisieren, usw.<br>Mehr dazu auf <a href="https://de.wikipedia.org/wiki/Cron" target="_blank" title="Cronjob / Crontab auf Wikipedia" class="alert-link">Wikipedia</a> und der <a href="https://wiki.khadija.agency/souk/extension/cron/" target="_blank" class="alert-link" title="Souk Dokumentation (in Englisch)">Souk Dokumentation</a>';
$_['text_cron_2']		= 'Zum Abrufen des Cronjobs die Zeit z.B. auf jede Stunde einstellen';
$_['text_hour']		= 'Stunde';
$_['text_day']		= 'Tag';
$_['text_month']		= 'Monat';
$_['column_code']		= 'Code';
$_['column_cycle']		= 'Zyklus';
$_['column_status']		= 'Status';
$_['column_date_added']		= 'Erstellt';
$_['column_date_modified']		= 'Bearbeitet';
$_['column_action']		= 'Aktion';
$_['entry_cron']		= 'Cron URL';
$_['error_permission']		= 'Keine Berechtigung für diese Aktion';
