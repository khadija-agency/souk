<?php
$_['heading_title']		= 'Amministrazione';
$_['text_heading']		= 'Amministrazione';
$_['text_login']		= 'Inserisci i tuoi dati di accesso.';
$_['text_forgotten']		= 'Password dimenticata';
$_['entry_username']		= 'Nome Utente';
$_['entry_password']		= 'Password';
$_['button_login']		= 'Accedi';
$_['error_login']		= 'Nome Utente o Password Errati';
$_['error_token']		= 'Sessione scaduta. Si prega di accedere nuovamente.';
$_['error_attempts']		= 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour or reset password.';
