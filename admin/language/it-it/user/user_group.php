<?php
$_['heading_title']		= 'Gruppo utente';
$_['text_success']		= 'Gruppi utenti modificati correttamente!';
$_['text_list']		= 'Gruppo Utente';
$_['text_add']		= 'Aggiungi Gruppo Utente';
$_['text_edit']		= 'Modifica Gruppo Utente';
$_['column_name']		= 'Nome gruppo';
$_['column_action']		= 'Azione';
$_['entry_name']		= 'Nome gruppo:';
$_['entry_access']		= 'Permessi di accesso:';
$_['entry_modify']		= 'Permessi di modifica:';
$_['error_permission']		= 'Attenzione: Non si hanno i permessi per modificare i gruppi di utenti!';
$_['error_name']		= 'Il nome del gruppo deve essere lungo almeno 3 caratteri e non pi&ugrave; di 64!';
$_['error_user']		= 'Attenzione: Questo gruppo non pu&ograve; essere cancellato perch&eacute; &egrave; assegnato a %s utenti!';
