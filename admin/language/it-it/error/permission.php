<?php
$_['heading_title']		= 'Permesso negato!';
$_['text_permission']		= 'Non si hanno i permessi per accedere a questa pagina. Si prega di contattare l&apos;amministratore.';
