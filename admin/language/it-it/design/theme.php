<?php
$_['heading_title']		= 'Editor del Tema';
$_['text_success']		= 'Hai modificato i temi con successo!';
$_['text_edit']		= 'Modifica Tema';
$_['text_store']		= 'Seleziona Negozio';
$_['text_template']		= 'Seleziona Template';
$_['text_default']		= 'Prefinito';
$_['text_history']		= 'Theme History';
$_['text_twig']		= 'The theme editor uses the template language Twig. You can read about <a href="https://twig.symfony.com/doc/" target="_blank" class="alert-link">Twig syntax here</a>.';
$_['column_store']		= 'Store';
$_['column_route']		= 'Route';
$_['column_theme']		= 'Theme';
$_['column_date_added']		= 'Date Added';
$_['column_action']		= 'Action';
$_['error_permission']		= 'Attenzione: Non si hanno i permessi per modificare l&apos;editor del tema!';
$_['error_twig']		= 'Warning: You can only save .twig files!';
