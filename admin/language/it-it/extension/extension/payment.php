<?php
$_['heading_title']		= 'Pagamento';
$_['text_success']		= 'Success: You have modified payments!';
$_['column_name']		= 'Metodo di pagamento';
$_['column_status']		= 'Stato';
$_['column_sort_order']		= 'Ordinamento';
$_['column_action']		= 'Azione';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare i metodi di pagamento!';
