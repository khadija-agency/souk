<?php
$_['heading_title']		= 'Totale ordine';
$_['text_success']		= 'Success: You have modified totals!';
$_['column_name']		= 'Totale ordine';
$_['column_status']		= 'Stato';
$_['column_sort_order']		= 'Ordinamento';
$_['column_action']		= 'Azione';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare il totale ordine!';
