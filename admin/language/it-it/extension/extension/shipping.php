<?php
$_['heading_title']		= 'Spedizione';
$_['text_success']		= 'Success: You have modified shipping!';
$_['column_name']		= 'Metodo di spedizione';
$_['column_status']		= 'Stato';
$_['column_sort_order']		= 'Ordinamento';
$_['column_action']		= 'Azione';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare i metodi di spedizione!';
