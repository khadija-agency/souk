<?php
$_['heading_title']		= 'Categorie';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato il modulo categorie con successo!';
$_['text_edit']		= 'Edit Category Module';
$_['entry_status']		= 'Stato:';
$_['error_permission']		= 'Attenzione: non hai il permesso di modificare il modulo categorie!';
