<?php
$_['heading_title']		= 'Prodotti in evidenza';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato il modulo Prodotti in evidenza con successo!';
$_['text_edit']		= 'Edit Featured Module';
$_['entry_name']		= 'Module Name';
$_['entry_product']		= 'Prodotti:';
$_['entry_limit']		= 'Limit';
$_['entry_width']		= 'Width';
$_['entry_height']		= 'Height';
$_['entry_status']		= 'Stato:';
$_['help_product']		= '(Autocomplete)';
$_['error_permission']		= 'Attenzione: non hai il permesso di modificare il modulo Prodotti in evidenza!';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_width']		= 'Width required!';
$_['error_height']		= 'Height required!';
