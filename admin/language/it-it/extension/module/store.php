<?php
$_['heading_title']		= 'Negozio';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato il modulo negozio con successo!';
$_['text_edit']		= 'Edit Store Module';
$_['entry_admin']		= 'Solo Admin:';
$_['entry_status']		= 'Stato:';
$_['error_permission']		= 'Attenzione: non hai il permesso di modificare il modulo negozio!';
