<?php
$_['heading_title']		= 'Offerte Speciali';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato il modulo Offerte Speciali con successo!';
$_['text_edit']		= 'Edit Specials Module';
$_['entry_name']		= 'Module Name';
$_['entry_limit']		= 'Limite:';
$_['entry_width']		= 'Width';
$_['entry_height']		= 'Height';
$_['entry_status']		= 'Stato:';
$_['error_permission']		= 'Attenzione: non hai il permesso di modificare il modulo Offerte Speciali!';
$_['error_name']		= 'Module Name must be between 3 and 64 characters!';
$_['error_width']		= 'Width required!';
$_['error_height']		= 'Height required!';
