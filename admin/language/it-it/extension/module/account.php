<?php
$_['heading_title']		= 'Account';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato il modulo account con successo!';
$_['text_edit']		= 'Edit Account Module';
$_['entry_status']		= 'Stato:';
$_['error_permission']		= 'Attenzione: non hai il permesso di modificare il modulo account!';
