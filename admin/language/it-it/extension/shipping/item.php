<?php
$_['heading_title']		= 'Per Oggetto';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato con successo il modulo prezzo spedizione per oggetto!';
$_['text_edit']		= 'Edit Per Item Shipping';
$_['entry_cost']		= 'Costo:';
$_['entry_tax_class']		= 'Tax Class';
$_['entry_geo_zone']		= 'Zona geografica:';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordina per:';
$_['error_permission']		= 'Attenzione:non hai i permessi per modificare il modulo prezzo spedizione per oggetto!';
