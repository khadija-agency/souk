<?php
$_['heading_title']		= 'Totale';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Totale modificato con successo!';
$_['text_edit']		= 'Edit Total Total';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare il totale!';
