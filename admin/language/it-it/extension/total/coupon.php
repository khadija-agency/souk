<?php
$_['heading_title']		= 'Buono sconto';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Buoni sconto modificati con successo!';
$_['text_edit']		= 'Edit Coupon';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare i buoni sconto nel totale ordine!';
