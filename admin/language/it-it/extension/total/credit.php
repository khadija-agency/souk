<?php
$_['heading_title']		= 'Credito';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato Credito con successo!';
$_['text_edit']		= 'Edit Store Credit Total';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['error_permission']		= 'Attenzione: Non hai il permesso di modificare Credito!';
