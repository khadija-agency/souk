<?php
$_['heading_title']		= 'Tasse';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Tasse modificate con successo!';
$_['text_edit']		= 'Edit Tax Total';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare le tasse!';
