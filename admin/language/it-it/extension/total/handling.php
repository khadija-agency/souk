<?php
$_['heading_title']		= 'Contributo gestione ordine';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Supplemento per contributo gestione ordine modificato con successo!';
$_['text_edit']		= 'Edit Handling Fee Total';
$_['entry_total']		= 'Totale supplemento:';
$_['entry_fee']		= 'Supplemento:';
$_['entry_tax_class']		= 'Tax Class';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['help_total']		= 'The checkout total the order must reach before this order total becomes active.';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare il contributo gestione ordine!';
