<?php
$_['heading_title']		= 'Rapporto Prodotti Visualizzati';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Products Viewed Report';
$_['text_success']		= 'Rapporto Prodotti Visualizzati reimpostato con successo!';
$_['column_name']		= 'Nome Prodotto';
$_['column_model']		= 'Modello';
$_['column_viewed']		= 'Visualizzato';
$_['column_percent']		= 'Percento';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Attenzione: Non si hanno i permessi per reimpostare i report sui prodotti visualizzati!';
