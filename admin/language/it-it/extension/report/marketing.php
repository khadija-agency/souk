<?php
$_['heading_title']		= 'Rapporto Marketing';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Marketing Report';
$_['text_success']		= 'Success: You have modified marketing report!';
$_['text_filter']		= 'Filter';
$_['text_all_status']		= 'Tutti gli stati';
$_['column_campaign']		= 'Nome Campagna';
$_['column_code']		= 'Codice';
$_['column_clicks']		= 'Click';
$_['column_orders']		= 'Nr. ordini';
$_['column_total']		= 'Totale';
$_['entry_date_start']		= 'Data Inizio';
$_['entry_date_end']		= 'Data Fine';
$_['entry_order_status']		= 'Order Status';
$_['entry_status']		= 'Stato ordine';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify marketing report!';
