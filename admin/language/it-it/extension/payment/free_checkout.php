<?php
$_['heading_title']		= 'Acquisto gratuito';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato con successo il modulo acquisto gratuito!';
$_['text_edit']		= 'Edit Free Checkout';
$_['entry_order_status']		= 'Stato Ordine:';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordinamento:';
$_['error_permission']		= 'Attenzione: non hai i permessi per modificare il modulo acquisto gratuito!';
