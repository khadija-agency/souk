<?php
$_['heading_title']		= 'Assegno/Vaglia/Postepay';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Hai modificato con successo il modulo Assegno/Vaglia/Postepay!';
$_['text_edit']		= 'Edit Cheque / Money Order';
$_['entry_payable']		= 'Paga a:';
$_['entry_total']		= 'Totale:<br /><span class="help">Il totale che deve essere raggiunto per abilitare questo metodo di pagamento.</span>';
$_['entry_order_status']		= 'Stato Ordine:';
$_['entry_geo_zone']		= 'Zona Geografica:';
$_['entry_status']		= 'Stato:';
$_['entry_sort_order']		= 'Ordina per:';
$_['help_total']		= 'The checkout total the order must reach before this payment method becomes active.';
$_['error_permission']		= 'Attenzione: Non hai i permessi necessari per modificare il modulo Assegno/Vaglia/Postepay!';
$_['error_payable']		= 'Il campo Paga a &eacute; richiesto!';
