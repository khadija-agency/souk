<?php
$_['heading_title']		= 'Backup / Ripristino';
$_['text_success']		= 'Database importato con successo!';
$_['entry_progress']		= 'Progress';
$_['entry_export']		= 'Esporta';
$_['tab_backup']		= 'Backup';
$_['tab_restore']		= 'Restore';
$_['error_permission']		= 'Attenzione: Non si hanno i permessi per modificare i backup!';
$_['error_export']		= 'Attenzione: Selezionare almeno una tabella da esportare!';
$_['error_file']		= 'File could not be found!';
