<?php
$_['heading_title']		= 'Log degli Errori';
$_['text_success']		= 'Log degli Errori ripulito con successo!';
$_['text_list']		= 'Elenco Errori';
$_['error_warning']		= 'Attenzione: Il file di log %s &egrave; %s!';
$_['error_permission']		= 'Attenzione: Non si hanno i permessi per cancellare pulire il file di log!';
