<?php
$_['heading_title']		= 'Stato Magazzino';
$_['text_success']		= 'Stati del Magazzino modificati con successo!';
$_['text_list']		= 'Elenco Stato del Magazzino';
$_['text_add']		= 'Aggiungi Stato del Magazzino';
$_['text_edit']		= 'Modifica Stato del Magazzino';
$_['column_name']		= 'Nome Stato Magazzino';
$_['column_action']		= 'Azione';
$_['entry_name']		= 'Nome Stato Magazzino:';
$_['error_permission']		= 'Attenzione: Non si hanno i permessi per modificare gli stati del magazzino';
$_['error_name']		= 'Il nome dello stato del magazzino deve essere tra 3 e 32 caratteri!';
$_['error_product']		= 'Attenzione: questo stato del magazzino non pu&ograve; essere cancellato perch&eacute; &egrave; assegnato a %s ordini!';
