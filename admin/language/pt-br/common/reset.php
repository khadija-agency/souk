<?php
$_['heading_title']		= 'Redefinir sua senha';
$_['text_password']		= 'Digite a nova senha que deseja utilizar.';
$_['text_success']		= 'Sua senha foi redefinida com sucesso.';
$_['entry_password']		= 'Nova senha:';
$_['entry_confirm']		= 'Repetir a senha:';
$_['error_password']		= 'A nova senha deve ter entre 4 e 20 caracteres!';
$_['error_confirm']		= 'A senha repetida está errada!';
