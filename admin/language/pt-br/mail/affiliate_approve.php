<?php
$_['text_subject']		= '%s - Sua conta de afiliado foi aprovada!';
$_['text_welcome']		= 'Obrigado por se cadastrar na loja %s!';
$_['text_login']		= 'Sua conta foi aprovada, agora você pode acessar sua conta utilizando seu e-mail e sua senha através de nossa loja:';
$_['text_service']		= 'Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.';
$_['text_thanks']		= 'Atenciosamente,';
$_['button_login']		= 'Login';
