<?php
$_['heading_title']		= 'Página não encontrada!';
$_['text_not_found']		= 'A página que você está procurando não pode ser encontrada! Entre em contato com o administrador da loja caso o problema persistir.';
