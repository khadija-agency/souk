<?php
$_['heading_title']		= 'Usuários online';
$_['text_extension']		= 'Extensões';
$_['text_success']		= 'Usuários online atualizado com sucesso!';
$_['text_list']		= 'Online';
$_['text_filter']		= 'Filtros';
$_['text_guest']		= 'Visitante';
$_['column_ip']		= 'IP';
$_['column_customer']		= 'Cliente';
$_['column_url']		= 'Última URL';
$_['column_referer']		= 'Referência';
$_['column_date_added']		= 'Último clique';
$_['column_action']		= 'Ação';
$_['entry_ip']		= 'IP';
$_['entry_customer']		= 'Cliente';
$_['entry_status']		= 'Situação';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Você não tem permissão para atualizar os usuários online!';
