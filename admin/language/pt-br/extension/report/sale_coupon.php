<?php
$_['heading_title']		= 'Relatório de cupões';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Coupons Report';
$_['text_success']		= 'Success: You have modified coupon report!';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'Cupão';
$_['column_code']		= 'Código';
$_['column_orders']		= 'Pedidos';
$_['column_total']		= 'Total';
$_['column_action']		= 'Ação';
$_['entry_date_start']		= 'Data inicial';
$_['entry_date_end']		= 'Data final';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify coupon report!';
