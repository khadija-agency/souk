<?php
$_['heading_title']		= 'Relatório de produtos visualizados';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Products Viewed Report';
$_['text_success']		= 'Visualizações redefinidas!';
$_['column_name']		= 'Produto';
$_['column_model']		= 'Modelo';
$_['column_viewed']		= 'Visualizações';
$_['column_percent']		= 'Porcentagem';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Atenção: Você não tem permissão para acessar o relatório de produtos visualizados!';
