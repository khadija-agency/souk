<?php
$_['heading_title']		= 'Menu de informações';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Módulo menu de informações modificado com sucesso!';
$_['text_edit']		= 'Configurações do módulo menu de informações';
$_['entry_status']		= 'Estado';
$_['error_permission']		= 'Atenção: Você não tem permissão para modificar o módulo menu de informações!';
