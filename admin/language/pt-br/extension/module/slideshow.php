<?php
$_['heading_title']		= 'Slideshow de Imagens';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Módulo de slideshow de imagens modificado com sucesso!';
$_['text_edit']		= 'Configurações do módulo de slideshow de imagens';
$_['entry_name']		= 'Módulo';
$_['entry_banner']		= 'Banner';
$_['entry_width']		= 'Largura';
$_['entry_height']		= 'Altura';
$_['entry_status']		= 'Estado';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o módulo de slideshow de imagens!';
$_['error_name']		= 'O módulo deve ter entre 3 e 64 caracteres!';
$_['error_width']		= 'A largura é obrigatória!';
$_['error_height']		= 'A altura é obrigatória!';
