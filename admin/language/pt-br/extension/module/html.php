<?php
$_['heading_title']		= 'Bloco HTML livre';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Módulo bloco HTML livre modificado com sucesso!';
$_['text_edit']		= 'Configurações do módulo bloco HTML livre';
$_['entry_name']		= 'Módulo';
$_['entry_title']		= 'Título';
$_['entry_description']		= 'Conteúdo';
$_['entry_status']		= 'Estado';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o módulo bloco HTML livre!';
$_['error_name']		= 'O módulo deve ter entre 3 e 64 caracteres!';
