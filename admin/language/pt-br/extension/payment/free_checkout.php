<?php
$_['heading_title']		= 'Pagamento grátis';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Pagamento grátis modificado com sucesso!';
$_['text_edit']		= 'Configurações do Pagamento grátis';
$_['entry_order_status']		= 'Situação do pedido';
$_['entry_status']		= 'Situação';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Você não tem permissão para modificar o Pagamento grátis!';
