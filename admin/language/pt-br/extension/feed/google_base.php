<?php
$_['heading_title']		= 'Google Merchante Center';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Módulo Google Merchante Center modificado com sucesso!';
$_['text_edit']		= 'Alterar Google Merchante Center';
$_['text_import']		= 'To download the latest Google category list by <a href="https://support.google.com/merchants/answer/160081?hl=en" target="_blank" class="alert-link">clicking here</a> and choose taxonomy with numeric IDs in Plain Text (.txt) file. Upload via the green import button.';
$_['column_google_category']		= 'Google Category';
$_['column_category']		= 'Category';
$_['column_action']		= 'Action';
$_['entry_google_category']		= 'Google Category';
$_['entry_category']		= 'Category';
$_['entry_data_feed']		= 'URL';
$_['entry_status']		= 'Estado';
$_['error_permission']		= 'Atenção: Não possui permissão para modificar o módulo Google Merchante Center!';
$_['error_upload']		= 'File could not be uploaded!';
$_['error_filetype']		= 'Invalid file type!';
