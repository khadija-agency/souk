<?php
$_['heading_title']		= 'Google Sitemap';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Módulo Google Sitemap modificado com sucesso!';
$_['text_edit']		= 'Alterar Google Sitemap';
$_['entry_status']		= 'Situação';
$_['entry_data_feed']		= 'URL';
$_['error_permission']		= 'Atenção: Não possui permissão para modificar o módulo Google Sitemap!';
