<?php
$_['heading_title']		= 'Taxa para pequenos pedidos';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Taxa para pequenos pedidos modificado com sucesso!';
$_['text_edit']		= 'Configurações da Taxa para pequenos pedidos';
$_['entry_total']		= 'Total mínimo';
$_['entry_fee']		= 'Taxa';
$_['entry_tax_class']		= 'Classe de impostos';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['help_total']		= 'O valor mínimo que o total do pedido deve alcançar para que a taxa seja aplicada.';
$_['error_permission']		= 'Atenção: Você não tem permissão para modificar a Taxa para pequenos pedidos!';
