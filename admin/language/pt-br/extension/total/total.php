<?php
$_['heading_title']		= 'Total';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Total modificado com sucesso!';
$_['text_edit']		= 'Configurações do total';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Você não tem permissão para modificar o total!';
