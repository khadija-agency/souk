<?php
$_['heading_title']		= 'Impostos';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Impostos modificados com sucesso!';
$_['text_edit']		= 'Configurações dos impostos';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Você não tem permissão para modificar os impostos!';
