<?php
$_['heading_title']		= 'Expedição';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Expedição modificada com sucesso!';
$_['text_edit']		= 'Configurações da Expedição';
$_['entry_estimator']		= 'Calcular expedição';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o Frete!';
