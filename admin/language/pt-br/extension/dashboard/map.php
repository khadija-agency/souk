<?php
$_['heading_title']		= 'Mapa mundo';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Success: You have modified dashboard map!';
$_['text_edit']		= 'Edit Dashboard Map';
$_['text_order']		= 'Pedidos';
$_['text_sale']		= 'Vendas';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['entry_width']		= 'Width';
$_['error_permission']		= 'Warning: You do not have permission to modify dashboard map!';
