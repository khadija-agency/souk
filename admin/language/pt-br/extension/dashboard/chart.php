<?php
$_['heading_title']		= 'Gráfico de vendas';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Success: You have modified dashboard chart!';
$_['text_edit']		= 'Edit Dashboard Chart';
$_['text_order']		= 'Pedidos';
$_['text_customer']		= 'Clientes';
$_['text_day']		= 'Hoje';
$_['text_week']		= 'Semanal';
$_['text_month']		= 'Mensal';
$_['text_year']		= 'Anual';
$_['entry_status']		= 'Status';
$_['entry_sort_order']		= 'Sort Order';
$_['entry_width']		= 'Width';
$_['error_permission']		= 'Warning: You do not have permission to modify dashboard chart!';
