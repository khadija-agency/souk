<?php
$_['heading_title']		= 'Atributos';
$_['text_success']		= 'Atributo modificado com sucesso!';
$_['text_list']		= 'Listando atributos';
$_['text_add']		= 'Novo atributo';
$_['text_edit']		= 'Editando atributo';
$_['column_name']		= 'Atributo';
$_['column_attribute_group']		= 'Grupo de atributos';
$_['column_sort_order']		= 'Posição';
$_['column_action']		= 'Ação';
$_['entry_name']		= 'Atributo';
$_['entry_attribute_group']		= 'Grupo de atributos';
$_['entry_sort_order']		= 'Posição';
$_['error_warning']		= 'Warning: Please check the form carefully for errors!';
$_['error_permission']		= 'Atenção: Você não tem permissão para modificar os atributos!';
$_['error_attribute_group']		= 'Grupo de atributos é obrigatório!';
$_['error_name']		= 'Atributo deve ter entre 1 e 64 caracteres!';
$_['error_product']		= 'Atenção: Este atributo não pode ser excluído, pois está vinculado a %s produtos!';
