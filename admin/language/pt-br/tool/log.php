<?php
$_['heading_title']		= 'Log de erros';
$_['text_success']		= 'Seu log de erros foi completamente apagado!';
$_['text_list']		= 'Listando erros';
$_['error_warning']		= 'Atenção: O arquivo %s possui o tamanho de %s!';
$_['error_permission']		= 'Atenção: Você não tem permissão para limpar o log de ​erros!';
