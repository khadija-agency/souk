<?php
$_['heading_title']		= 'Backup / Restaurar';
$_['text_success']		= 'A importação do banco de dados foi concluída com sucesso!';
$_['entry_progress']		= 'Progresso';
$_['entry_export']		= 'Exportar';
$_['tab_backup']		= 'Backup';
$_['tab_restore']		= 'Restaurar';
$_['error_permission']		= 'Atenção: Você não tem permissão para realizar Backup / Restaurar!';
$_['error_export']		= 'Atenção: Você deve selecionar pelo menos uma tabela para exportar!';
$_['error_file']		= 'O arquivo para importação não foi encontrado!';
