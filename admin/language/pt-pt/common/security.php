<?php
$_['heading_title']		= 'Notificação de segurança importante!';
$_['text_success']		= 'A pasta de armazenamento foi modificada com sucesso!';
$_['text_security']		= 'É muito importante que mova a directoria de armazenamento fora do directoria da web (por exemplo, public_html, www ou htdocs).';
$_['text_choose']		= 'Escolha como mover a diretório de armazenamento';
$_['text_automatic']		= 'Mover Automáticamente';
$_['text_manual']		= 'Mover manualmente';
$_['text_move']		= 'Mover';
$_['text_to']		= 'para';
$_['text_config']		= 'Editar configuração.php mudar';
$_['text_admin']		= 'Editar admin / config.php e alterar';
$_['text_by']		= 'by';
$_['entry_directory']		= 'Directory';
$_['button_move']		= 'Mover';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o directoria storage!';
$_['error_path']		= 'Atenção: Path inválido!';
$_['error_directory']		= 'Atenção: Directoria inválida!';
$_['error_exists']		= 'Atenção: A Directoria já existe!';
$_['error_writable']		= 'Atenção: config.php and admin/config.php precisam ser gravados!';
