<?php
$_['heading_title']		= 'Administração';
$_['text_heading']		= 'Administração';
$_['text_login']		= 'Introduza os seus dados';
$_['text_forgotten']		= 'Esqueceu-se da palavra-passe?';
$_['entry_username']		= 'Utilizador';
$_['entry_password']		= 'Palavra-passe';
$_['button_login']		= 'Login';
$_['error_login']		= 'Os dados de acesso não são válidos.';
$_['error_token']		= 'O token de sessão é inválido. Tente aceder novamente.';
$_['error_attempts']		= 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour or reset password.';
