<?php
$_['heading_title']		= 'Logs';
$_['text_success']		= 'Apagou os erros do log com sucesso!';
$_['text_list']		= 'Lista de Erros';
$_['error_warning']		= 'Atenção: O seu ficheiro de log %s é %s!';
$_['error_permission']		= 'Atenção: Não tem permissão para apagar a lista de erros!';
