<?php
$_['heading_title']		= 'Uploads';
$_['text_success']		= 'Upload modificado com sucesso!';
$_['text_list']		= 'Lista de Uploads';
$_['text_filter']		= 'Filter';
$_['column_name']		= 'Upload';
$_['column_filename']		= 'Ficheiro';
$_['column_date_added']		= 'Adicionado';
$_['column_action']		= 'Ação';
$_['entry_name']		= 'Upload';
$_['entry_filename']		= 'Ficheiro';
$_['entry_date_added']		= 'Adicionado';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar os uploads!';
$_['error_upload']		= 'Invalid upload!';
$_['error_file']		= 'Upload file is not found!';
