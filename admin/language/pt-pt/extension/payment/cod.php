<?php
$_['heading_title']		= 'Pagamento na entrega';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Pagamento na entrega modificado com sucesso!';
$_['text_edit']		= 'Configurações do pagamento na entrega';
$_['entry_total']		= 'Total mínimo';
$_['entry_order_status']		= 'Estado do pedido';
$_['entry_geo_zone']		= 'Região geográfica';
$_['entry_status']		= 'Situação';
$_['entry_sort_order']		= 'Posição';
$_['help_total']		= 'O valor mínimo que o total do pedido para que o pagamento na entrega fique disponível para seleção pelo cliente.';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o Pagamento na entrega!';
