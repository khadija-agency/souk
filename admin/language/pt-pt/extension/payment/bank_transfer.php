<?php
$_['heading_title']		= 'Transferência bancária';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Pagamento por transferência bancária modificado com sucesso!';
$_['text_edit']		= 'Configurações do pagamento por transferência bancária';
$_['entry_bank']		= 'Instruções para transferência ou depósito';
$_['entry_total']		= 'Total mínimo';
$_['entry_order_status']		= 'Estado do pedido';
$_['entry_geo_zone']		= 'Região geográfica';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['help_total']		= 'O valor mínimo que o total do pedido para que o pagamento por transferência bancária fique disponível para seleção pelo cliente.';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o pagamento por transferência bancária!';
$_['error_bank']		= 'As instruções para transferência ou depósito são obrigatórias!';
