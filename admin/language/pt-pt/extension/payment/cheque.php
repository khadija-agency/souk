<?php
$_['heading_title']		= 'Cheque';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Pagamento por cheque modificado com sucesso!';
$_['text_edit']		= 'Configurações do pagamento por cheque';
$_['entry_payable']		= 'Nominal';
$_['entry_total']		= 'Total mínimo';
$_['entry_order_status']		= 'Estado do pedido';
$_['entry_geo_zone']		= 'Região geográfica';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['help_total']		= 'O valor mínimo que o total do pedido para que o pagamento por cheque fique disponível para seleção pelo cliente.';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o pagamento por cheque!';
$_['error_payable']		= 'O campo Nominal é obrigatório!';
