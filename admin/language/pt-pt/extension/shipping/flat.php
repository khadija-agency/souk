<?php
$_['heading_title']		= 'Expedição com taxa fixa';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Expedição com taxa fixa modificado com sucesso!';
$_['text_edit']		= 'Configurações da expedição com taxa fixa';
$_['entry_cost']		= 'Valor fixo';
$_['entry_tax_class']		= 'Classe de impostos';
$_['entry_geo_zone']		= 'Região geográfica';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar a expedição com taxa fixa!';
