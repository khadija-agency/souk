<?php
$_['heading_title']		= 'Expedição por item';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Expedição por item modificado com sucesso!';
$_['text_edit']		= 'Configurações da expedição por item';
$_['entry_cost']		= 'Valor por item';
$_['entry_tax_class']		= 'Classe de impostos';
$_['entry_geo_zone']		= 'Região geográfica';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar a expedição por item!';
