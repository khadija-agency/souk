<?php
$_['heading_title']		= 'Levantar no estabelecimento';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Levantar no estabelecimento modificado com sucesso!';
$_['text_edit']		= 'Configurações do levantamento no estabelecimento';
$_['entry_geo_zone']		= 'Região geográfica';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o levantamento no estabelecimento!';
