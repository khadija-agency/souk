<?php
$_['heading_title']		= 'Pagamentos';
$_['text_success']		= 'Método de pagamento modificado com sucesso!';
$_['column_name']		= 'Pagamento';
$_['column_status']		= 'Estado';
$_['column_sort_order']		= 'Posição';
$_['column_action']		= 'Ação';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar os pagamentos!';
