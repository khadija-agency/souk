<?php
$_['heading_title']		= 'Expedição';
$_['text_success']		= 'Método de expedição modificado com sucesso!';
$_['column_name']		= 'Método';
$_['column_status']		= 'Situação';
$_['column_sort_order']		= 'Posição';
$_['column_action']		= 'Ação';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar os fretes!';
