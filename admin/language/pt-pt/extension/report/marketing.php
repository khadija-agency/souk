<?php
$_['heading_title']		= 'Estatísticas de Marketing';
$_['text_extension']		= 'Extensions';
$_['text_edit']		= 'Edit Marketing Report';
$_['text_success']		= 'Success: You have modified marketing report!';
$_['text_filter']		= 'Filter';
$_['text_all_status']		= 'Todos';
$_['column_campaign']		= 'Nome da Campanha';
$_['column_code']		= 'Código';
$_['column_clicks']		= 'Cliques';
$_['column_orders']		= 'No. de Pedidos';
$_['column_total']		= 'Total';
$_['entry_date_start']		= 'Data Inicial';
$_['entry_date_end']		= 'Data Final';
$_['entry_order_status']		= 'Order Status';
$_['entry_status']		= 'Estado do Pedido';
$_['entry_sort_order']		= 'Sort Order';
$_['error_permission']		= 'Warning: You do not have permission to modify marketing report!';
