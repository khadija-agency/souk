<?php
$_['heading_title']		= 'Taxa de embalagem';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Módulo taxa de embalagem modificado com sucesso!';
$_['text_edit']		= 'Configurações da taxa de embalagem';
$_['entry_total']		= 'Total mínimo';
$_['entry_fee']		= 'Taxa';
$_['entry_tax_class']		= 'Classe de impostos';
$_['entry_status']		= 'Estado';
$_['entry_sort_order']		= 'Posição';
$_['help_total']		= 'O valor máximo do total do pedido sujeito à aplicação da taxa.';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar a taxa de embalagem!';
