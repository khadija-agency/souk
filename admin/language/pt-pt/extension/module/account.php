<?php
$_['heading_title']		= 'Menu do cliente';
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Módulo menu do cliente modificado com sucesso!';
$_['text_edit']		= 'Configurações do módulo Menu do cliente';
$_['entry_status']		= 'Estado';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar o módulo menu do cliente!';
