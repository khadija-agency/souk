<?php
$_['heading_title']		= 'Página não encontrada!';
$_['text_not_found']		= 'A página que está a procurar não pôde ser encontrada! Entre em contacto com o administrador se o problema persistir.';
