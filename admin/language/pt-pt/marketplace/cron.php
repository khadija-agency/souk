<?php
$_['heading_title']		= 'Cron Jobs';
$_['text_success']		= 'Modificou os cron jobs com sucesso!';
$_['text_instruction']		= 'Instruções CRON';
$_['text_list']		= 'Lista Cron';
$_['text_cron_1']		= 'Cron Jobs são tarefas agendadas que são executadas periodicamente. Para configurar os seus servidores para usar o trabalho do cron, veja a <a href="https://wiki.khadija.agency/souk/extension/cron/" target="_blank" class="alert-link"> documentação do souk </ a > página. ';
$_['text_cron_2']		= 'Precisa configurar a tarefa do Cron para ser executada todas as horas.';
$_['text_hour']		= 'Hora';
$_['text_day']		= 'Dia';
$_['text_month']		= 'Mês';
$_['column_code']		= 'Cron Code';
$_['column_cycle']		= 'Ciclo';
$_['column_status']		= 'Estado';
$_['column_date_added']		= 'Data Adicionado';
$_['column_date_modified']		= 'Data Modificado';
$_['column_action']		= 'Ação';
$_['entry_cron']		= 'Cron URL';
$_['error_permission']		= 'Atenção: Não tem permissão para modificar cron jobs!';
