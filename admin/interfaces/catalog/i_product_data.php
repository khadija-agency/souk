<?php
/**
* @package     souk
* @author      contact at khadija dot agency
* @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
* @license     https://opensource.org/license/bsd-3-clause/
* @link        https://khadija.agency
*/
namespace Souk\Catalog;

interface i_product_data {
    public function get_data();
    public function set_data($data) ;
    public function getProductDescription() ;
    public function getModel();
    public function getSku();
    public function getUpc();
    public function getEan();
    public function getJan();
    public function getIsbn();
    public function getMpn();
    public function getLocation();
    public function getPrice();
    public function getTaxClassId();
    public function getQuantity();
    public function getMinimum();
    public function getSubtract();
    public function getStockStatusId();
    public function getShipping();
    public function getDateAvailable();
    public function getLength();
    public function getWidth();
    public function getHeight();
    public function getLengthClassId();
    public function getWeight();
    public function getWeightClassId();
    public function getStatus();
    public function getSortOrder();
    public function getManufacturer();
    public function getManufacturerId();
    public function getCategory();
    public function getProductCategory();
    public function getFilter();
    public function getProductStore();
    public function getDownload();
    public function getRelated();
    public function getOption();
    public function getImage();
    public function getPoints();
    public function getProductReward();
    public function getProductSeoUrl();
    public function getProductLayout();
}
