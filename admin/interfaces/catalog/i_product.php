<?php
/**
* @package     souk
* @author      contact at khadija dot agency
* @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
* @license     https://opensource.org/license/bsd-3-clause/
* @link        https://khadija.agency
*/
namespace Souk\Catalog;

interface i_product {
    public function getProduct($product_id);
    public function getProducts($data = array());
    public function getProductsByCategoryId($category_id);
    public function getProductDescriptions($product_id);
    public function getProductCategories($product_id);
    public function getProductFilters($product_id);
    public function getProductAttributes($product_id);
    public function getProductOptions($product_id);
    public function getProductOptionValue($product_id, $product_option_value_id);
    public function getProductImages($product_id);
    public function getProductDiscounts($product_id);
    public function getProductSpecials($product_id);
    public function getProductRewards($product_id);
    public function getProductDownloads($product_id);
    public function getProductStores($product_id);
    public function getProductSeoUrls($product_id);
    public function getProductLayouts($product_id);
    public function getProductRelated($product_id);
    public function getRecurrings($product_id);
    public function getTotalProducts($data = array());
    public function getTotalProductsByTaxClassId($tax_class_id);
    public function getTotalProductsByStockStatusId($stock_status_id);
    public function getTotalProductsByWeightClassId($weight_class_id);
    public function getTotalProductsByLengthClassId($length_class_id);
    public function getTotalProductsByDownloadId($download_id);
    public function getTotalProductsByManufacturerId($manufacturer_id);
    public function getTotalProductsByAttributeId($attribute_id);
    public function getTotalProductsByOptionId($option_id);
    public function getTotalProductsByProfileId($recurring_id);
    public function getTotalProductsByLayoutId($layout_id);
}

interface i_product_crud extends i_product {
    public function addProduct($data);
    public function editProduct($product_id, $data);
    public function copyProduct($product_id);
    public function deleteProduct($product_id);
    public function addDescription($product_id, $data);
}
