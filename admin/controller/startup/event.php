<?php
class ControllerStartupEvent extends Europa\Controller {
	public function index() {
		// Add events from the DB
		$this->load->model('setting/event');

		$results = $this->model_setting_event->getEvents();

		foreach ($results as $result) {
			if ((substr($result['event_trigger'], 0, 6) == 'admin/') && $result['status']) {
				$this->event->register(substr($result['event_trigger'], 6), new Europa\Action($result['action']), $result['sort_order']);
			}
		}
	}
}
