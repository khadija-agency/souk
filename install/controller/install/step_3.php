<?php
class ControllerInstallStep3 extends Europa\Controller {
	private $error = [];

	public function index() {
		$this->load->language('install/step_3');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            // we are migrating to europa system
            $this->request->post['http_server'] = HTTP_SERVER;

            require_once(DIR_APPLICATION . 'model/system/db.php');
            require_once(DIR_APPLICATION . 'model/install/db.php');
            require_once(DIR_APPLICATION . 'model/install/config.php');

            $this->registry->set('model_install_config', new ModelInstallConfig(new Europa\data()));
			$this->model_install_config->create_file($this->request->post);

            $reg = new Europa\data();
            $reg->set('model_system_db', new ModelSystemDb($reg));
            $reg->set('model_install_db', new ModelInstallDb($reg));

            $reg->get('model_install_db')
                ->install_database($this->request->post);

			$this->response->redirect($this->url->link('install/step_4'));
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_step_3'] = $this->language->get('text_step_3');
		$data['text_db_connection'] = $this->language->get('text_db_connection');
		$data['text_db_administration'] = $this->language->get('text_db_administration');
		$data['text_mysqli'] = $this->language->get('text_mysqli');
		$data['text_pdo'] = $this->language->get('text_pdo');
		$data['text_pgsql'] = $this->language->get('text_pgsql');

		$data['entry_db_system'] = $this->language->get('entry_db_system');
		$data['entry_db_hostname'] = $this->language->get('entry_db_hostname');
		$data['entry_db_username'] = $this->language->get('entry_db_username');
		$data['entry_db_password'] = $this->language->get('entry_db_password');
		$data['entry_db_database'] = $this->language->get('entry_db_database');
		$data['entry_db_port'] = $this->language->get('entry_db_port');
		$data['entry_db_prefix'] = $this->language->get('entry_db_prefix');
		$data['entry_username'] = $this->language->get('entry_username');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_email'] = $this->language->get('entry_email');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['db_system'])) {
			$data['error_db_system'] = $this->error['db_system'];
		} else {
			$data['error_db_system'] = '';
		}

		if (isset($this->error['db_hostname'])) {
			$data['error_db_hostname'] = $this->error['db_hostname'];
		} else {
			$data['error_db_hostname'] = '';
		}

		if (isset($this->error['db_username'])) {
			$data['error_db_username'] = $this->error['db_username'];
		} else {
			$data['error_db_username'] = '';
		}

		if (isset($this->error['db_database'])) {
			$data['error_db_database'] = $this->error['db_database'];
		} else {
			$data['error_db_database'] = '';
		}

		if (isset($this->error['db_port'])) {
			$data['error_db_port'] = $this->error['db_port'];
		} else {
			$data['error_db_port'] = '';
		}

		if (isset($this->error['db_prefix'])) {
			$data['error_db_prefix'] = $this->error['db_prefix'];
		} else {
			$data['error_db_prefix'] = '';
		}

		if (isset($this->error['username'])) {
			$data['error_username'] = $this->error['username'];
		} else {
			$data['error_username'] = '';
		}

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		$data['action'] = $this->url->link('install/step_3');

        $db_systems = \PDO::getAvailableDrivers();

		$data['systems'] = array();

		foreach ($db_systems as $db_system) {
			$data['db_systems'][] = array(
				'text'  => $this->language->get('text_' . $db_system),
				'value' => $db_system
			);
		}

		if (isset($this->request->post['db_system'])) {
			$data['db_system'] = $this->request->post['db_system'];
		} else {
			$data['db_system'] = '';
		}

		if (isset($this->request->post['db_hostname'])) {
			$data['db_hostname'] = $this->request->post['db_hostname'];
		} else {
			$data['db_hostname'] = 'localhost';
		}

		if (isset($this->request->post['db_username'])) {
			$data['db_username'] = $this->request->post['db_username'];
		} else {
			$data['db_username'] = 'user_souk';
		}

		if (isset($this->request->post['db_password'])) {
			$data['db_password'] = $this->request->post['db_password'];
		} else {
			$data['db_password'] = '';
		}

		if (isset($this->request->post['db_database'])) {
			$data['db_database'] = $this->request->post['db_database'];
		} else {
			$data['db_database'] = 'db_souk';
		}

		if (isset($this->request->post['db_port'])) {
			$data['db_port'] = $this->request->post['db_port'];
		} else {
			$data['db_port'] = 3306;
		}

		if (isset($this->request->post['db_prefix'])) {
			$data['db_prefix'] = $this->request->post['db_prefix'];
		} else {
			$data['db_prefix'] = 'sk_';
		}

		if (isset($this->request->post['username'])) {
			$data['username'] = $this->request->post['username'];
		} else {
			$data['username'] = 'admin';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}

		$data['back'] = $this->url->link('install/step_2');

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');

		$this->response->set_output($this->load->view('install/step_3', $data));
	}

	private function validate() {
		if (!$this->request->post['db_hostname']) {
			$this->error['db_hostname'] = $this->language->get('error_db_hostname');
		}

		if (!$this->request->post['db_username']) {
			$this->error['db_username'] = $this->language->get('error_db_username');
		}

		if (!$this->request->post['db_database']) {
			$this->error['db_database'] = $this->language->get('error_db_database');
		}

		if (!$this->request->post['db_port']) {
			$this->error['db_port'] = $this->language->get('error_db_port');
		}

        if ($this->request->post['db_prefix'] && preg_match('/[^a-z0-9_]/', $this->request->post['db_prefix'])) {
			$this->error['db_prefix'] = $this->language->get('error_db_prefix');
		}

        $db_systems = \PDO::getAvailableDrivers();

		if (!in_array($this->request->post['db_system'], $db_systems)) {
			$this->error['db_system'] = $this->language->get('error_db_system');
		} else {
			try {
                $db = new Europa\database(
                    $this->request->post['db_system'],
                    $this->request->post['db_hostname'],
                    $this->request->post['db_username'],
                    $this->request->post['db_password'],
                    $this->request->post['db_database'],
                    $this->request->post['db_port'],
                    $this->request->post['db_prefix']);
                $pdo = $db->get_pdo();
			} catch (Exception $e) {
				$this->error['warning'] = $e->getMessage();
			}
		}

		if (!$this->request->post['username']) {
			$this->error['username'] = $this->language->get('error_username');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if (!$this->request->post['password']) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if (!is_writable(DIR_SOUK . 'config.php')) {
			$this->error['warning'] = $this->language->get('error_config') . DIR_SOUK . 'config.php!';
		}

		if (!is_writable(DIR_SOUK . 'admin/config.php')) {
			$this->error['warning'] = $this->language->get('error_config') . DIR_SOUK . 'admin/config.php!';
		}

		return !$this->error;
	}
}
