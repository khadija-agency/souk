<?php
class ControllerStartupRouter extends Europa\Controller {
	public function index() {
		if (isset($this->request->get['route']) && $this->request->get['route'] != 'action/route') {
			return new Europa\Action($this->request->get['route']);
		} else {
			return new Europa\Action($this->config->get('action_default'));
		}
	}
}
