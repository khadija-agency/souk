<?php
class ControllerStartupDatabase extends Europa\Controller {
	public function index() {
		if (is_file(DIR_SOUK . 'config.php') && filesize(DIR_SOUK . 'config.php') > 0) {
			$lines = file(DIR_SOUK . 'config.php');

			foreach ($lines as $line) {
				if (strpos(strtoupper($line), 'DB_') !== false) {
					eval($line);
				}
			}

            // check if on upgrade process
            if(defined('DB_DRIVER') && !defined('DB_SYSTEM')){
                define('DB_SYSTEM', DB_DRIVER);
            }
            $db = new Europa\database(
                DB_SYSTEM,
                DB_HOSTNAME,
                DB_USERNAME,
                DB_PASSWORD,
                DB_DATABASE,
                DB_PORT,
                DB_PREFIX
            );

			$this->registry->set('pdo', $db->get_pdo());
            $this->registry->set('database', $db);
		}
	}
}
