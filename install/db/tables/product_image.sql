CREATE TABLE sk_product_image (
  product_image_id SERIAL PRIMARY KEY,
  product_id INTEGER NOT NULL,
  image VARCHAR(255) DEFAULT NULL,
  sort_order SMALLINT NOT NULL DEFAULT '0'
);
