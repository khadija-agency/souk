CREATE TABLE sk_country (
  country_id SERIAL PRIMARY KEY,
  name VARCHAR(128) NOT NULL,
  iso_code_2 VARCHAR(2) NOT NULL,
  iso_code_3 VARCHAR(3) NOT NULL,
  address_format TEXT NOT NULL,
  postcode_required BOOLEAN NOT NULL,
  status BOOLEAN NOT NULL DEFAULT true
);
