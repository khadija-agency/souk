CREATE TABLE sk_product_option_value (
  product_option_value_id SERIAL PRIMARY KEY,
  product_option_id INTEGER NOT NULL,
  product_id INTEGER NOT NULL,
  option_id INTEGER NOT NULL,
  option_value_id INTEGER NOT NULL,
  quantity SMALLINT NOT NULL,
  subtract BOOLEAN NOT NULL,
  price NUMERIC(15,4) NOT NULL,
  price_prefix VARCHAR(1) NOT NULL,
  points INTEGER NOT NULL,
  points_prefix VARCHAR(1) NOT NULL,
  weight NUMERIC(15,8) NOT NULL,
  weight_prefix VARCHAR(1) NOT NULL
);
