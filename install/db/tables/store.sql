CREATE TABLE sk_store (
  store_id SERIAL PRIMARY KEY,
  store_name VARCHAR(64) NOT NULL,
  store_url VARCHAR(255) NOT NULL,
  store_ssl VARCHAR(255) NOT NULL
);
