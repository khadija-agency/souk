CREATE TABLE sk_user_group (
  user_group_id SERIAL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  permission TEXT NOT NULL
);
