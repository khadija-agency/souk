CREATE TABLE sk_product_to_layout (
  product_id INTEGER NOT NULL,
  store_id INTEGER NOT NULL,
  layout_id INTEGER NOT NULL,
  PRIMARY KEY (product_id,store_id)
);
