CREATE TABLE sk_product_description (
  product_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  name VARCHAR(255) NOT NULL,
  description TEXT NOT NULL,
  tag TEXT NOT NULL,
  meta_title VARCHAR(255) NOT NULL,
  meta_description VARCHAR(255) NOT NULL,
  meta_keyword VARCHAR(255) NOT NULL
);
