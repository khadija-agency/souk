CREATE TABLE sk_customer_search (
  customer_search_id SERIAL PRIMARY KEY,
  store_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  customer_id INTEGER NOT NULL,
  keyword VARCHAR(255) NOT NULL,
  category_id INTEGER,
  sub_category BOOLEAN NOT NULL,
  description BOOLEAN NOT NULL,
  products INTEGER NOT NULL,
  ip VARCHAR(40) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
