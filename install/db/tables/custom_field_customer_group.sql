CREATE TABLE sk_custom_field_customer_group (
  custom_field_id INTEGER NOT NULL,
  customer_group_id INTEGER NOT NULL,
  required BOOLEAN NOT NULL,
  PRIMARY KEY (custom_field_id,customer_group_id)
);
