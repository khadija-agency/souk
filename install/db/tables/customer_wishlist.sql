CREATE TABLE sk_customer_wishlist (
  customer_id INTEGER NOT NULL,
  product_id INTEGER NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY (customer_id,product_id)
);
