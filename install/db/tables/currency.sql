CREATE TABLE sk_currency (
  currency_id SERIAL PRIMARY KEY,
  title VARCHAR(32) NOT NULL,
  code VARCHAR(3) NOT NULL,
  symbol_left VARCHAR(12) NOT NULL,
  symbol_right VARCHAR(12) NOT NULL,
  decimal_place char(1) NOT NULL,
  value NUMERIC(15,8) NOT NULL,
  status BOOLEAN NOT NULL,
  date_modified TIMESTAMP NOT NULL DEFAULT NOW()
);
