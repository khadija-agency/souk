CREATE TABLE sk_product_related (
  product_id INTEGER NOT NULL,
  related_id INTEGER NOT NULL,
  PRIMARY KEY (product_id,related_id)
);
