CREATE TABLE sk_user_login (
  user_login_id SERIAL PRIMARY KEY,
  username VARCHAR(96) NOT NULL,
  ip VARCHAR(40) NOT NULL,
  total INTEGER NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  date_modified TIMESTAMP NOT NULL DEFAULT NOW()
);
