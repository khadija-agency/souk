CREATE TABLE sk_extension_path (
  extension_path_id SERIAL PRIMARY KEY,
  extension_install_id INTEGER NOT NULL,
  path VARCHAR(255) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
