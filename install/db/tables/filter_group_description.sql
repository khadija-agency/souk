CREATE TABLE sk_filter_group_description (
  filter_group_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  name VARCHAR(64) NOT NULL,
  PRIMARY KEY (filter_group_id,language_id)
);
