CREATE TABLE sk_attribute_group (
  attribute_group_id SERIAL PRIMARY KEY,
  sort_order SMALLINT NOT NULL
);
