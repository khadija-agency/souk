CREATE TABLE sk_custom_field_description (
  custom_field_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  name VARCHAR(128) NOT NULL,
  PRIMARY KEY (custom_field_id,language_id)
);
