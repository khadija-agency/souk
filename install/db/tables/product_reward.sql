CREATE TABLE sk_product_reward (
  product_reward_id SERIAL PRIMARY KEY,
  product_id INTEGER NOT NULL DEFAULT '0',
  customer_group_id INTEGER NOT NULL DEFAULT '0',
  points INTEGER NOT NULL DEFAULT '0'
);
