CREATE TABLE sk_return_history (
  return_history_id SERIAL PRIMARY KEY,
  return_id INTEGER NOT NULL,
  return_status_id INTEGER NOT NULL,
  notify BOOLEAN NOT NULL,
  comment TEXT NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
