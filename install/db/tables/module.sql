CREATE TABLE sk_module (
  module_id SERIAL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  code VARCHAR(32) NOT NULL,
  setting TEXT NOT NULL
);
