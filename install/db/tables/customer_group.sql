CREATE TABLE sk_customer_group (
  customer_group_id SERIAL PRIMARY KEY,
  approval BOOLEAN NOT NULL,
  sort_order SMALLINT NOT NULL
);
