CREATE TABLE sk_filter_group (
  filter_group_id SERIAL PRIMARY KEY,
  sort_order SMALLINT NOT NULL
);
