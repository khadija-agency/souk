CREATE TABLE sk_weight_class (
  weight_class_id SERIAL PRIMARY KEY,
  value NUMERIC(15,8) NOT NULL DEFAULT '0.00000000'
);
