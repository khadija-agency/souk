CREATE TABLE sk_product_recurring (
  product_id INTEGER NOT NULL,
  recurring_id INTEGER NOT NULL,
  customer_group_id INTEGER NOT NULL,
  PRIMARY KEY (product_id,recurring_id,customer_group_id)
);
