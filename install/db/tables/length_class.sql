CREATE TABLE sk_length_class (
  length_class_id SERIAL PRIMARY KEY,
  value NUMERIC(15,8) NOT NULL
);
