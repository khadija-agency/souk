CREATE TABLE sk_filter (
  filter_id SERIAL PRIMARY KEY,
  filter_group_id INTEGER NOT NULL,
  sort_order SMALLINT NOT NULL
);
