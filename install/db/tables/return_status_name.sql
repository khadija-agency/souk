CREATE TABLE sk_return_status_name (
  return_status_id BIGINT NOT NULL,
  language_id BIGINT NOT NULL,
  name VARCHAR(32) NOT NULL
);
