CREATE TABLE sk_attribute_description (
  attribute_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  name VARCHAR(64) NOT NULL,
  PRIMARY KEY (attribute_id,language_id)
);
