CREATE TABLE sk_option_value (
  option_value_id SERIAL PRIMARY KEY,
  option_id INTEGER NOT NULL,
  image VARCHAR(255) NOT NULL,
  sort_order SMALLINT NOT NULL
);
