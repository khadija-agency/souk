CREATE TABLE sk_custom_field (
  custom_field_id SERIAL PRIMARY KEY,
  type VARCHAR(32) NOT NULL,
  value TEXT NOT NULL,
  validation VARCHAR(255) NOT NULL,
  location VARCHAR(10) NOT NULL,
  status BOOLEAN NOT NULL,
  sort_order SMALLINT NOT NULL
);
