CREATE TABLE sk_information_to_layout (
  information_id INTEGER NOT NULL,
  store_id INTEGER NOT NULL,
  layout_id INTEGER NOT NULL,
  PRIMARY KEY (information_id,store_id)
);
