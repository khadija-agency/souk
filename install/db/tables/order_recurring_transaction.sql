CREATE TABLE sk_order_recurring_transaction (
  order_recurring_transaction_id SERIAL PRIMARY KEY,
  order_recurring_id INTEGER NOT NULL,
  reference VARCHAR(255) NOT NULL,
  type VARCHAR(255) NOT NULL,
  amount NUMERIC(10,4) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
