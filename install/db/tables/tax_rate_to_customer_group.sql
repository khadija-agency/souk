CREATE TABLE sk_tax_rate_to_customer_group (
  tax_rate_id INTEGER NOT NULL,
  customer_group_id INTEGER NOT NULL,
  PRIMARY KEY (tax_rate_id,customer_group_id)
);
