CREATE TABLE sk_product_discount (
  product_discount_id SERIAL PRIMARY KEY,
  product_id INTEGER NOT NULL,
  customer_group_id INTEGER NOT NULL,
  quantity INTEGER NOT NULL DEFAULT 0,
  priority INTEGER NOT NULL DEFAULT '1',
  price NUMERIC(15,4) NOT NULL DEFAULT '0.0000',
  date_start date NOT NULL DEFAULT '1970-01-01',
  date_end date NOT NULL DEFAULT '1970-01-01'
);
