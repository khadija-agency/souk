CREATE TABLE sk_banner (
  banner_id SERIAL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  status BOOLEAN NOT NULL
);
