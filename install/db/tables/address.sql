CREATE TABLE sk_address (
  address_id SERIAL PRIMARY KEY,
  customer_id BIGINT NOT NULL,
  firstname VARCHAR(32) NOT NULL,
  lastname VARCHAR(32) NOT NULL,
  company VARCHAR(60) NOT NULL,
  address_1 VARCHAR(128) NOT NULL,
  address_2 VARCHAR(128) NOT NULL,
  city VARCHAR(128) NOT NULL,
  postcode VARCHAR(10) NOT NULL,
  country_id BIGINT NOT NULL,
  zone_id BIGINT NOT NULL,
  custom_field TEXT NOT NULL,
  default_address BOOLEAN NOT NULL DEFAULT '0'
);
