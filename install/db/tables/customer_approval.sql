CREATE TABLE sk_customer_approval (
  customer_approval_id SERIAL PRIMARY KEY,
  customer_id INTEGER NOT NULL,
  type VARCHAR(9) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
