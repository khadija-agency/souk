CREATE TABLE sk_event (
  event_id SERIAL PRIMARY KEY,
  code VARCHAR(64) NOT NULL,
  event_trigger TEXT NOT NULL,
  action TEXT NOT NULL,
  status BOOLEAN NOT NULL,
  sort_order SMALLINT NOT NULL
);
