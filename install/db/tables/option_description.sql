CREATE TABLE sk_option_description (
  option_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  name VARCHAR(128) NOT NULL,
  PRIMARY KEY (option_id,language_id)
);
