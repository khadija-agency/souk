CREATE TABLE sk_customer_reward (
  customer_reward_id SERIAL PRIMARY KEY,
  customer_id INTEGER NOT NULL DEFAULT '0',
  order_id INTEGER NOT NULL DEFAULT '0',
  description TEXT NOT NULL,
  points INTEGER NOT NULL DEFAULT '0',
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
