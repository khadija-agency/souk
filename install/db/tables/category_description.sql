CREATE TABLE sk_category_description (
  category_id BIGINT NOT NULL,
  language_id BIGINT NOT NULL,
  name VARCHAR(255) NOT NULL,
  description TEXT NOT NULL,
  meta_title VARCHAR(255) NOT NULL,
  meta_description VARCHAR(255) NOT NULL,
  meta_keyword VARCHAR(255) NOT NULL
);
