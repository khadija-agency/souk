CREATE TABLE sk_api_session (
  api_session_id SERIAL PRIMARY KEY,
  api_id INTEGER NOT NULL,
  session_id VARCHAR(32) NOT NULL,
  ip VARCHAR(40) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  date_modified TIMESTAMP NOT NULL DEFAULT NOW()
);
