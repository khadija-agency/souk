CREATE TABLE sk_category_filter (
  category_id INTEGER NOT NULL,
  filter_id INTEGER NOT NULL,
  PRIMARY KEY (category_id,filter_id)
);
