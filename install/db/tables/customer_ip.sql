CREATE TABLE sk_customer_ip (
  customer_ip_id SERIAL PRIMARY KEY,
  customer_id INTEGER NOT NULL,
  store_id INTEGER NOT NULL,
  ip VARCHAR(40) NOT NULL,
  country VARCHAR(2) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
