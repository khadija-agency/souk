CREATE TABLE sk_order_history (
  order_history_id SERIAL PRIMARY KEY,
  order_id INTEGER NOT NULL,
  order_status_id INTEGER NOT NULL,
  notify BOOLEAN NOT NULL DEFAULT '0',
  comment TEXT NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
