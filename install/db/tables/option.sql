CREATE TABLE sk_option (
  option_id SERIAL PRIMARY KEY,
  type VARCHAR(32) NOT NULL,
  sort_order SMALLINT NOT NULL
);
