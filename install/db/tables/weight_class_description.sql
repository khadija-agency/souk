CREATE TABLE sk_weight_class_description (
  weight_class_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  title VARCHAR(32) NOT NULL,
  unit VARCHAR(4) NOT NULL,
  PRIMARY KEY (weight_class_id,language_id)
);
