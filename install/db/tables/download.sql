CREATE TABLE sk_download (
  download_id SERIAL PRIMARY KEY,
  filename VARCHAR(160) NOT NULL,
  mask VARCHAR(128) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
