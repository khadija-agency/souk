CREATE TABLE sk_customer_group_description (
  customer_group_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  name VARCHAR(32) NOT NULL,
  description TEXT NOT NULL,
  PRIMARY KEY (customer_group_id,language_id)
);
