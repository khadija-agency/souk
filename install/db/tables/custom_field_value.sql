CREATE TABLE sk_custom_field_value (
  custom_field_value_id SERIAL PRIMARY KEY,
  custom_field_id INTEGER NOT NULL,
  sort_order SMALLINT NOT NULL
);
