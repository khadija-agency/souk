CREATE TABLE sk_return_action_name (
  return_action_id BIGINT NOT NULL,
  language_id BIGINT NOT NULL,
  name VARCHAR(64) NOT NULL
);
