CREATE TABLE sk_coupon (
  coupon_id SERIAL PRIMARY KEY,
  name VARCHAR(128) NOT NULL,
  code VARCHAR(20) NOT NULL,
  type char(1) NOT NULL,
  discount NUMERIC(15,4) NOT NULL,
  logged BOOLEAN NOT NULL,
  shipping BOOLEAN NOT NULL,
  total NUMERIC(15,4) NOT NULL,
  date_start date NOT NULL DEFAULT '1970-01-01',
  date_end date NOT NULL DEFAULT '1970-01-01',
  uses_total INTEGER NOT NULL,
  uses_customer VARCHAR(11) NOT NULL,
  status BOOLEAN NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
