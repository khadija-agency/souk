CREATE TABLE sk_location (
  location_id SERIAL PRIMARY KEY,
  name VARCHAR(32) NOT NULL,
  address TEXT NOT NULL,
  telephone VARCHAR(32) NOT NULL,
  geocode VARCHAR(32) NOT NULL,
  image VARCHAR(255) DEFAULT NULL,
  open TEXT NOT NULL,
  comment TEXT NOT NULL
);
