CREATE TABLE sk_extension (
  extension_id SERIAL PRIMARY KEY,
  type VARCHAR(32) NOT NULL,
  code VARCHAR(32) NOT NULL
);
