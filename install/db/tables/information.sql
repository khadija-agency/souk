CREATE TABLE sk_information (
  information_id SERIAL PRIMARY KEY,
  bottom BOOLEAN NOT NULL DEFAULT false,
  sort_order SMALLINT NOT NULL DEFAULT '0',
  status BOOLEAN NOT NULL DEFAULT true
);
