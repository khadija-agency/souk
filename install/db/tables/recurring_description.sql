CREATE TABLE sk_recurring_description (
  recurring_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (recurring_id,language_id)
);
