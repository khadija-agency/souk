CREATE TABLE sk_extension_install (
  extension_install_id SERIAL PRIMARY KEY,
  extension_download_id INTEGER NOT NULL,
  filename VARCHAR(255) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
