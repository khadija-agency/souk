CREATE TABLE sk_information_description (
  information_id BIGINT NOT NULL,
  language_id BIGINT NOT NULL,
  title VARCHAR(64) NOT NULL,
  description TEXT NOT NULL,
  meta_title VARCHAR(255) NOT NULL,
  meta_description VARCHAR(255) NOT NULL,
  meta_keyword VARCHAR(255) NOT NULL
);
