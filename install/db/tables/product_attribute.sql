CREATE TABLE sk_product_attribute (
  product_id INTEGER NOT NULL,
  attribute_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  text TEXT NOT NULL,
  PRIMARY KEY (product_id,attribute_id,language_id)
);
