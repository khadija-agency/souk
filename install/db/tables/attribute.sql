CREATE TABLE sk_attribute (
  attribute_id SERIAL PRIMARY KEY,
  attribute_group_id INTEGER NOT NULL,
  sort_order SMALLINT NOT NULL
);
