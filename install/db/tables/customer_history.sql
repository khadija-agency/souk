CREATE TABLE sk_customer_history (
  customer_history_id SERIAL PRIMARY KEY,
  customer_id INTEGER NOT NULL,
  comment TEXT NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
