CREATE TABLE sk_coupon_category (
  coupon_id INTEGER NOT NULL,
  category_id INTEGER NOT NULL,
  PRIMARY KEY (coupon_id,category_id)
);
