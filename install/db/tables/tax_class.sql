CREATE TABLE sk_tax_class (
  tax_class_id SERIAL PRIMARY KEY,
  title VARCHAR(32) NOT NULL,
  description VARCHAR(255) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  date_modified TIMESTAMP NOT NULL DEFAULT NOW()
);
