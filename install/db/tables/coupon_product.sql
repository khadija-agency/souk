CREATE TABLE sk_coupon_product (
  coupon_product_id SERIAL PRIMARY KEY,
  coupon_id INTEGER NOT NULL,
  product_id INTEGER NOT NULL
);
