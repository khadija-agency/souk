CREATE TABLE sk_layout_module (
  layout_module_id SERIAL PRIMARY KEY,
  layout_id INTEGER NOT NULL,
  code VARCHAR(64) NOT NULL,
  position VARCHAR(14) NOT NULL,
  sort_order SMALLINT NOT NULL
);
