CREATE TABLE sk_option_value_description (
  option_value_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  option_id INTEGER NOT NULL,
  name VARCHAR(128) NOT NULL,
  PRIMARY KEY (option_value_id,language_id)
);
