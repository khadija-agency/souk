CREATE TABLE sk_order_status_name (
  order_status_id BIGINT NOT NULL,
  language_id BIGINT NOT NULL,
  name VARCHAR(32) NOT NULL
);
