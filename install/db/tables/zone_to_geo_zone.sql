CREATE TABLE sk_zone_to_geo_zone (
  zone_to_geo_zone_id SERIAL PRIMARY KEY,
  country_id INTEGER NOT NULL,
  zone_id INTEGER NOT NULL DEFAULT '0',
  geo_zone_id INTEGER NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  date_modified TIMESTAMP NOT NULL DEFAULT NOW()
);
