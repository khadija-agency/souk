CREATE TABLE sk_customer_online (
  ip VARCHAR(40) NOT NULL,
  customer_id INTEGER NOT NULL,
  url TEXT NOT NULL,
  referer TEXT NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY (ip)
);
