CREATE TABLE sk_product_filter (
  product_id INTEGER NOT NULL,
  filter_id INTEGER NOT NULL,
  PRIMARY KEY (product_id,filter_id)
);
