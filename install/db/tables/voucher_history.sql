CREATE TABLE sk_voucher_history (
  voucher_history_id SERIAL PRIMARY KEY,
  voucher_id INTEGER NOT NULL,
  order_id INTEGER NOT NULL,
  amount NUMERIC(15,4) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
