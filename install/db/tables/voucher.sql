CREATE TABLE sk_voucher (
  voucher_id SERIAL PRIMARY KEY,
  order_id INTEGER NOT NULL,
  code VARCHAR(10) NOT NULL,
  from_name VARCHAR(64) NOT NULL,
  from_email VARCHAR(96) NOT NULL,
  to_name VARCHAR(64) NOT NULL,
  to_email VARCHAR(96) NOT NULL,
  voucher_theme_id INTEGER NOT NULL,
  message TEXT NOT NULL,
  amount NUMERIC(15,4) NOT NULL,
  status BOOLEAN NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
