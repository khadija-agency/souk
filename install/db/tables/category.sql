CREATE TABLE sk_category (
  category_id SERIAL PRIMARY KEY,
  image VARCHAR(255) DEFAULT NULL,
  parent_id BIGINT NOT NULL DEFAULT '0',
  top BOOLEAN NOT NULL,
  col SMALLINT NOT NULL,
  sort_order SMALLINT NOT NULL DEFAULT '0',
  status BOOLEAN NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  date_modified TIMESTAMP NOT NULL DEFAULT NOW()
);
