CREATE TABLE sk_download_report (
  download_report_id SERIAL PRIMARY KEY,
  download_id INTEGER NOT NULL,
  store_id INTEGER NOT NULL,
  ip VARCHAR(40) NOT NULL,
  country VARCHAR(2) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
