CREATE TABLE sk_cart (
  cart_id SERIAL PRIMARY KEY,
  api_id INTEGER NOT NULL,
  customer_id INTEGER NOT NULL,
  session_id VARCHAR(32) NOT NULL,
  product_id INTEGER NOT NULL,
  recurring_id INTEGER NOT NULL,
  option TEXT NOT NULL,
  quantity SMALLINT NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
