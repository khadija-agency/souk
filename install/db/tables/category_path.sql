CREATE TABLE sk_category_path (
  category_id INTEGER NOT NULL,
  path_id INTEGER NOT NULL,
  level INTEGER NOT NULL,
  PRIMARY KEY (category_id,path_id)
);
