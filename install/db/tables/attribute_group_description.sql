CREATE TABLE sk_attribute_group_description (
  attribute_group_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  name VARCHAR(64) NOT NULL,
  PRIMARY KEY (attribute_group_id,language_id)
);
