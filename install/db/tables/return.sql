/* version 4 */
CREATE TABLE sk_return (
  return_id SERIAL PRIMARY KEY,
  order_id BIGINT NOT NULL,
  product_id BIGINT NOT NULL,
  customer_id BIGINT NOT NULL,
  firstname VARCHAR(32) NOT NULL,
  lastname VARCHAR(32) NOT NULL,
  email VARCHAR(96) NOT NULL,
  telephone VARCHAR(32) NOT NULL,
  product VARCHAR(255) NOT NULL,
  model VARCHAR(64) NOT NULL,
  quantity INTEGER NOT NULL,
  opened BOOLEAN NOT NULL,
  return_reason_id BIGINT NOT NULL,
  return_action_id BIGINT NOT NULL,
  return_status_id BIGINT NOT NULL,
  return_comment TEXT,
  date_ordered date NOT NULL DEFAULT '1970-01-01',
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  date_modified TIMESTAMP NOT NULL DEFAULT NOW()
);
