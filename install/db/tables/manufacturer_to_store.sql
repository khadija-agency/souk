CREATE TABLE sk_manufacturer_to_store (
  manufacturer_id INTEGER NOT NULL,
  store_id INTEGER NOT NULL,
  PRIMARY KEY (manufacturer_id,store_id)
);
