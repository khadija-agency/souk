CREATE TABLE sk_setting (
  setting_id SERIAL PRIMARY KEY,
  store_id INTEGER NOT NULL DEFAULT '0',
  code VARCHAR(128) NOT NULL,
  setting_key VARCHAR(128) NOT NULL,
  value TEXT NOT NULL,
  serialized BOOLEAN NOT NULL
);
