CREATE TABLE sk_session (
  session_id VARCHAR(32) NOT NULL,
  data TEXT NOT NULL,
  expire TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY (session_id)
);
