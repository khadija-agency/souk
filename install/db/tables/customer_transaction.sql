CREATE TABLE sk_customer_transaction (
  customer_transaction_id SERIAL PRIMARY KEY,
  customer_id INTEGER NOT NULL,
  order_id INTEGER NOT NULL,
  description TEXT NOT NULL,
  amount NUMERIC(15,4) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
