CREATE TABLE sk_customer_affiliate_report (
  customer_affiliate_report_id INTEGER NOT NULL,
  customer_id INTEGER NOT NULL,
  store_id INTEGER NOT NULL,
  ip VARCHAR(40) NOT NULL,
  country VARCHAR(2) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY (customer_affiliate_report_id)
);
