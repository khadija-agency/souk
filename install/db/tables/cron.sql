CREATE TABLE sk_cron (
  cron_id SERIAL PRIMARY KEY,
  code VARCHAR(64) NOT NULL,
  cycle VARCHAR(12) NOT NULL,
  action TEXT NOT NULL,
  status BOOLEAN NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  date_modified TIMESTAMP NOT NULL DEFAULT NOW()
);
