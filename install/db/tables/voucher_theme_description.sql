CREATE TABLE sk_voucher_theme_description (
  voucher_theme_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  name VARCHAR(32) NOT NULL,
  PRIMARY KEY (voucher_theme_id,language_id)
);
