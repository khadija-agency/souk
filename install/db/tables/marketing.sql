CREATE TABLE sk_marketing (
  marketing_id SERIAL PRIMARY KEY,
  name VARCHAR(32) NOT NULL,
  description TEXT NOT NULL,
  code VARCHAR(64) NOT NULL,
  clicks INTEGER NOT NULL DEFAULT '0',
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
