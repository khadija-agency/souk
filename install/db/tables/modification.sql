CREATE TABLE sk_modification (
  modification_id SERIAL PRIMARY KEY,
  extension_install_id INTEGER NOT NULL,
  name VARCHAR(64) NOT NULL,
  code VARCHAR(64) NOT NULL,
  author VARCHAR(64) NOT NULL,
  version VARCHAR(32) NOT NULL,
  link VARCHAR(255) NOT NULL,
  xml TEXT NOT NULL,
  status BOOLEAN NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
