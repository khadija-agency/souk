CREATE TABLE sk_product_to_store (
  product_id INTEGER NOT NULL,
  store_id INTEGER NOT NULL DEFAULT '0',
  PRIMARY KEY (product_id,store_id)
);
