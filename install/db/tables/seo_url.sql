CREATE TABLE sk_seo_url (
  seo_url_id SERIAL PRIMARY KEY,
  store_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,  
  query VARCHAR(255) NOT NULL,
  keyword VARCHAR(255) NOT NULL
);
