CREATE TABLE sk_filter_description (
  filter_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  filter_group_id INTEGER NOT NULL,
  name VARCHAR(64) NOT NULL,
  PRIMARY KEY (filter_id,language_id)
);
