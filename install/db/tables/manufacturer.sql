CREATE TABLE sk_manufacturer (
  manufacturer_id SERIAL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  image VARCHAR(255) DEFAULT NULL,
  sort_order SMALLINT NOT NULL
);
