CREATE TABLE sk_stock_status_name (
  stock_status_id BIGINT NOT NULL,
  language_id BIGINT NOT NULL,
  name VARCHAR(32) NOT NULL
);
