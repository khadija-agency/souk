CREATE TABLE sk_translation (
  translation_id SERIAL PRIMARY KEY,
  store_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  route VARCHAR(64) NOT NULL,
  translation_key VARCHAR(64) NOT NULL,
  value TEXT NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
