CREATE TABLE sk_geo_zone (
  geo_zone_id SERIAL PRIMARY KEY,
  name VARCHAR(32) NOT NULL,
  description VARCHAR(255) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  date_modified TIMESTAMP NOT NULL DEFAULT NOW()
);
