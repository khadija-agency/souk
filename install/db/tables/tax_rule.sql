CREATE TABLE sk_tax_rule (
  tax_rule_id SERIAL PRIMARY KEY,
  tax_class_id INTEGER NOT NULL,
  tax_rate_id INTEGER NOT NULL,
  based VARCHAR(10) NOT NULL,
  priority INTEGER NOT NULL DEFAULT '1'
);
