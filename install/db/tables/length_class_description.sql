CREATE TABLE sk_length_class_description (
  length_class_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  title VARCHAR(32) NOT NULL,
  unit VARCHAR(4) NOT NULL,
  PRIMARY KEY (length_class_id,language_id)
);
