CREATE TABLE sk_coupon_history (
  coupon_history_id SERIAL PRIMARY KEY,
  coupon_id INTEGER NOT NULL,
  order_id INTEGER NOT NULL,
  customer_id INTEGER NOT NULL,
  amount NUMERIC(15,4) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
