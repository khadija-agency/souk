CREATE TABLE sk_download_description (
  download_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  name VARCHAR(64) NOT NULL,
  PRIMARY KEY (download_id,language_id)
);
