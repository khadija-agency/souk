CREATE TABLE sk_statistics (
  statistics_id SERIAL PRIMARY KEY,
  code VARCHAR(64) NOT NULL,
  value NUMERIC(15,4) NOT NULL
);
