CREATE TABLE sk_customer_login (
  customer_login_id SERIAL PRIMARY KEY,
  email VARCHAR(96) NOT NULL,
  ip VARCHAR(40) NOT NULL,
  total SMALLINT NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW(),
  date_modified TIMESTAMP NOT NULL DEFAULT NOW()
);
