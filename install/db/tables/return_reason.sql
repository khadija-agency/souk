CREATE TABLE sk_return_reason (
  return_reason_id SERIAL,
  language_id INTEGER NOT NULL DEFAULT '0',
  name VARCHAR(128) NOT NULL,
  PRIMARY KEY (return_reason_id,language_id)
);
