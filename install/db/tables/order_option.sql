CREATE TABLE sk_order_option (
  order_option_id SERIAL PRIMARY KEY,
  order_id INTEGER NOT NULL,
  order_product_id INTEGER NOT NULL,
  product_option_id INTEGER NOT NULL,
  product_option_value_id INTEGER NOT NULL DEFAULT '0',
  name VARCHAR(255) NOT NULL,
  value TEXT NOT NULL,
  type VARCHAR(32) NOT NULL
);
