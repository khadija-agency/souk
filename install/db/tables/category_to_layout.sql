CREATE TABLE sk_category_to_layout (
  category_id INTEGER NOT NULL,
  store_id INTEGER NOT NULL,
  layout_id INTEGER NOT NULL,
  PRIMARY KEY (category_id,store_id)
);
