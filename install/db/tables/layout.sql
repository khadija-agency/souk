CREATE TABLE sk_layout (
  layout_id SERIAL PRIMARY KEY,
  name VARCHAR(64) NOT NULL
);
