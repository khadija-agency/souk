CREATE TABLE sk_banner_image (
  banner_image_id SERIAL PRIMARY KEY,
  banner_id INTEGER NOT NULL,
  language_id INTEGER NOT NULL,
  title VARCHAR(64) NOT NULL,
  link VARCHAR(255) NOT NULL,
  image VARCHAR(255) NOT NULL,
  sort_order SMALLINT NOT NULL DEFAULT '0'
);
