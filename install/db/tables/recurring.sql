CREATE TABLE sk_recurring (
  recurring_id SERIAL PRIMARY KEY,
  price NUMERIC(10,4) NOT NULL,
  frequency VARCHAR(20) NOT NULL,
  duration INTEGER NOT NULL,
  cycle INTEGER NOT NULL,
  trial_status SMALLINT NOT NULL,
  trial_price NUMERIC(10,4) NOT NULL,
  trial_frequency VARCHAR(20) NOT NULL,
  trial_duration INTEGER NOT NULL,
  trial_cycle INTEGER NOT NULL,
  status SMALLINT NOT NULL,
  sort_order INTEGER NOT NULL
);
