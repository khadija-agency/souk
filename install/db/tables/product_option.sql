CREATE TABLE sk_product_option (
  product_option_id SERIAL PRIMARY KEY,
  product_id INTEGER NOT NULL,
  option_id INTEGER NOT NULL,
  value TEXT NOT NULL,
  required BOOLEAN NOT NULL
);
