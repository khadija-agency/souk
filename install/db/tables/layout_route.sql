CREATE TABLE sk_layout_route (
  layout_route_id SERIAL PRIMARY KEY,
  layout_id INTEGER NOT NULL,
  store_id INTEGER NOT NULL,
  route VARCHAR(64) NOT NULL
);
