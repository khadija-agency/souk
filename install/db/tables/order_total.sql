CREATE TABLE sk_order_total (
  order_total_id SERIAL PRIMARY KEY,
  order_id INTEGER NOT NULL,
  code VARCHAR(32) NOT NULL,
  title VARCHAR(255) NOT NULL,
  value NUMERIC(15,4) NOT NULL DEFAULT '0.0000',
  sort_order SMALLINT NOT NULL
);
