CREATE TABLE sk_customer_activity (
  customer_activity_id SERIAL PRIMARY KEY,
  customer_id INTEGER NOT NULL,
  activity_key VARCHAR(64) NOT NULL,
  data TEXT NOT NULL,
  ip VARCHAR(40) NOT NULL,
  date_added TIMESTAMP NOT NULL DEFAULT NOW()
);
