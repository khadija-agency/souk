INSERT INTO sk_currency (currency_id, title, code, symbol_left, symbol_right, decimal_place, value, status, date_modified) VALUES
(1,'Euro','EUR','','€','2',1,true,CURRENT_TIMESTAMP),
(2,'Maroccan Dirham','MAD','','DH','2',0.090,true,CURRENT_TIMESTAMP),
(3,'Pound Sterling','GBP','£','','2',0.87728,true,CURRENT_TIMESTAMP),
(4,'US Dollar','USD','$','','2',0.973,true,CURRENT_TIMESTAMP),
(5,'Hong Kong Dollar','HKD','HK$','','2',7.84885512,false,CURRENT_TIMESTAMP),
(6,'Indian Rupee','INR', '₹','','2',82.14200637,false,CURRENT_TIMESTAMP),
(7,'Russian Ruble','RUB','','₽','2',56.40360000,false,CURRENT_TIMESTAMP),
(8,'Chinese Yuan Renminbi','CNY','¥','','2',7.18194887,false,CURRENT_TIMESTAMP),
(9,'Australian Dollar','AUD','$','','2',1.59102577,false,CURRENT_TIMESTAMP);
