INSERT INTO sk_language (language_id, name, code, locale, image, directory, sort_order, status) VALUES
(1,'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 1, true),
(2,'Portuguese', 'pt-pt', 'pt-pt,pt,pt-PT,pt-pt.UTF-8', 'pt-pt.png', 'portuguese', 2, true),
(3,'Español', 'es-es', 'es-ES,es_ES.UTF-8,es_ES,es-es', 'es-es.png', 'spanish', 3, true),
(4,'Français', 'fr-fr', 'fr,fr-FR,fr_FR.UTF-8,french', 'fr-fr.png', 'french', 4, true),
(5,'Deutsch', 'de-de', 'de,de-de,de-DE,de_DE,german,de_DE.UTF-8,de-de.UTF-8,german.UTF-8', 'de-de.png', 'german', 5, true),
(6,'Italiano', 'it-it', 'it_IT.UTF-8,it-IT,italian', 'it-it.png', 'italian', 6, true),
(7,'Chinese', 'zh-cn', 'zh,zh-hk,zh-cn,UTF-8,cn-gb,chinese', 'zh-cn.png', 'manderin', 7, true),
(8,'Portuguese (BR)', 'pt-br', 'pt-BR,pt_BR.UTF-8,pt_BR,pt-br', 'pt-br.png', 'brazilian', 8, true);
