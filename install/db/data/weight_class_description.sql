INSERT INTO sk_weight_class_description (weight_class_id, language_id, title, unit) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(3, 1, 'Pound ', 'lb'),
(4, 1, 'Ounce', 'oz'),
(1, 2, 'Kilogram', 'kg'),
(2, 2, 'Gram', 'g'),
(3, 2, 'Pound ', 'lb'),
(4, 2, 'Ounce', 'oz'),
(1, 3, 'Kilogram', 'kg'),
(2, 3, 'Gram', 'g'),
(3, 3, 'Pound ', 'lb'),
(4, 3, 'Ounce', 'oz'),
(1, 4, 'Kilogram', 'kg'),
(2, 4, 'Gram', 'g'),
(3, 4, 'Pound ', 'lb'),
(4, 4, 'Ounce', 'oz'),
(1, 5, 'Kilogram', 'kg'),
(2, 5, 'Gram', 'g'),
(3, 5, 'Pound ', 'lb'),
(4, 5, 'Ounce', 'oz'),
(1, 6, 'Kilogram', 'kg'),
(2, 6, 'Gram', 'g'),
(3, 6, 'Pound ', 'lb'),
(4, 6, 'Ounce', 'oz'),
(1, 7, 'Kilogram', 'kg'),
(2, 7, 'Gram', 'g'),
(3, 7, 'Pound ', 'lb'),
(4, 7, 'Ounce', 'oz'),
(1, 8, 'Kilogram', 'kg'),
(2, 8, 'Gram', 'g'),
(3, 8, 'Pound ', 'lb'),
(4, 8, 'Ounce', 'oz');





