INSERT INTO sk_option_value_description (option_value_id, language_id, option_id, name) VALUES
(1, 1, 1, 'Large'),
(2, 1, 1, 'Small'),
(4, 1, 2, 'Checkbox 4'),
(5, 1, 2, 'Checkbox 3'),
(6, 1, 1, 'Medium'),
(7, 1, 5, 'Yellow'),
(8, 1, 5, 'Green'),
(9, 1, 5, 'Red'),
(10, 1, 5, 'Blue'),
(11, 1, 2, 'Checkbox 1'),
(12, 1, 2, 'Checkbox 2'),
(13, 1, 11, 'Large'),
(14, 1, 11, 'Medium');
