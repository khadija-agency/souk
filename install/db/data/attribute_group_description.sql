INSERT INTO sk_attribute_group_description (attribute_group_id, language_id, name) VALUES
(1, 1, 'Memory'),
(2, 1, 'Technical'),
(3, 1, 'Motherboard'),
(4, 1, 'Processor'),
(1, 2, 'Memory'),
(2, 2, 'Technical'),
(3, 2, 'Motherboard'),
(4, 2, 'Processor'),
(1, 3, 'Memory'),
(2, 3, 'Technical'),
(3, 3, 'Motherboard'),
(4, 3, 'Processor'),
(1, 4, 'Memory'),
(2, 4, 'Technical'),
(3, 4, 'Motherboard'),
(4, 4, 'Processor'),
(1, 5, 'Memory'),
(2, 5, 'Technical'),
(3, 5, 'Motherboard'),
(4, 5, 'Processor'),
(1, 6, 'Memory'),
(2, 6, 'Technical'),
(3, 6, 'Motherboard'),
(4, 6, 'Processor'),
(1, 7, 'Memory'),
(2, 7, 'Technical'),
(3, 7, 'Motherboard'),
(4, 7, 'Processor'),
(1, 8, 'Memory'),
(2, 8, 'Technical'),
(3, 8, 'Motherboard'),
(4, 8, 'Processor');
