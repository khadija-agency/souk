INSERT INTO sk_tax_class (tax_class_id, title, description, date_added, date_modified) VALUES
(1, 'Taxable Goods', 'Taxed goods', '2022-10-10 23:21:53', '2020-10-23 14:07:50'),
(2, 'Downloadable Products', 'Downloadable', '2022-10-10 22:19:39', '2022-10-22 10:27:36');
