INSERT INTO sk_layout_module (layout_module_id, layout_id, code, position, sort_order) VALUES
(1, 10, 'account', 'column_right', 1),
(2, 6, 'account', 'column_right', 1),
(3, 1, 'carousel.29', 'content_top', 3),
(4, 1, 'slideshow.27', 'content_top', 1),
(5, 1, 'featured.28', 'content_top', 2),
(6, 3, 'category', 'column_left', 1),
(7, 3, 'banner.30', 'column_left', 2);
