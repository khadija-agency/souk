<?php
// Command line tool for installing souk
// Original Author: Vineet Naik <vineet.naik@kodeplay.com> <naikvin@gmail.com>
// Updated and maintained by Souk
// (Currently tested on linux only)
//
// Usage:
//
//   php cli_install.php install --username    admin
//                               --email       email@example.com
//                               --password    password
//                               --http_server http://localhost:9000/
//                               --db_system   mysql
//                               --db_hostname localhost
//                               --db_username root
//                               --db_password pass
//                               --db_database souk
//								 --db_port     3306
//                               --db_prefix   sk_
//

ini_set('display_errors', 1);

error_reporting(E_ALL);

// DIR
define('DIR_SOUK', str_replace('\\', '/', realpath(dirname(__FILE__) . '/../')) . '/');
define('DIR_APPLICATION', DIR_SOUK . 'install/');
define('DIR_SYSTEM', DIR_SOUK . 'system/');
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_DATABASE', DIR_SYSTEM . 'database/');
define('DIR_MODIFICATION', DIR_SYSTEM . 'modification/');

// Europa framework
require_once(DIR_SOUK . '/europa/europa.php');
// Helper functions (install module needs token function)
require_once(DIR_SYSTEM . 'helper/general.php');



// Registry
$registry = new Europa\Data();

// Loader
$loader = new Europa\Loader($registry);
$registry->set('load', $loader);

// Request
$registry->set('request', new Europa\Request());

// Response
$response = new Europa\Response();
$response->add_header('Content-Type: text/plain; charset=utf-8');
$registry->set('response', $response);

// Model
require_once(DIR_APPLICATION . 'model/install/config.php');
$registry->set('model_install_config', new ModelInstallConfig($registry));

require_once(DIR_APPLICATION . 'model/system/db.php');
$registry->set('model_system_db', new ModelSystemDb($registry));

require_once(DIR_APPLICATION . 'model/install/db.php');
$registry->set('model_install_db', new ModelInstallDb($registry));

set_error_handler(function($code, $message, $file, $line, array $errcontext = array()) {
    // error was suppressed with the @-operator
    if (error_reporting() === 0) {
        return false;
    }

    throw new \ErrorException($message, 0, $code, $file, $line);
});

class CliInstall extends Europa\controller {
    public function index() {
        if (isset($this->request->server['argv'])) {
            $argv = $this->request->server['argv'];
        } else {
            $argv = [];
        }

        // Just displays the path to the file
        $script = array_shift($argv);

        // Get the arguments passed with the command
        $command = array_shift($argv);

        switch ($command) {
        case 'install':
            $output = $this->install($argv);
            break;
        case 'usage':
        default:
        $output = $this->usage($argv);
        break;
        }

        $this->response->set_output($output);
    }

    public function install($argv) {
        // Options
        $option = [
            'username'    => 'admin',
            'db_system'   => 'mysql',
            'db_hostname' => 'localhost',
            'db_password' => '',
            'db_port'     => '3306',
            'db_prefix'   => 'sk_',
            'http_server' => 'http://localhost:9000/'
        ];

        // Turn args into an array
        for ($i = 0; $i < count($argv); $i++) {
            if (substr($argv[$i], 0, 2) == '--') {
                $key = substr($argv[$i], 2);

                // If the next line also starts with -- we need to fill in a null value for the current one
                if (isset($argv[$i + 1]) && substr($argv[$i + 1], 0, 2) != '--') {
                    $option[$key] = $argv[$i + 1];

                    // Skip the counter by 2
                    $i++;
                } else {
                    $option[$key] = '';
                }
            }
        }

        // Command line is sending true and false as strings so used 1 or 0 instead.

        // Required
        $required = [
            'username',    // Already set
            'email',
            'password',
            'http_server', // Already set
            'db_system',   // Already set
            'db_hostname',
            'db_username', // Already set
            'db_password', // Already set
            'db_database',
            'db_port',     // Already set
            'db_prefix'    // Already set
        ];

        // Validation
        $missing = [];

        foreach ($required as $value) {
            if (!array_key_exists($value, $option)) {
                $missing[] = $value;
            }
        }

        if (count($missing)) {
            return 'ERROR: Following inputs were missing or invalid: ' . implode(', ', $missing)  . "\n";
        }

        // Pre-installation check
        $error = '';

        if (version_compare(phpversion(), '8.1.0', '<')) {
            $error .= 'ERROR: You need to use PHP8.1.+ or above for Souk to work!' . "\n";
        }

        if (!ini_get('file_uploads')) {
            $error .= 'ERROR: file_uploads needs to be enabled!' . "\n";
        }

        if (ini_get('session.auto_start')) {
            $error .= 'ERROR: Souk will not work with session.auto_start enabled!' . "\n";
        }

        if (!extension_loaded('pdo_pgsql') || !extension_loaded('pdo_mysql')) {
            $error .= 'ERROR: PDO pgsql or mysql extension needs to be loaded for Souk to work!' . "\n";
        }

        if (!extension_loaded('gd')) {
            $error .= 'ERROR: GD extension needs to be loaded for Souk to work!' . "\n";
        }

        if (!extension_loaded('curl')) {
            $error .= 'ERROR: CURL extension needs to be loaded for Souk to work!' . "\n";
        }

        if (!function_exists('openssl_encrypt')) {
            $error .= 'ERROR: OpenSSL extension needs to be loaded for Souk to work!' . "\n";
        }

        if (!extension_loaded('zlib')) {
            $error .= 'ERROR: ZLIB extension needs to be loaded for Souk to work!' . "\n";
        }

        if (!is_file(DIR_SOUK . 'config.php')) {
            $error .= 'ERROR: config.php does not exist. You need to rename config-dist.php to config.php!' . "\n";
        } elseif (!is_writable(DIR_SOUK . 'config.php')) {
            $error .= 'ERROR: config.php needs to be writable for Souk to be installed!' . "\n";
        } elseif (!is_file(DIR_SOUK . 'admin/config.php')) {
            $error .= 'ERROR: admin/config.php does not exist. You need to rename admin/config-dist.php to admin/config.php!' . "\n";
        } elseif (!is_writable(DIR_SOUK . 'admin/config.php')) {
            $error .= 'ERROR: admin/config.php needs to be writable for Souk to be installed!' . "\n";
        }

        $data_dirs = array(
            DIR_SOUK . 'image/',
            DIR_SOUK . 'system/storage/download/',
            DIR_SOUK . 'system/storage/upload/',
            DIR_SOUK . 'system/storage/cache/',
            DIR_SOUK . 'system/storage/logs/',
            DIR_SOUK . 'system/storage/modification/',
        );

        exec('chmod -R o+w ' . implode(' ',$data_dirs));
        unset($data_dirs);

        if ($error) {
            $output  = 'ERROR: Pre-installation check failed: ' . "\n";
            $output .= $error . "\n\n";

            return $output;
        }

        // Pre-installation check
        $error = '';

        if ((strlen($option['username']) < 3) || (strlen($option['username']) > 20)) {
            $error .= 'ERROR: Username must be between 3 and 20 characters!' . "\n";
        }

        if ((strlen($option['email']) > 96) || !filter_var($option['email'], FILTER_VALIDATE_EMAIL)) {
            $error .= 'ERROR: E-Mail Address does not appear to be valid!' . "\n";
        }

        // If not cloud then we validate the password
        $password = html_entity_decode($option['password'], ENT_QUOTES, 'UTF-8');

        if ((strlen($password) < 5) || (strlen($password) > 20)) {
            $error .= 'ERROR: Password must be between 5 and 20 characters!' . "\n";
        }

        if ($error) {
            $output  = 'ERROR: Validation failed: ' . "\n";
            $output .= $error . "\n\n";

            return $output;
        }

        try {

            $this->model_install_db->install_database($option);
        } catch (\PDOException $e) {
            if($e->getCode() == 2002){
                echo "Error: is database service running ?\n";
                die();
            }
            throw $e;
        }

        // Write config files
        define('HTTP_SOUK', $option['http_server']);
	    $this->model_install_config->create_file($option);

        // Return success message
        $output  = 'SUCCESS! Souk successfully installed on your server' . "\n";
        $output .= 'Store link: ' . $option['http_server'] . "\n";
        $output .= 'Admin link: ' . $option['http_server'] . 'admin/' . "\n\n";

        return $output;
    }

    public function usage() {
        $option = implode(' ', [
            '--username',
            'admin',
            '--email',
            'email@example.com',
            '--password',
            'password',
            '--http_server',
            'http://localhost:9000',
            '--db_system',
            'mysql',
            '--db_hostname',
            'localhost',
            '--db_username',
            'root',
            '--db_password',
            'pass',
            '--db_database',
            'souk',
            '--db_port',
            '3306',
            '--db_prefix',
            'sk_'
        ]);

        $output  = 'Usage:' . "\n";
        $output .= '======' . "\n\n";
        $output .= 'php cli_install.php install ' . $option . "\n\n";

        return $output;
    }
}


// Controller
$controller = new CliInstall($registry);
$controller->index();

// Output
$response->send();
