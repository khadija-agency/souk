<?php
class ModelInstallConfig extends Europa\Model {

    public function upgrade_file(): void {
        // check if on upgrade process
        if(defined('DB_DRIVER') && !defined('DB_SYSTEM')){
            define('DB_SYSTEM', DB_DRIVER);
        }

        $data['db_system']    = DB_SYSTEM;
        $data['db_hostname']  = DB_HOSTNAME;
        $data['db_username']  = DB_USERNAME;
        $data['db_password']  = DB_PASSWORD;
        $data['db_database']  = DB_DATABASE;
        $data['db_port']      = DB_PORT;
        $data['db_prefix']    = DB_PREFIX;

        $this->create_file($data);

    }
    public function create_file(array $data): void {
        // Catalog config.php
        $output  = '<?php' . "\n";
        $output .= '// HTTP' . "\n";
        $output .= 'define(\'HTTP_SERVER\', \'' . HTTP_SOUK . '\');' . "\n\n";

        $output .= '// HTTPS' . "\n";
        $output .= 'define(\'HTTPS_SERVER\', \'' . HTTP_SOUK . '\');' . "\n\n";

        $output .= '// DIR' . "\n";
        $output .= 'define(\'DIR_APPLICATION\', \'' . addslashes(DIR_SOUK) . 'catalog/\');' . "\n";
        $output .= 'define(\'DIR_SYSTEM\', \'' . addslashes(DIR_SOUK) . 'system/\');' . "\n";
        $output .= 'define(\'DIR_IMAGE\', \'' . addslashes(DIR_SOUK) . 'image/\');' . "\n";
        $output .= 'define(\'DIR_STORAGE\', DIR_SYSTEM . \'storage/\');' . "\n";
        $output .= 'define(\'DIR_LANGUAGE\', DIR_APPLICATION . \'language/\');' . "\n";
        $output .= 'define(\'DIR_TEMPLATE\', DIR_APPLICATION . \'view/theme/\');' . "\n";
        $output .= 'define(\'DIR_CONFIG\', DIR_SYSTEM . \'config/\');' . "\n";
        $output .= 'define(\'DIR_CACHE\', DIR_STORAGE . \'cache/\');' . "\n";
        $output .= 'define(\'DIR_DOWNLOAD\', DIR_STORAGE . \'download/\');' . "\n";
        $output .= 'define(\'DIR_LOGS\', DIR_STORAGE . \'logs/\');' . "\n";
        $output .= 'define(\'DIR_MODIFICATION\', DIR_STORAGE . \'modification/\');' . "\n";
        $output .= 'define(\'DIR_SESSION\', DIR_STORAGE . \'session/\');' . "\n";
        $output .= 'define(\'DIR_UPLOAD\', DIR_STORAGE . \'upload/\');' . "\n\n";

        $output .= '// DB' . "\n";
        $output .= 'define(\'DB_SYSTEM\', \'' . addslashes($data['db_system']) . '\');' . "\n";
        $output .= 'define(\'DB_HOSTNAME\', \'' . addslashes($data['db_hostname']) . '\');' . "\n";
        $output .= 'define(\'DB_USERNAME\', \'' . addslashes($data['db_username']) . '\');' . "\n";
        $output .= 'define(\'DB_PASSWORD\', \'' . addslashes(html_entity_decode($data['db_password'], ENT_QUOTES, 'UTF-8')) . '\');' . "\n";
        $output .= 'define(\'DB_DATABASE\', \'' . addslashes($data['db_database']) . '\');' . "\n";
        $output .= 'define(\'DB_PORT\', \'' . addslashes($data['db_port']) . '\');' . "\n";
        $output .= 'define(\'DB_PREFIX\', \'' . addslashes($data['db_prefix']) . '\');';

        $file = fopen(DIR_SOUK . 'config.php', 'w');

        fwrite($file, $output);

        fclose($file);

        // Admin config.php
        $output  = '<?php' . "\n";
        $output .= '// HTTP' . "\n";
        $output .= 'define(\'HTTP_SERVER\', \'' . HTTP_SOUK . 'admin/\');' . "\n";
        $output .= 'define(\'HTTP_CATALOG\', \'' . HTTP_SOUK . '\');' . "\n\n";

        $output .= '// HTTPS' . "\n";
        $output .= 'define(\'HTTPS_SERVER\', \'' . HTTP_SOUK . 'admin/\');' . "\n";
        $output .= 'define(\'HTTPS_CATALOG\', \'' . HTTP_SOUK . '\');' . "\n\n";

        $output .= '// DIR' . "\n";
        $output .= 'define(\'DIR_APPLICATION\', \'' . addslashes(DIR_SOUK) . 'admin/\');' . "\n";
        $output .= 'define(\'DIR_SYSTEM\', \'' . addslashes(DIR_SOUK) . 'system/\');' . "\n";
        $output .= 'define(\'DIR_IMAGE\', \'' . addslashes(DIR_SOUK) . 'image/\');' . "\n";
        $output .= 'define(\'DIR_STORAGE\', DIR_SYSTEM . \'storage/\');' . "\n";
        $output .= 'define(\'DIR_CATALOG\', \'' . addslashes(DIR_SOUK) . 'catalog/\');' . "\n";
        $output .= 'define(\'DIR_LANGUAGE\', DIR_APPLICATION . \'language/\');' . "\n";
        $output .= 'define(\'DIR_TEMPLATE\', DIR_APPLICATION . \'view/template/\');' . "\n";
        $output .= 'define(\'DIR_CONFIG\', DIR_SYSTEM . \'config/\');' . "\n";
        $output .= 'define(\'DIR_CACHE\', DIR_STORAGE . \'cache/\');' . "\n";
        $output .= 'define(\'DIR_DOWNLOAD\', DIR_STORAGE . \'download/\');' . "\n";
        $output .= 'define(\'DIR_LOGS\', DIR_STORAGE . \'logs/\');' . "\n";
        $output .= 'define(\'DIR_MODIFICATION\', DIR_STORAGE . \'modification/\');' . "\n";
        $output .= 'define(\'DIR_SESSION\', DIR_STORAGE . \'session/\');' . "\n";
        $output .= 'define(\'DIR_UPLOAD\', DIR_STORAGE . \'upload/\');' . "\n\n";

        $output .= '// DB' . "\n";
        $output .= 'define(\'DB_SYSTEM\', \'' . addslashes($data['db_system']) . '\');' . "\n";
        $output .= 'define(\'DB_HOSTNAME\', \'' . addslashes($data['db_hostname']) . '\');' . "\n";
        $output .= 'define(\'DB_USERNAME\', \'' . addslashes($data['db_username']) . '\');' . "\n";
        $output .= 'define(\'DB_PASSWORD\', \'' . addslashes(html_entity_decode($data['db_password'], ENT_QUOTES, 'UTF-8')) . '\');' . "\n";
        $output .= 'define(\'DB_DATABASE\', \'' . addslashes($data['db_database']) . '\');' . "\n";
        $output .= 'define(\'DB_PORT\', \'' . addslashes($data['db_port']) . '\');' . "\n";
        $output .= 'define(\'DB_PREFIX\', \'' . addslashes($data['db_prefix']) . '\');' . "\n\n";

        $output .= '// Souk API' . "\n";
        $output .= 'define(\'SOUK_SERVER\', \'https://khadija.agency/\');' . "\n";

        $file = fopen(DIR_SOUK . 'admin/config.php', 'w');

        fwrite($file, $output);

        fclose($file);
    }
}
