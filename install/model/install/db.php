<?php
class ModelInstallDb extends Europa\Model {

    public function install_database(array $data): void {

        $db = $this->model_system_db->new_db($data);

        // prefix
        $prefix = $db->get_prefix();

        $this->model_system_db->setup_db($db);

        // delete tables with same name
        $tables = $db->list_tables();
        $this->model_system_db->delete_tables($db, $tables);

        // create tables
        $this->model_system_db->create_tables($db);

        // insert data
        $this->model_system_db->insert_data($db);

        // add constraints
        $this->model_system_db->add_constraints($db);

        // setup application
        $this->app_setup($db, $data);
    }

    public function app_setup($db, $data) {
        $pdo    = $db->get_pdo();
        $prefix = $db->get_prefix();

        $store_update = $pdo->prepare("UPDATE ".$prefix."store SET
            store_name = :store_name,
            store_url = :store_url,
            store_ssl = :store_ssl
            WHERE store_id = :store_id;");

        $setting_insert = $pdo->prepare("INSERT
            INTO " . $prefix . "setting
            (store_id, code, setting_key, value, serialized)
            VALUES
            (1, 'config', :key, :value, false)");

        $setting_delete = $pdo->prepare("DELETE
            FROM " . $prefix . "setting
            WHERE setting_key = :key");

        $user_delete = $pdo->prepare("DELETE
            FROM " . $prefix . "user
            WHERE user_id = '1'");

        $user_insert = $pdo->prepare("INSERT
            INTO " . $prefix . "user (
                user_id,
                user_group_id,
                username,
                password,
                firstname,
                lastname,
                email,
                status,
                image,
                code,
                ip,
                date_added
        ) VALUES (
            '1',
            '1',
            :username,
            :password,
            'System',
            'Admin',
            :email,
            '1',
            'profile.png',
            :ip,
            true,
            NOW()
        )");

        $product_view = $pdo->prepare("UPDATE
            " . $prefix . "product
            SET viewed = '0'");

        $api_insert = $pdo->prepare("INSERT
            INTO " . $prefix . "api
            (username, api_key, status)
            VALUES
            (:username, :api_key, true)");

        $invoice_update = $pdo->prepare("UPDATE
            " . $prefix . "setting SET
            value = :value
            WHERE setting_key = 'config_invoice_prefix'");

        if(!isset($data['http_server'])){
            throw new exception("http_server missing data array.");
        }
        $host = parse_url($data['http_server']);
        $host = $host['host'];

        $store_update->execute([
            'store_id'   => 1,
            'store_name' => $host,
            'store_url'  => $data['http_server'],
            'store_ssl'  => str_replace('http', 'https', $data['http_server'])
        ]);
        unset($host);

        $setting_insert->execute(array(
            'key'     => 'system',
            'value'   => '1.1.0'
        ));

        $user_delete->execute();

        $user_insert->execute([
            'username'  => $data['username'],
            'password'  => password_hash(html_entity_decode($data['password'], ENT_QUOTES, 'UTF-8'), PASSWORD_DEFAULT),
            'email'     => $data['email'],
            'ip'        => '127.0.0.1'
        ]);

        $setting_delete->execute(array(
            'key' => 'config_email'
        ));

        $setting_insert->execute(array(
            'key'          => 'config_email',
            'value'   => $data['email']
        ));

        $product_view->execute();

        $setting_delete->execute(array(
            'key'   => 'config_encryption'
        ));

        $setting_insert->execute(array(
            'key'          => 'config_encryption',
            'value'   => token(512)
        ));

        $api_insert->execute(array(
            'username'  => $data['username'],
            'api_key'   => token(256)
        ));

        $api_id = $pdo->LastInsertId();

        $setting_delete->execute(array(
            'key'   => 'config_api_id'
        ));

        $setting_insert->execute(array(
            'key'          => 'config_api_id',
            'value'   => $api_id
        ));

        $invoice_update->execute(["INV-" . date('Y') . "-00"]);
    }
}
