<?php
// new_db(array $data): bool
// receives connection data array
// return new database object

// setup_db(i_database $db): void
// receives db and
// set global database system configuration

// delete_tables(i_database $db, array $tables){
// receives db and array of tables to delete

// create_tables(i_database $db, string $p_orig = null, string $p_dest = null, string $sql_files_path = null)
// create tables from sql files
// p_orig table prefix on sql files
// p_dest created tables prefix

// insert_data(i_database $db, string $p_orig = null, string $p_dest = null, string $sql_files_path = null)
// insert data from sql files
// p_orig table prefix on sql files
// p_dest data destiny tables prefix

// add_constraints(i_database $db, string $prefix = null, string $sql_files_path = null)
// add constraints to tables

// copy_tables(i_database $db, array $tables, string $p_orig = null, string $p_dest = null)
// copy data from tables array to tables prefixed with p_dest

// replace_tables(i_database $db, array $tables, string $p_orig = null, string $p_dest = null)
// replace array of tables names
// p_orig table prefix replaced tables; 'sk_'
// p_dest table prefix to replace with ; 'upgrade_'

class ModelSystemDb extends Europa\Model {

    public function new_db(array $data): Europa\i_database|bool {
        $db = new Europa\database(
            $data['db_system'],
            $data['db_hostname'],
            $data['db_username'],
            $data['db_password'],
            $data['db_database'],
            $data['db_port'],
            $data['db_prefix']
        );

        if(($pdo = $db->get_pdo()) == -1){
            throw $db->get_error();
        }
        return $db;
    }

    public function setup_db(Europa\i_database $db): void {
        $pdo = $db->get_pdo();
        $database = $db->get_database();

        if($db->get_system() == "mysql"){
            $pdo->query("SET default_storage_engine=InnoDB;");
            $pdo->query("SET storage_engine=InnoDB;");
            $pdo->query("ALTER DATABASE ".$database." CHARACTER SET utf8 COLLATE utf8_general_ci");
        }
    }

    public function delete_tables(Europa\i_database $db, array $tables){
        $pdo = $db->get_pdo();
        foreach($tables as $table) {
            $pdo->exec("DROP TABLE " . $table);
        }

    }

    public function create_tables(Europa\i_database $db, string $p_orig = null, string $p_dest = null, string $sql_files_path = null) {

        $pdo    = $db->get_pdo();
        $system = $db->get_system();

        // get prefix
        if(is_null($p_orig)){
            // by default sql files
            $p_orig = "sk_";
        }
        if(is_null($p_dest)){
            $p_dest = $db->get_prefix();
        }

        // get list of sql table files
        if(is_null($sql_files_path)) {
            $sql_files_path = DIR_APPLICATION . "db/tables/*.sql";
        }
        $files = glob($sql_files_path);

        // start loading table structure files
        if($system == "mysql"){
            $pdo->query("SET FOREIGN_KEY_CHECKS = 0;");
        }

        try {
            foreach($files as $file) {

                // load sql from file
                $sql = file_get_contents($file);
                // set correct prefix
                $sql = str_replace(
                    "CREATE TABLE " . $p_orig,
                    "CREATE TABLE " . $p_dest,
                    $sql
                );
                // tell mysql serial id's are unsigned
                if($system == "mysql"){
                    $sql = str_replace(
                        "_id BIGINT ",
                        "_id BIGINT UNSIGNED ",
                        $sql
                    );
                }
                // create table
                $pdo->exec($sql);

            }
        } catch (Exception $e) {

            if($system == "mysql"){
                $pdo->query("SET FOREIGN_KEY_CHECKS = 1;");
            }
            echo "Error: sql error on table '" . $file . "'<br>";
            echo $e->getMessage();
            echo "<hr/>";
            var_dump($sql);
            exit(1);
        }
        if($system == "mysql"){
            $pdo->query("SET FOREIGN_KEY_CHECKS = 1;");
        }
    }

    public function insert_data(Europa\i_database $db, string $p_orig = null, string $p_dest = null, string $sql_files_path = null) {

        $pdo    = $db->get_pdo();

        // get prefix
        if(is_null($p_orig)){
            // by default sql files
            $p_orig = "sk_";
        }
        if(is_null($p_dest)){
            $p_dest = $db->get_prefix();
        }

        // get list of sql data files
        if(is_null($sql_files_path)) {
            $sql_files_path = DIR_APPLICATION . "db/data/*.sql";
        }
        $files = glob($sql_files_path);

        try {

            foreach($files as $file) {
                // load sql
                $sql = file_get_contents($file);
                // set correct prefix
                $sql = str_replace(
                    "INSERT INTO " . $p_orig,
                    "INSERT INTO " . $p_dest,
                    $sql
                );
                // insert data
                $pdo->exec($sql);
            }

        } catch( PDOException $e) {
            echo "Error: data insert '".$file."'\n";
            echo $e->getMessage()."\n";
            exit(1);
        }
    }

    public function add_constraints(Europa\i_database $db, string $prefix = null, string $sql_files_path = null) {

        $pdo    = $db->get_pdo();

        // get prefix
        if(is_null($prefix)){
            $prefix = $db->get_prefix();
        }

        // get list of sql key files
        if(is_null($sql_files_path)) {
            $sql_files_path = DIR_APPLICATION . "db/keys/*.sql";
        }
        $files = glob($sql_files_path);

        try {

            foreach($files as $file) {
                // load sql
                $sql = file_get_contents($file);
                // set correct prefix
                $sql = str_replace("ALTER TABLE sk_", "ALTER TABLE " . $prefix, $sql);
                $sql = str_replace("ADD CONSTRAINT sk_", "ADD CONSTRAINT " . $prefix, $sql);
                $sql = str_replace("REFERENCES sk_", "REFERENCES " . $prefix, $sql);

                // add constraints
                $pdo->exec($sql);
            }

        } catch( PDOException $e) {
            echo "Error: data keys '".$file."'\n";
            echo $e->getMessage()."\n";
            echo "<hr/>";
            var_dump($sql);
            exit(1);
        }

    }

    public function copy_tables(Europa\i_database $db, array $tables, string $p_orig = null, string $p_dest = null) {

        $pdo    = $db->get_pdo();

        // get prefix from origin tables
        if(is_null($p_orig)){
            $p_orig = $db->get_prefix();
        }
        $np_orig = strlen($p_orig);

        // get prefix from destiny tables
        if(is_null($p_dest)){
            $p_dest = 'upgrade_';
        }

        $error = array();
        foreach($tables as $table) {

            if(strncmp($table, $p_orig, $np_orig) != 0){
                continue;
            }

            $orig_cols = $db->list_columns($table);

            $dest = str_replace (
                $p_orig,
                $p_dest,
                $table
            );

            // copy the data from the table
            // to the new table
            $sql = "INSERT INTO ".$dest." (";
            foreach($orig_cols as $col) {
                $sql .= $col.", ";
            }
            $sql = rtrim($sql, ", ");
            $sql .= ") SELECT ";
            foreach($orig_cols as $col) {
                $sql .= $col.", ";
            }
            $sql = rtrim($sql, ", ");
            $sql .= " FROM ".$table.";";

            try {
                $pdo->exec($sql);
            } catch (\PDOException $e){
                // will be treated later
                $error[$table] = array (
                        'dest' => $dest,
                        'sql' => $sql,
                        'e' => $e
                );
                continue;
            } // try
        } // for table cycle

        if(!empty($error)){
            return $error;
        }

        return true;
    }

    public function replace_tables(Europa\i_database $db, array $tables, string $p_orig = null, string $p_dest = null) {
        $pdo       = $db->get_pdo();

        // get prefix from origin tables
        if(is_null($p_orig)){
            $p_orig = $db->get_prefix();
        }
        $np_orig = strlen($p_orig);

        // get prefix from destiny tables
        if(is_null($p_dest)){
            $p_dest = 'upgrade_';
        }
        $np_dest = strlen($p_dest);

        // all previous tables
        $error=array();

        foreach($tables as $table) {
            if(strncmp($table, $p_dest, $np_dest) == 0){
                continue;
            }

            $orig = str_replace (
                $p_orig,
                $p_dest,
                $table
            );

            try {
            // delete old table and rename the new
            $pdo->exec("DROP TABLE ".$table.";");

            $pdo->exec("ALTER TABLE ".$orig."
                RENAME TO ".$table.";");

            } catch (\PDOException $e){
                // will be treated later
                $error[$table] = array (
                        'orig' => $orig,
                        'sql' => $sql,
                        'e' => $e
                );
                continue;
            } // try
        }// for
        if(!empty($error)){
            return $error;
        }

        return true;

    }
}
