<?php
class ModelUpgrade1002Columns extends Europa\Model {

    public function upgrade() {

        $db        = $this->get_db();
        $pdo       = $db->get_pdo();
        $prefix    = $db->get_prefix();

        $tables = $db->list_tables();
        foreach($tables as $table) {
            if(strncmp($table, "upgrade_", 8) == 0){
                continue;
            }
            $new_table = str_replace(
                $prefix,
                "upgrade_",
                $table
            );

            $orig_cols = $db->list_columns($table);
            $new_cols = $db->list_columns($new_table);

            $diff =  array_diff($new_cols, $orig_cols);
            $ndiff = count($diff);
            if($ndiff == 0) {
                continue;
            }

            $odiff =  array_diff($orig_cols, $new_cols);

            foreach($diff as $key => $value) {
                if(!isset($orig_cols[$key])){
                    continue;
                }
                $sql = "ALTER TABLE ".$table." RENAME COLUMN `".$odiff[$key]."` TO ". $value;
                $pdo->exec($sql);
            }
        }
    }
}
