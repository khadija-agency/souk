<?php
class ModelUpgrade1004Structure extends Europa\Model {

    public function upgrade() {

        $db        = $this->get_db();
        $pdo       = $db->get_pdo();
        $prefix    = $db->get_prefix();

        $this->load->model('system/db');

        // all previous tables
        $tables = $db->list_tables();
        $rename = array();
        foreach($tables as $table) {
            if(strncmp($table, "upgrade_", 8) != 0){
                array_push($rename, $table);
            }
        }
        $this->model_system_db->replace_tables($db, $rename, $prefix, "upgrade_");

        // new tables
        $tables = $db->list_tables();
        foreach($tables as $table) {
            if(strncmp($table, "upgrade_", 8) == 0){
                $pdo->exec("ALTER TABLE ".$table."
                    RENAME TO ".str_replace("upgrade_", "", $table).";");
            }
        }
    }
}
