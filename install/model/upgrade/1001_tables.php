<?php
class ModelUpgrade1001Tables extends Europa\Model {

    public function upgrade() {
        // Database upgrade
        $db     = $this->get_db();

        $this->load->model('system/db');

        //emulate install of v1
        $this->emu($db);

        // setup sql database system
        $this->model_system_db->setup_db($db);

        // delete tables with same name
        $tables = $db->list_tables();
        $delete = array();
        foreach($tables as $table) {
            if(strncmp($table, "upgrade_", 8) == 0){
                array_push($delete, $table);
            }
        }
        if(!empty($delete)){
            $this->model_system_db->delete_tables($db, $delete);
        }

        // create new tables
        $this->model_system_db->create_tables($db, 'sk_', 'upgrade_');

        // add constraints
        $this->model_system_db->add_constraints($db, 'upgrade_');

    }
}
