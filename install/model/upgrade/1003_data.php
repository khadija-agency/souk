<?php
class ModelUpgrade1003Data extends Europa\Model {

    public function upgrade() {

        $db     = $this->get_db();
        $pdo    = $db->get_pdo();
        $prefix = $db->get_prefix();

        $this->load->model('system/db');

        $tables = $db->list_tables();
        $copy = array();
        foreach($tables as $table) {
            if(strncmp($table, "upgrade_", 8) != 0){
                array_push($copy, $table);
            }
        }

        $error = $this->model_system_db->copy_tables($db, $copy, $prefix, 'upgrade_');

        foreach($error as $table => $data){

            switch ($table) {
            case $prefix."session" :
                $sql = "INSERT INTO upgrade_session
                    (session_id, data, expire)
                    SELECT session_id, data, CONVERT_TZ(expire, '+00:00', @@session.time_zone)
                    FROM ".$prefix."session;";
                $pdo->exec($sql);
                unset($error[$table]);
                break;

            case $prefix."order_status" :
                // copy data to order_status
                $sql = "INSERT INTO upgrade_order_status
                    (order_status_id)
                    SELECT DISTINCT order_status_id
                    FROM ".$prefix."order_status";
                $pdo->exec($sql);

                // copy data to order_status_name
                $sql = "INSERT INTO upgrade_order_status_name
                    (order_status_id, language_id, name)
                    SELECT order_status_id, language_id, name
                    FROM ".$prefix."order_status";
                $pdo->exec($sql);
                unset($error[$table]);
                break;

            case $prefix."return_action" :
                // copy data to return_action
                $sql = "INSERT INTO upgrade_return_action
                    (return_action_id)
                    SELECT DISTINCT return_action_id
                    FROM ".$prefix."return_action";

                // copy data to return_action_name
                $sql = "INSERT INTO upgrade_return_action_name
                    (return_action_id, language_id, name)
                    SELECT return_action_id, language_id, name
                    FROM ".$prefix."return_action";
                $pdo->exec($sql);
                unset($error[$table]);
                break;

            case $prefix."return_status" :
                // copy data to return_status
                $sql = "INSERT INTO upgrade_return_status
                    (return_status_id)
                    SELECT DISTINCT return_status_id
                    FROM ".$prefix."return_status";
                $pdo->exec($sql);

                // copy data to return_status_name
                $sql = "INSERT INTO upgrade_return_status_name
                    (return_status_id, language_id, name)
                    SELECT return_status_id, language_id, name
                    FROM ".$prefix."return_status";
                $pdo->exec($sql);
                unset($error[$table]);
                break;

            case $prefix."stock_status" :
                // copy data to stock_status
                $sql = "INSERT INTO upgrade_stock_status
                    (stock_status_id)
                    SELECT DISTINCT stock_status_id
                    FROM ".$prefix."stock_status";
                $pdo->exec($sql);

                // copy data to stock_status_name
                $sql ="INSERT INTO upgrade_stock_status_name
                    (stock_status_id, language_id, name)
                    SELECT stock_status_id, language_id, name
                    FROM ".$prefix."stock_status";

                $pdo->exec($sql);
                unset($error[$table]);
                break;

            } // switch
        } // for table cycle
        if(!empty($error)){
            foreach($error as $table => $data){

                var_dump($table);
                var_dump($data['sql']);
                var_dump($data['e']->getMessage());
                echo "<hr>";
            }
            exit();
        }
    }// function

} //class
