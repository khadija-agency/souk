<?php
class ModelUpgrade1000Config extends Europa\Model {

    public function upgrade() {
        $this->load->model('install/config');
        $this->model_install_config->upgrade_file();
    }
}
