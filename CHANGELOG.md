# Souk change log

## [v1.1] (Release date: 12.8.2023)

Database revision with normalization fixes
New database class with PDO support added
Login revision with pdo prepared statements

## [v1.0] (Release date: 10.10.2022)

First Souk 1.0.0 release.
Fork from oc 3.0.3.8.
Added conodr patches.
installer, languages and database.
