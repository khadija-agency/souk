<?php
/**
 * @package     Souk
 * @author      Khadija Agency
 * @copyright   Copyright (c) 2022 - 2022, Agentia Khadija, Ltd. (https://khadija.agency/)
 * @license     https://opensource.org/licenses/GPL-3.0
 * @link        https://khadija.agency
 */

/**
* Action class
*/
namespace Europa;
class action implements i_action {
	private $id;
	private $route;
	private $method = 'index';

	public function __construct(string $route) {
        $this->route = null;
		$this->id = $route;

		$parts = explode('/', preg_replace('/[^a-zA-Z0-9_\/]/', '', (string)$route));

		// Break apart the route
		while ($parts) {
			$file = DIR_APPLICATION . 'controller/' . implode('/', $parts) . '.php';

			if (is_file($file)) {
				$this->route = implode('/', $parts);

				break;
			} else {
				$this->method = array_pop($parts);
			}
		}
	}

	public function getId():string {
		return $this->id;
	}

	public function execute(i_data $registry, array $args = array()) {
		if($this->route == null){
			return new \Exception('Error: Route is null ' . $this->route . '/' . $this->method . '!');
		}
		// Stop any magical methods being called
		if (substr($this->method, 0, 2) == '__') {
			return new \Exception('Error: Calls to magic methods are not allowed!');
		}

		$file  = DIR_APPLICATION . 'controller/' . $this->route . '.php';
		$class = 'Controller' . preg_replace('/[^a-zA-Z0-9]/', '', $this->route);

		// Initialize the class
		if (is_file($file)) {
			include_once($file);

			$controller = new $class($registry);
		} else {
			return new \Exception('Error: Could not call ' . $this->route . '/' . $this->method . '!');
		}

		$reflection = new \ReflectionClass($class);

		if ($reflection->hasMethod($this->method) && $reflection->getMethod($this->method)->getNumberOfRequiredParameters() <= count($args)) {
			return call_user_func_array(array($controller, $this->method), $args);
		} else {
			return new \Exception('Error: Could not call ' . $this->route . '/' . $this->method . '!');
		}
	}
}
