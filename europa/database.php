<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

namespace Europa;

use Europa\i_database as i_database;
use Europa\i_config as i_config;
use \PDO as PDO;

class database implements i_database{

    private $sslmode    = null;
    private static $pdo = null;

    // by order of __construct
    private $system     = null;
    private $hostname   = null;
    private $username   = null;
    private $password   = null;
    private $database   = null;
    private $port       = null;
    private $prefix     = null;
    private $charset    = null;
    private $options    = null;

    public function list_tables():bool|array {
        $list = array();

        if(self::$pdo === null){
            return false;
        }

        switch ($this->system){
            case "mysql":
                $list = self::$pdo->query("SHOW TABLES")
                    ->fetchAll(\PDO::FETCH_COLUMN);
                break;
            case "pgsql":
                $list = self::$pdo->query('SELECT table_name
                    FROM information_schema.tables
                    WHERE table_schema = \'public\'')
                    ->fetchAll(\PDO::FETCH_COLUMN);
                break;
            default:
                $list = false;
        }

        return $list;
    }
    public function list_columns(string $table):bool|array{
        $list = array();

        if(self::$pdo === null){
            return false;
        }

        switch ($this->system){
            case "mysql":
                $st = self::$pdo->prepare("DESCRIBE ".$table);
                $st->execute();
                $list =  $st->fetchAll(\PDO::FETCH_COLUMN);
                break;
            case "pgsql":
                $st = self::$pdo->prepare("SELECT column_name
                    FROM information_schema.columns
                    WHERE table_name = :table_name");
                $st->bindParam(':table_name', $table);
                $st->execute();
                $list = $st->fetchAll(\PDO::FETCH_COLUMN);
                break;
            default:
                $list = false;
        }

        return $list;
    }
    public function set_prefix(string $prefix){
        $this->prefix = $prefix;
    }
    public function set_system(string $system){
        // postgresql or mysql
        $this->system = null;
        if($system == "pgsql" || $system == "postgresql") {
            $this->system = "pgsql";
        }
        if($system == "mysql" || $system == "mysqli") {
            $this->system = "mysql";
        }
        if(is_null($this->system)){
            return false;
        }
        return true;
    }
    public function set_hostname(string $hostname){
        $this->hostname = $hostname;
    }
    public function set_username(string $username){
        $this->username = $username;
    }
    public function set_password(string $password){
        $this->password = $password;
    }
    public function set_database(string $database){
        $this->database = $database;
    }
    public function set_charset(string $charset){
        $this->charset = $charset;
    }
    public function set_sslmode(string $mode){
        $this->sslmode = $mode;
    }
    public function set_port(int $port){
        $this->port = $port;
    }
    public function set_options(array $options){
        $this->options = array_merge($this->options, $options);
    }
    public function get_prefix():?string{
        return $this->prefix;
    }
    public function get_system():?string{
        return $this->system;
    }
    public function get_database():?string{
        return $this->database;
    }
    public function get_dsn(){
        if(is_null($this->system)){
            return false;
        }
        $dsn = $this->system.":";

        if(is_null($this->hostname)){
            return false;
        }
        $dsn .= "host=".$this->hostname.";";

        if(is_null($this->database)){
            return false;
        }
        $dsn .= "dbname=".$this->database.";";

        if(!is_null($this->charset)){
            if($this->system == "mysql"){
                $dsn .= "charset=".$this->charset.";";
            }else if($this->system == "pgsql"){
                $dsn .="options='--client_encoding=".$this->charset."'";
            }
        }

        if(!is_null($this->sslmode)){
            $dsn .= "sslmode=".$this->sslmode.";";
        }
        if(is_null($this->port)){
            return false;
        }
        $dsn .= "port=".$this->port.";";
        return $dsn;
    }

    public function set_config(i_config $config):bool{
        $complete = true;

        if($config->has("system", "database")){
            $this->set_system($config->get("system", "database"));
        }else{
            $complete = false;
        }

        if($config->has("username", "database")){
            $this->set_username($config->get("username", "database"));
        }else{
            $complete = false;
        }

        if($config->has("password", "database")){
            $this->set_password($config->get("password", "database"));
        }else{
            $complete = false;
        }

        if($config->has("database", "database")){
            $this->set_database($config->get("database", "database"));
        }else{
            $complete = false;
        }

        if($config->has("sslmode", "database")){
            $this->set_sslmode($config->get("sslmode", "database"));
        }

        if($config->has("hostname", "database")){
            $this->set_hostname($config->get("hostname", "database"));
        }else{
            $complete = false;
        }

        if($config->has("port", "database")){
            $this->set_port($config->get("port", "database"));
        }else{
            $complete = false;
        }

        if($config->has("charset", "database")){
            $this->set_charset($config->get("charset", "database"));
        }

        if($config->has("prefix", "database")){
            $this->set_prefix($config->get("prefix", "database"));
        }

        return $complete;
    }

    public function get_pdo(){
        if(!is_null(self::$pdo)){
            return self::$pdo;
        }

        $pdo = new PDO(
            $this->get_dsn(),
            $this->username,
            $this->password,
            $this->options
        );

        self::$pdo = $pdo;

        return $pdo;
    }

    public function __construct(
        string $system = null,
        string $hostname = null,
        string $username = null,
        string $password = null,
        string $database = null,
        string $port = null,
        string $prefix = null,
        string $charset = null,
        array $options = array()) {

        if(!is_null($system)){
            $this->set_system($system);
        }
        if(!is_null($hostname)){
            $this->set_hostname($hostname);
        }
        if(!is_null($username)){
            $this->set_username($username);
        }
        if(!is_null($password)){
            $this->set_password($password);
        }
        if(!is_null($database)){
            $this->set_database($database);
        }
        $this->set_charset("utf8");
        if(!is_null($charset)){
            $this->set_charset($charset);
        }
        if(!is_null($port)){
            $this->set_port($port);
        }

        $this->options = array(
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::ATTR_PERSISTENT         => true
        );

        if(!empty($options)) {
            $this->set_options($options);
        }

        if(!is_null($prefix)) {
            $this->prefix=$prefix;
        }
    }
}
