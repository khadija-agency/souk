
    private function get_authorization_head(){
        $heads = null;

        if (isset($_SERVER['Authorization'])) {
            $heads = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $heads = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } else if (function_exists('apache_request_heads')) {
            $requestHeaders = apache_request_heads();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $heads = trim($requestHeaders['Authorization']);
            }
        }

        return $heads;
    }

    private function get_bearer_token() {
        $heads = get_authorization_head();

        // HEADER: Get the access token from the head
        if (!empty($heads)) {
            if (preg_match('/Bearer\s(\S+)/', $heads, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

