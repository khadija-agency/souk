<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */


include_once("europa.php");

$config = new Europa\config();
$config->set_file("cfg.ini");
$config->set("key", "value");
$config->set("user", "admin", "database");
$config->save();

unset($config);

$config = new Europa\config();
$config->parse("cfg.ini");

if($config->get("key") != "value"){
    echo "ERROR: config get function return wrong value";
}

if($config->get("user", "database") != "admin") {
    echo "ERROR: config get function with section return wrong value";
}
unlink("cfg.ini");
