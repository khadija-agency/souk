<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */


include_once("europa.php");

$config = new Europa\config();
$config->set("system", "pgsql", "database");
$config->set("username", "user_souk", "database");
$config->set("password", "souk", "database");
$config->set("database", "db_souk", "database");
$config->set("hostname", "localhost", "database");
$config->set("port", "5432", "database");
$config->set("prefix", "sk_", "database");
$config->save("config.ini");

$database = new Europa\database();
$database->set_config($config);
//var_dump($database->get_dsn());
$test = array("first" => "fvalue");
$stest = array("first" => "svalue", "second" => "avalue");

//
//if($config->get("key") != "value"){
//    echo "ERROR: config get function return wrong value";
//}
//
//if($config->get("user", "database") != "admin") {
//    echo "ERROR: config get function with section return wrong value";
//}
