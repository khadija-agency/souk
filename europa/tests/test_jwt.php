<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */


include_once("europa.php");

$jwt = new Europa\jwt();

$secret = random_bytes(24);

$jwt->set("token", base64_encode(random_bytes(12)));
$date = date("c");
$jwt->set("stamp", $date);
$jwt->set_secret($secret);
$jwt->set_expire(3200);

$token = $jwt->get_token();
if($jwt->is_valid() !== true){
    echo "ERROR: jwt is_valid function return wrong is_validation";
}

// tamper data
$jwt->set['expire'] = 0;
$jwt->set['stamp'] = '0000';
$jwt->set("token", base64_encode(random_bytes(12)));
if($jwt->is_valid() !== false){
    echo "ERROR: jwt is_valid function return other than false";
}

unset($jwt);
$jwt = new Europa\jwt();
$jwt->set_secret($secret);
$jwt->set_token($token);
if($jwt->is_valid() !== true){
    echo "ERROR: jwt is_valid function return wrong is_validation after set";
}

if($jwt->get("stamp") !== $date){
    echo "ERROR: jwt get function return wrong value";
}
