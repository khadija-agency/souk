<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */


include_once("europa.php");

$data = new Europa\data();

// check if set returns boolean (true)
if(!is_bool($data->set("test", "test_value"))){
    echo "ERROR: data set function return non-boolean";
}

// check previous set and has function
if(!is_bool($data->has("test"))){
    echo "ERROR: data has function return non-boolean";
}

// check has with non existent key
if(!is_bool($data->has("other_test"))){
    echo "ERROR: data has function return non-boolean";
}

// check get function return value
if($data->get("test") !== "test_value"){
    echo "ERROR: data get function return wrong value";
}

// check get function return of non existing key
if(!is_null($data->get("another_test"))){
    echo "ERROR: data get function return wrong value on non existing key";
}

// check del function of non existing key
if(!is_bool($data->del("another_test"))){
    echo "ERROR: data del function return wrong value on non existing key";
}

// check del return function
$del = $data->del("test");
if(!is_bool($del) && $del !== true){
    echo "ERROR: data del function return wrong value";
}
// check if key was removed
if($data->has("test") !== false){
    echo "ERROR: data del function do not remove key";
}

// test all above but with section
if(!is_bool($data->set("test", "test_value", "t_sec"))){
    echo "ERROR: data set function with section return non-boolean";
}

if(!is_bool($data->has("test", "t_sec"))){
    echo "ERROR: data has function with section return non-boolean";
}

if(!is_bool($data->has("other_test", "t_sec"))){
    echo "ERROR: data has function with section return non-boolean";
}

if($data->get("test", "t_sec") !== "test_value"){
    echo "ERROR: data get function with section return wrong value";
}

if(!is_null($data->get("another_test", "t_sec"))){
    echo "ERROR: data get function return wrong value on non existing key";
}

if(!is_bool($data->del("another_test", "t_sec"))){
    echo "ERROR: data del function with section return wrong value on non existing key";
}

$del = $data->del("test", "t_sec");
if(!is_bool($del) && $del !== true){
    echo "ERROR: data del function with section return wrong value";
}

if($data->has("test", "t_sec") !== false){
    echo "ERROR: data del function with section do not remove key";
}
