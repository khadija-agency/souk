<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

namespace Europa;
use Europa\i_jwt as i_jwt;

class jwt implements i_jwt {
    private $head    = array();
    private $data    = array();
    private $secret  = null;
    private $token   = null;
    private $expire  = null;
    private $sig     = null;

    public function __construct() {
        $this->head=array("alg" => "HS256", "typ" => "JWT");
    }

    public function set(string $key, $value){
        if($key == "stamp"){
            return false;
        }
        if($key == "expire"){
            return false;
        }
        $this->data[$key] = $value;
        return true;
    }
    public function get(string $key){
        if(isset($this->data[$key])){
            return $this->data[$key];
        }
        return null;
    }
    public function set_expire(int $time){
        $this->expire = $time;
    }
    public function get_expire(){
        return $this->expire;
    }

    private function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    public function set_secret(string $secret,bool $enc=false){
        if($enc === true){
            $this->secret = base64_encode($secret);
        }else{
            $this->secret = $secret;
        }
    }
    public function get_token() {
        if(!is_null($this->token)){
            return $this->token;
        }
        if(is_null($this->secret)){
            $this->set_secret(random_bytes(24));
        }
        if(is_null($this->expire)){
            $this->set_expire(3200);
        }

        $this->data["expire"] = time() + $this->expire;
        $this->data["stamp"]  = date("c");

        $h=$this->base64url_encode(json_encode($this->head));
        $d=$this->base64url_encode(json_encode($this->data));

        $this->sig = $this->base64url_encode(
            hash_hmac('SHA256', "$h.$d", $this->secret, true)
        );

        $this->token = "$h.$d.$this->sig";

        return $this->token;	
    }

    public function set_token($token) {
        $tk        = explode('.', $token);
        $this->head= json_decode(base64_decode($tk[0]), true);
        $this->data= json_decode(base64_decode($tk[1]), true);
        $this->sig = $tk[2];
        $this->token  = $token;
    }

    private function clear_data(){
        $this->head=array(
            "alg" => "HS256",
            "typ" => "JWT"
        );
        $this->data  = null;
        $this->token = null;
        $this->sig   = null;
    }
    public function is_valid():bool{
        if(is_null($this->sig) || is_null($this->token)){
            return false;
        }
        // check if expired
        if(($this->data["expire"] - time()) < 0) {
            $this->clear_data();
            return false;
        }
        // encode heads
        $h = $this->base64url_encode(
            json_encode($this->head)
        );
        // encode data
        $d = $this->base64url_encode(
            json_encode($this->data)
        );

        // generate and encode signature 
        $sig = $this->base64url_encode(
            hash_hmac('SHA256', "$h.$d", $this->secret, true)
        );

        // check if they match
        if($this->sig === $sig){
            return true;
        }

        // not valid data
        $this->clear_data();
        return false;
    }
    private function get_auth_header(i_request $request){

        $headers = null;

        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } else if (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android 
            // versions (a nice side-effect of this 
            // fix means we don't care about capitalization 
            // for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    public function get_bearer_token(i_request $request) {
        $headers = get_authorization_header($request);
        
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
}
