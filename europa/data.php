<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */


namespace Europa;

use Europa\i_data as i_data;

class data implements i_data {
    protected array $data = array();

    public function has(string $key, string $section = null): bool{
        if(is_null($section)){
            return isset($this->data[$key]);
        }
        return isset($this->data[$section][$key]);
    }

    public function set(string $key, $value, string $section = null){
        if(is_null($section)){
            $this->data[$key] = $value;
            return true;
        }
        $this->data[$section][$key] = $value;
        return true;
    }

    public function get(string $key, string $section = null) {
        // returns null if key or section key is not set
        if(is_null($section)) {
            if(isset($this->data[$key])) {
                return $this->data[$key];
            }
        }
        if(isset($this->data[$section][$key])) {
            return $this->data[$section][$key];
        }
        return null;
    }

    public function del(string $key, string $section = null){
        if(is_null($section)){
            if(isset($this->data[$key])){
                unset($this->data[$key]);
                return true;
            }
            return false;
        }
        if(isset($this->data[$section][$key])){
            unset($this->data[$section][$key]);
            return true;
        }
        return false;
    }

    public function add_data(array $new_data){
        $this->data = array_replace_recursive
            ($this->data, $new_data);
    }

    public function get_data(){
        return $this->data;
    }
}
