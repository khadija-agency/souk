<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

namespace Europa;
interface i_data {
    public function has(string $key, string $section = null);
    public function set(string $key, $value, string $section = null);
    public function get(string $key, string $section = null) ;
    public function del(string $key, string $section = null);
    public function add_data(array $new_data);
    public function get_data();
}
