<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

namespace Europa;
interface i_response {
    public function add_header(string $header);
	public function redirect(string $url, int $status);
	public function set_compression(int $level);
	public function set_output(string $output);
	public function get_output():array;
	public function send();
}
