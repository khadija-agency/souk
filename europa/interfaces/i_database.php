<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

namespace Europa;
interface i_database {
    public function set_config(i_config $config):bool;
    public function get_pdo();
    public function list_tables():bool|array;
    public function list_columns(string $table):bool|array;
    public function get_system():?string;
    public function get_database():?string;
    public function get_prefix():?string;

    public function set_system(string $system);
    public function set_hostname(string $hostname);
    public function set_username(string $username);
    public function set_password(string $password);
    public function set_database(string $database);
    public function set_charset(string $charset);
    public function set_sslmode(string $sslmode);
    public function set_port(int $port);
    public function set_options(array $options);
    public function set_prefix(string $prefix);
    public function get_dsn();
}
