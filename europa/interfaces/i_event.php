<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

namespace Europa;
interface i_event {
	public function register(string $trigger,i_action $action,int $priority);
	public function trigger(string $event, array $args);
	public function unregister(string $trigger, string $route);
	public function clear(string $trigger);
}
