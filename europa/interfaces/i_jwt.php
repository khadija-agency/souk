<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

namespace Europa;
interface i_jwt {
    public function set(string $key, $value);
    public function get(string $key);
    public function set_expire(int $time);
    public function get_expire();
    public function set_secret(string $secret,bool $encode);
    public function get_token();
    public function set_token($token);
    public function is_valid():bool;
    public function get_bearer_token(i_request $request);
}
