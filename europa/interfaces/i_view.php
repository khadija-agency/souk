<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

namespace Europa;
interface i_view {
    public function set(string $key, $value);
    public function get(string $key);
    public function set_dir(string $dir);
    public function set_theme(string $theme);
    public function set_template(string $template);
    public function render(string $tpl = null);
}
