<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

define("EUROPA", "0.0.1");

// interfaces
require_once("interfaces/i_data.php");
require_once("interfaces/i_config.php");
require_once("interfaces/i_database.php");
require_once("interfaces/i_jwt.php");
require_once("interfaces/i_view.php");
require_once("interfaces/i_action.php");
require_once("interfaces/i_event.php");
require_once("interfaces/i_response.php");

// auxiliary classes
require_once("data.php");
require_once("config.php");
require_once("database.php");

// engine
require_once("request.php");
require_once("response.php");
require_once("loader.php");
require_once("action.php");
require_once("event.php");
require_once("router.php");
require_once("jwt.php");

// MVCL classes
require_once("model.php");
require_once("view.php");
require_once("controller.php");
require_once("language.php");

// Error Reporting
error_reporting(E_ALL);

// Check Version
if (version_compare(phpversion(), '8.1', '<')) {
	exit('PHP8.1+ Required');
}

if (!ini_get('date.timezone')) {
	date_default_timezone_set('UTC');
}

// Windows IIS Compatibility
if (!isset($_SERVER['DOCUMENT_ROOT'])) {
	if (isset($_SERVER['SCRIPT_FILENAME'])) {
		$_SERVER['DOCUMENT_ROOT'] = str_replace('\\', '/', substr($_SERVER['SCRIPT_FILENAME'], 0, 0 - strlen($_SERVER['PHP_SELF'])));
	}
}

if (!isset($_SERVER['DOCUMENT_ROOT'])) {
	if (isset($_SERVER['PATH_TRANSLATED'])) {
		$_SERVER['DOCUMENT_ROOT'] = str_replace('\\', '/', substr(str_replace('\\\\', '\\', $_SERVER['PATH_TRANSLATED']), 0, 0 - strlen($_SERVER['PHP_SELF'])));
	}
}

if (!isset($_SERVER['REQUEST_URI'])) {
	$_SERVER['REQUEST_URI'] = $_SERVER['PHP_SELF'];

	if (isset($_SERVER['QUERY_STRING'])) {
		$_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
	}
}

if (!isset($_SERVER['HTTP_HOST'])) {
	$_SERVER['HTTP_HOST'] = getenv('HTTP_HOST');
}

// Check if SSL
if ((isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) || (isset($_SERVER['HTTPS']) && (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443))) {
	$_SERVER['HTTPS'] = true;
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
	$_SERVER['HTTPS'] = true;
} else {
	$_SERVER['HTTPS'] = false;
}

// Check IP if forwarded IP
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	$_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
} elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	$_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_CLIENT_IP'];
}
