<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

namespace Europa;
class request {
	public array $get = array();
	public array $post = array();
	public array $cookie = array();
	public array $files = array();
	public array $server = array();
	public array $request = array();
	
	public function __construct() {
		$this->get = $this->clean($_GET);
		$this->post = $this->clean($_POST);
		$this->request = $this->clean($_REQUEST);
		$this->cookie = $this->clean($_COOKIE);
		$this->files = $this->clean($_FILES);
		$this->server = $this->clean($_SERVER);
	}

	public function clean(mixed $data): mixed {
		if (!is_array($data)) {
            return trim(
                htmlspecialchars($data, ENT_COMPAT, 'UTF-8')
            );
        }

		foreach ($data as $key => $value) {
			unset($data[$key]);
			$data[$this->clean($key)] = $this->clean($value);
		}
		return $data;
	}
}
