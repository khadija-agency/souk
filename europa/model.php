<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

namespace Europa;

abstract class model {
	protected $registry;
    protected $database = null;
    protected $pdo = null;
    protected $prefix = null;

	public function __construct(i_data $registry) {
		$this->registry = $registry;

        if($this->has_db()){
            $this->set_db($this->get_db());
        }

        if($this->has_pdo()){
            $this->set_pdo($this->get_pdo());
        }

	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}

    public function has_db(): bool {
        if(!is_null($this->database)) {
            return true;
        }
        return $this->registry->has('database');
    }

    public function has_pdo(): bool{
        if(!is_null($this->pdo)) {
            return true;
        }
        return $this->registry->has('pdo');
    }

    public function set_db(i_database $db){
        $this->set_prefix($db->get_prefix());
        $this->registry->set('database', $db);
        $this->database = $db;
    }

    public function set_pdo(\PDO $pdo){
        $this->registry->set('pdo', $pdo);
        $this->pdo = $this->registry->get('pdo');
    }

    public function set_prefix($prefix){
        $this->prefix = $prefix;
    }

    public function get_db() {
        if(!is_null($this->database)) {
            return $this->database;
        }

        $database = $this->registry->get('database');
        if($database instanceof i_database) {
            return $database;
        }
        return false;
    }

    public function get_pdo() {
        if(!is_null($this->pdo)) {
            return $this->pdo;
        }
        $pdo = $this->registry->get('pdo');
        if($pdo instanceof \PDO) {
            return $pdo;
        }
        return false;
    }

    public function get_prefix(){
        return $this->prefix;
    }
}
