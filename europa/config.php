<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */


namespace Europa;
use Europa\data as data;
use Europa\i_config as i_config;

class config extends data implements i_config {
    protected $file = null;

    public function set_file($file):bool{
        $this->file = $file; 
        return is_readable($this->file);
    }
    public function get_file(){
        if(is_null($this->file)) {
            return false;
        }
        return $this->file;
    }

    private function to_string(array $data, $nspaces = 0){
        $str = "";
        foreach ($data as $key => $value){
            if(is_array($value)){
                $str.= str_repeat(" ",$nspaces*2);
                $str.= "[$key]".PHP_EOL; 
                $str.= $this->to_string($value, $nspaces+1);
            }else{
                $str.= str_repeat(" ",$nspaces*2);
                $str.= "$key = $value".PHP_EOL;
            }
        }
        return $str;
    }

    public function parse($file = null, $sections = true, $mode = INI_SCANNER_TYPED) {
        if(!is_null($file)) {
            $this->set_file($file);
        }
        if(!is_readable($this->file)){
            return false;
        }

        $data = parse_ini_file
            ($this->file, $sections, $mode);

        if(is_bool($data) && $data === false){
            return false;
        }

        $this->add_data($data);
    }

    public function save($file = null){
        if(!is_null($file)){
            $this->set_file($file);
        }
        if (is_null($this->file)) {
            return false;
        }
        if(!file_exists($this->file) && !touch($this->file)) {
            return false;
        }

        return file_put_contents(
            $this->file,
            $this->to_string($this->data)
        );
    }
}
