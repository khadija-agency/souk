<?php
/**
 * @package     europa
 * @author      contact at khadija dot agency
 * @copyright   Copyright (c) 2022 - 2023, Agentia Khadija
 * @license     https://opensource.org/license/bsd-3-clause/
 * @link        https://khadija.agency
 */

namespace Europa;
use Europa\data as data;

class view extends data implements i_view {

    private ?string $dir;
    private ?string $theme;
    private ?string $tpl;

    public function __construct(){
        $this->dir   = null;
        $this->theme = null;
        $this->tpl   = null;
    }
    public function set_dir(string $dir){
        $this->dir = $dir;
    }
    public function set_theme(string $theme){
        $this->theme = $theme;
    }
    public function set_template(string $template){
        $this->tpl = $template;
    }
    public function render(string $tpl = null) {

        if(!is_null($tpl)){
            $this->tpl = $tpl;
        }

        $file = "";
        if(!is_null($this->dir)){
            $file = $this->dir;
        }
        if(!is_null($this->theme)){
            $file .= $this->theme;
        }
        $file.= 'template/'.$this->tpl.'.tpl';

        if (!is_file($file)) {
            return false;
        }

        extract($this->data);

        ob_start();

        require($file);

        return ob_get_clean();
    }
}
